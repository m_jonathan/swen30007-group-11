package ssp.kit.utilities;

import java.util.Date;

public class Utilities {
	public static boolean moreThan24hoursAgo(long timeAgo) {
		Date now = new Date();
		return (now.getTime() - timeAgo) > 24*3600*1000; 
	}
}
