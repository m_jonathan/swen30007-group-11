package ssp.kit.communication;

import ssp.kit.objects.Request.RequestType;
import ssp.kit.objects.Status.Signal;
import ssp.kit.objects.Status.Urgency;
import ssp.kit.ui.ActivityInitial;
import android.app.Activity;

public final class Symbols {
	// For encryption and decryption of enumerated types
	public static final int OK   = 0;
	public static final int HELP = 1;
	public static final int LT   = 0;
	public static final int NLT  = 1;
	public static final int BMC  = 0;
	public static final int BMA  = 1;
	public static final int ERR  = -1;
	
	// The prefix for a user's personal push channel. A user should never have this as the prefix to their username.
	public static final String channelprefix = "REPLIESTOSTATUSOF";
	
	// The activity that clicking on a push notification will send you to.
	public static final Class<? extends Activity> activityPushRelations = ActivityInitial.class;
	public static final Class<? extends Activity> activityPushPersonal  = ActivityInitial.class;
	
	// Strings for messages
	public static final String defaultmsg = "I'm OK!";
	public static final String helpnomsg  = " needs HELP!";
	public static final String helpmsg    = " needs HELP: ";
	public static final String oknomsg    = " is OK!";
	public static final String okmsg      = " is OK: ";
	public static final String statusmsg  = ": ";
	public static final String replymsg   = " replied to your status: ";
	public static final String BMCreqmsg  = " wants to be your Assisted Person!";
	public static final String BMAreqmsg  = " wants to be your Carer!";
	public static final String BMCappmsg  = " is now your Carer!";
	public static final String BMAappmsg  = " is now your Assisted Person!";
	public static final String statexpmsg = "This person has changed their status.";
	
	// JSON Strings
	public static final String INTENT_JSON_RECEIVED       = "ssp.kit.PUSH_NOTIFICATION_RECEIVED";
	public static final String NOTIFICATION_TITLE_STATUS  = "Keep in Touch!";
	public static final String NOTIFICATION_TITLE_REPLY   = "Keep in Touch!";
	public static final String NOTIFICATION_TITLE_REQUEST = "KIT Relations Manager";
	
	public static final String STATUS_SIGNAL_OK = "OK";
	public static final String STATUS_SIGNAL_HELP = "HELP";
	
	public static final String STATUS_URGENCY_LIFE_THREATENING = "LIFE THREATENING";
	public static final String STATUS_URGENCY_NON_LIFE_THREATENING = "NON LIFE THREATENING";
	
	public static final String STATUS_NO_MESSAGE = "NO MESSAGE";
	
	
	
	/** Use the Symbols library to encrypt integers into Signals */
	public static Signal encryptS(int i) {
		switch(i) {
			case Symbols.OK:
				return Signal.OK;
			case Symbols.HELP:
				return Signal.HELP;
			default:
				return null;
		}
	}
	
	/** Use the Symbols library to encrypt integers into Urgencies */
	public static Urgency encryptU(int i) {
		switch(i) {
			case Symbols.LT:
				return Urgency.LIFE_THREATENING;
			case Symbols.NLT:
				return Urgency.NON_LIFE_THREATENING;
			default:
				return null;
		}
	}
	
	/** Use the Symbols library to encrypt integers into Urgencies */
	public static RequestType encryptR(int i) {
		switch(i) {
			case Symbols.BMC:
				return RequestType.BE_MY_CARER;
			case Symbols.BMA:
				return RequestType.BE_MY_ASSISTED;
			default:
				return null;
		}
	}
	
	/** Use the Symbols library to decrypt integers from Signals */
	public static int decryptS(Signal s) {
		if(s == null) {
			return Symbols.ERR;
		}
		switch(s) {
			case OK:
				return Symbols.OK;
			case HELP:
				return Symbols.HELP;
			default:
				return Symbols.ERR;
		}
	}
	
	/** Use the Symbols library to decrypt integers into Urgencies */
	public static int decryptU(Urgency u) {
		if(u == null) {
			return Symbols.ERR;
		}
		switch(u) {
			case LIFE_THREATENING:
				return Symbols.LT;
			case NON_LIFE_THREATENING:
				return Symbols.NLT;
			default:
				return Symbols.ERR;
		}
	}
	
	/** Use the Symbols library to decrypt integers into Urgencies */
	public static int decryptR(RequestType r) {
		if(r == null) {
			return Symbols.ERR;
		}
		switch(r) {
			case BE_MY_CARER:
				return Symbols.BMC;
			case BE_MY_ASSISTED:
				return Symbols.BMA;
			default:
				return Symbols.ERR;
		}
	}
}
