package ssp.kit.communication;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Set;

import org.json.JSONException;
import org.json.JSONObject;

import ssp.kit.ServerResponseListener;
import ssp.kit.exception.IllegalRequestApprovalException;
import ssp.kit.exception.IllegalRequestException;
import ssp.kit.exception.InvalidStatusException;
import ssp.kit.exception.InvalidUserException;
import ssp.kit.exception.NotLoggedInException;
import ssp.kit.objects.Reply;
import ssp.kit.objects.Request;
import ssp.kit.objects.Request.RequestType;
import ssp.kit.objects.Status;
import ssp.kit.objects.Status.Signal;
import ssp.kit.objects.Status.Urgency;
import ssp.kit.objects.User;
import android.content.Context;

import com.parse.DeleteCallback;
import com.parse.FindCallback;
import com.parse.LogInCallback;
import com.parse.ParseACL;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParsePush;
import com.parse.ParseQuery;
import com.parse.ParseUser;
import com.parse.PushService;
import com.parse.SaveCallback;
import com.parse.SignUpCallback;

/**
 * Communication module: manages the app's communication with the KIT server.
 * It maintains a persistent communication with the server so that the app can 
 * interact with it using the stored identity generated during log-in.
 * 
 * @author wwidjaya
 */
public class Communication {
	private enum Action {
		SEND, APPROVE, DELETE
	}
	
	private static Communication comm = new Communication(null);
	
	
	private ParseUser pUser;
	
	
	/** Constructor should remain private. */
	private Communication(ParseUser pUser) {
		this.pUser = pUser;
	}
	
	
	/** Retrieve the singleton Communication object. */
	public static Communication instance() {
		return comm;
	}
		
	
	
	//////////////////////
	// Fetch Operations //
	//////////////////////
	
	/** Fetch the logged-in user and pass it to the listener. */
	public void getUser(ServerResponseListener listener) {
		if(!isAuthenticated()) {
			listener.onServerResponded(new Object(), new NotLoggedInException());
			return;
		}
		
		User user = compose(pUser);
		listener.onServerResponded(user, null);
		
		return;
	}
	
	/** Fetch the named user and pass it to the listener. */
	public void getUser(String username, final ServerResponseListener listener) {
		if(!isAuthenticated()) {
			listener.onServerResponded(new Object(), new NotLoggedInException());
			return;
		}
		
		ParseQuery<ParseUser> query = ParseUser.getQuery();
		
		query.whereEqualTo("username", username);
		
		query.findInBackground(new FindCallback<ParseUser>() {
			public void done(List<ParseUser> objects, ParseException e) {
				if (e == null && !objects.isEmpty()) {
					ParseUser pUser = objects.get(0);
					User      user  = compose(pUser);
					
					listener.onServerResponded(user, null);
				} else {
					listener.onServerResponded(new Object(), e);
				}
			}
		});
		
		return;
	}
	
	/** Fetches the list of carers for the user, then passes it to the listener. The returned objects have the full name field missing. */
	public void getCarerNames(final ServerResponseListener listener) {
		if(!isAuthenticated()) {
			listener.onServerResponded(new Object(), new NotLoggedInException());
			return;
		}
		
		ParseQuery<ParseObject> query = ParseQuery.getQuery("Relationship");
		
		query.whereEqualTo("assisted", pUser.getUsername());
		
		query.findInBackground(new FindCallback<ParseObject>() {
		    public void done(List<ParseObject> pCarers, ParseException e) {
		        if (e == null) {
		        	ArrayList<User> carers = new ArrayList<User>();
		        	
		        	for(ParseObject pCarer : pCarers) {
		        		User carer = new User(pCarer.getString("carer"));
		        		carers.add(carer);
		        	}
		        	
		        	listener.onServerResponded(carers, null);
		        } else {
		        	listener.onServerResponded(new Object(), e);
		        }
		    }
		});
		
		return;
	}
	
	/** Fetches the list of assisted persons for the user, then passes it to the listener. The returned objects have the full name field missing. */
	public void getAssistedNames(final ServerResponseListener listener) {
		if(!isAuthenticated()) {
			listener.onServerResponded(new Object(), new NotLoggedInException());
			return;
		}
		
		ParseQuery<ParseObject> query = ParseQuery.getQuery("Relationship");
		
		query.whereEqualTo("carer", pUser.getUsername());
		
		query.findInBackground(new FindCallback<ParseObject>() {
		    public void done(List<ParseObject> pAssisteds, ParseException e) {
		        if (e == null) {
		        	ArrayList<User> assisteds = new ArrayList<User>();
		        	
		        	for(ParseObject pAssisted : pAssisteds) {
		        		User assisted = new User(pAssisted.getString("assisted"));
		        		assisteds.add(assisted);
		        	}
		        	
		        	listener.onServerResponded(assisteds, null);
		        } else {
		        	listener.onServerResponded(new Object(), e);
		        }
		    }
		});
		
		return;
	}
	
	/** Fetches the status for the given user, then passes it to the listener. */
	public void getStatus(User user, final ServerResponseListener listener) {
		if(!isAuthenticated()) {
			listener.onServerResponded(new Object(), new NotLoggedInException());
			return;
		}
		
		ParseQuery<ParseObject> query = ParseQuery.getQuery("Status");
		
		query.whereEqualTo("username", user.getUsername());
		
		query.findInBackground(new FindCallback<ParseObject>() {
			public void done(List<ParseObject> objects, ParseException e) {
				if (e == null && !objects.isEmpty()) {
			    	ParseObject pStatus = objects.get(0);
			    	Status      status  = composeStatus(pStatus);
			    	
			    	listener.onServerResponded(status, null);
				} else {
					listener.onServerResponded(new Object(), e);
				}
			}
		});
		
		return;
	}
	
	/** Fetches the replies for the given status, then passes it to the listener. */
	public void getReplies(Status status, final ServerResponseListener listener) {
		if(!isAuthenticated()) {
			listener.onServerResponded(new Object(), new NotLoggedInException());
			return;
		}
		
		ParseQuery<ParseObject> query = ParseQuery.getQuery("Reply");
		
		query.whereEqualTo("s_author", status.getUsername());
		query.whereEqualTo("s_time",   status.getTimestamp());
		
		query.findInBackground(new FindCallback<ParseObject>() {
		    public void done(List<ParseObject> pReplies, ParseException e) {
		        if (e == null) {
		        	ArrayList<Reply> replies = new ArrayList<Reply>();
		        	
		        	for(ParseObject pReply : pReplies) {
		        		Reply reply = composeReply(pReply);
		        		replies.add(reply);
		        	}
		        	
		        	listener.onServerResponded(replies, null);
		        } else {
		        	listener.onServerResponded(new Object(), e);
		        }
		    }
		});
		
		return;
	}
	
	/** Fetches all relationship requests related to the user. */
	public void getRequests(final ServerResponseListener listener) {
		if(!isAuthenticated()) {
			listener.onServerResponded(new Object(), new NotLoggedInException());
			return;
		}
		
		getRequestsBy(pUser.getUsername(), new ServerResponseListener() {
			@SuppressWarnings("unchecked")
			public void onServerResponded(Object o, Exception e) {
				if(o instanceof ArrayList) {
					final ArrayList<Request> first_half = (ArrayList<Request>) o;
					
					getRequestsTo(pUser.getUsername(), new ServerResponseListener() {
						public void onServerResponded(Object o, Exception e) {
							if(o instanceof ArrayList) {
								ArrayList<Request> requests = (ArrayList<Request>) o;
								
								requests.addAll(first_half);
								
								listener.onServerResponded(requests, null);
							} else {
								listener.onServerResponded(new Object(), e);
							}
						}
					});
				} else {
					listener.onServerResponded(new Object(), e);
				}
			}
		});
		
		return;
	}
	
	
	/** Fetches relationship requests requested by the given username, then passes it to the listener. */
	private void getRequestsBy(String username, final ServerResponseListener listener) {
		if(!isAuthenticated()) {
			listener.onServerResponded(new Object(), new NotLoggedInException());
			return;
		}
		
		ParseQuery<ParseObject> query = ParseQuery.getQuery("Request");
		
		query.whereEqualTo("requester", username);
		
		query.findInBackground(new FindCallback<ParseObject>() {
		    public void done(List<ParseObject> pRequests, ParseException e) {
		        if (e == null) {
		        	ArrayList<Request> requests = new ArrayList<Request>();
		        	
		        	for(ParseObject pRequest : pRequests) {
		        		Request request = composeRequest(pRequest);
		        		requests.add(request);
		        	}
		        	
		        	listener.onServerResponded(requests, null);
		        } else {
		        	listener.onServerResponded(new Object(), e);
		        }
		    }
		});
		
		return;
	}
	
	/** Fetches relationship requests targeted to the given username, then passes it to the listener. */
	private void getRequestsTo(String username, final ServerResponseListener listener) {
		if(!isAuthenticated()) {
			listener.onServerResponded(new Object(), new NotLoggedInException());
			return;
		}
		
		ParseQuery<ParseObject> query = ParseQuery.getQuery("Request");
		
		query.whereEqualTo("targeted", username);
		
		query.findInBackground(new FindCallback<ParseObject>() {
		    public void done(List<ParseObject> pRequests, ParseException e) {
		        if (e == null) {
		        	ArrayList<Request> requests = new ArrayList<Request>();
		        	
		        	for(ParseObject pRequest : pRequests) {
		        		Request request = composeRequest(pRequest);
		        		requests.add(request);
		        	}
		        	
		        	listener.onServerResponded(requests, null);
		        } else {
		        	listener.onServerResponded(new Object(), e);
		        }
		    }
		});
		
		return;
	}
	
	
	///////////////////////
	// Active Operations //
	///////////////////////
	
	/** Query for the user's status. Then updates the relevant entries. */
	public void publishStatus(final Status status, final ServerResponseListener listener) {
		if(!isAuthenticated()) {
			listener.onServerResponded(new Object(), new NotLoggedInException());
			return;
		}
		
		ParseQuery<ParseObject> query = ParseQuery.getQuery("Status");
		
		query.whereEqualTo("username", status.getUsername());
		
		query.findInBackground(new FindCallback<ParseObject>() {
			public void done(List<ParseObject> objects, ParseException e) {
				ParseObject nStatus = null;
				
				if (e == null && !objects.isEmpty()) {
			    	ParseObject pStatus = objects.get(0);
			    	
			    	nStatus = parse(pStatus, status);
			    } else {
			    	nStatus = parse(status);
			    }
				
				nStatus.saveInBackground(new SaveCallback() {
		    		public void done(ParseException e) {
		    			if(e == null) {
			    			try {
			    				push(status);
			    				listener.onServerResponded(new Object(), null);
				    		} catch (Exception pe) {
			    				listener.onServerResponded(new Object(), pe);
			    			}
		    			} else {
		    				listener.onServerResponded(new Object(), e);
		    			}
		    		}
		    	});
			}
		});
		
		return;
	}
	
	/** Query for the user's status. Then updates the relevant entries, but update the old replies to point to the new status. */
	public void publishStatusPreserveReplies(final Status status, final ServerResponseListener listener) {
		if(!isAuthenticated()) {
			listener.onServerResponded(new Object(), new NotLoggedInException());
			return;
		}
		
		ParseQuery<ParseObject> query = ParseQuery.getQuery("Status");
		
		query.whereEqualTo("username", status.getUsername());
		
		query.findInBackground(new FindCallback<ParseObject>() {
			public void done(List<ParseObject> objects, ParseException e) {
				ParseObject nStatus = null;
				
				final Date old_time;
				
				if (e == null && !objects.isEmpty()) {
			    	ParseObject pStatus = objects.get(0);
			    	
			    	old_time = pStatus.getUpdatedAt();
			    	
			    	nStatus = parse(pStatus, status);
			    } else {
			    	old_time = null;
			    	
			    	nStatus = parse(status);
			    }
				
				nStatus.saveInBackground(new SaveCallback() {
		    		public void done(ParseException e) {
		    			if(e != null) {
		    				listener.onServerResponded(new Object(), e);
		    			} else if(old_time == null) {
		    				try {
		    					push(status);
		    					listener.onServerResponded(new Object(), null);
		    				} catch (Exception pe) {
		        				listener.onServerResponded(new Object(), pe);
		        			}
		    			} else {
			    			ParseQuery<ParseObject> query = ParseQuery.getQuery("Status");
			    			
			    			query.whereEqualTo("username", status.getUsername());
			    			
			    			query.findInBackground(new FindCallback<ParseObject>() {
			    				public void done(List<ParseObject> objects, ParseException e) {
			    					if (e == null && !objects.isEmpty()) {
			    				    	ParseObject pStatus = objects.get(0);
			    				    	
			    				    	String author   = status.getUsername();
			    				    	Date   new_time = pStatus.getUpdatedAt();
			    				    	
			    				    	graftReplies(author, old_time, new_time, new ServerResponseListener() {
			    				    		public void onServerResponded(Object o, Exception e) {
			    				    			if(e == null) {
			    				    				try {
			    				    					push(status);
			    				    					listener.onServerResponded(new Object(), null);
			    				    				} catch (Exception pe) {
			    				        				listener.onServerResponded(new Object(), pe);
			    				        			}
			    				    			} else {
			    				    				listener.onServerResponded(new Object(), e);
			    				    			}
			    				    		}
			    				    	});
			    				    } else {
			    				    	listener.onServerResponded(new Object(), e);
			    				    }
			    				}
			    			});
		    			}
		    		}
		    	});
			}
		});
		
		return;
	}
	
	/** Insert the new reply into the database. */
	public void publishReply(final Reply reply, final Status status, final ServerResponseListener listener) {
		if(!isAuthenticated()) {
			listener.onServerResponded(new Object(), new NotLoggedInException());
			return;
		}
		
		ParseQuery<ParseObject> query = ParseQuery.getQuery("Status");
		
		query.whereEqualTo("username", status.getUsername());
		
		query.findInBackground(new FindCallback<ParseObject>() {
			public void done(List<ParseObject> objects, ParseException e) {
				if (e == null && !objects.isEmpty()) {
			    	Status pStatus = composeStatus(objects.get(0));
			    	
			    	if (status.sameAs(pStatus)) {
			    		ParseObject pReply = parse(reply, pStatus);
			    		pReply.saveInBackground(new SaveCallback() {
			        		public void done(ParseException e) {
				        		if(e == null) {
			        				try {
	    								push(reply, status);
	    								listener.onServerResponded(new Object(), null);
	    							} catch (Exception pe) {
	    								listener.onServerResponded(new Object(), pe);
	    							}
				        		} else {
				        			listener.onServerResponded(new Object(), e);
				        		}
			        		}
			        	});
			    	} else {
			    		listener.onServerResponded(new Object(), new InvalidStatusException());
			    	}
			    } else {
			    	listener.onServerResponded(new Object(), e);
			    }
			}
		});
		
		return;
	}
	
	/** Delete a carer from your relationship list. */
	public void deleteCarer(final String carer, final ServerResponseListener listener) {
		if(!isAuthenticated()) {
			listener.onServerResponded(new Object(), new NotLoggedInException());
			return;
		}
		
		removeRelationship(carer, pUser.getUsername(), new ServerResponseListener() {
			public void onServerResponded(Object data, Exception e) {
				if(e == null) {
					try {
						push(carer);
						listener.onServerResponded(new Object(), null);
					} catch (Exception pe) {
						listener.onServerResponded(new Object(), pe);
					}
				} else {
					listener.onServerResponded(new Object(), e);
				}
			}
		});
		return;
	}
	
	/** Delete an assisted person from your relationship list. */
	public void deleteAssisted(final String assisted, final ServerResponseListener listener) {
		if(!isAuthenticated()) {
			listener.onServerResponded(new Object(), new NotLoggedInException());
			return;
		}
		
		removeRelationship(pUser.getUsername(), assisted, new ServerResponseListener() {
			public void onServerResponded(Object data, Exception e) {
				if(e == null) {
					try {
						push(assisted);
						listener.onServerResponded(new Object(), null);
					} catch (Exception pe) {
						listener.onServerResponded(new Object(), pe);
					}
				} else {
					listener.onServerResponded(new Object(), e);
				}
			}
		});
		return;
	}
	
	/** Insert a new relationship request into the database. */
	public void sendRequest(final Request request, final ServerResponseListener listener) {
		if(!isAuthenticated()) {
			listener.onServerResponded(new Object(), new NotLoggedInException());
			return;
		}
		
		ParseQuery<ParseUser> query = ParseUser.getQuery();
		
		query.whereEqualTo("username", request.getTargeted());
		
		query.findInBackground(new FindCallback<ParseUser>() {
			public void done(List<ParseUser> objects, ParseException e) {
				if (e == null && !objects.isEmpty()) {
					ParseUser   target   = objects.get(0);
					ParseObject pRequest = parse(request, target);
					
					pRequest.saveInBackground(new SaveCallback() {
			    		public void done(ParseException e) {
			    			if(e == null) {
			    				try {
				    				push(request, Action.SEND);
				    				listener.onServerResponded(new Object(), null);
				    			} catch (Exception pe) {
				    				listener.onServerResponded(new Object(), pe);
				    			}
			    			} else {
			    				listener.onServerResponded(new Object(), e);
			    			}
			    		}
			    	});
				} else {
					listener.onServerResponded(new Object(), new InvalidUserException());
				}
			}
		});
		
		return;
	}
	
	/** Approve an existing relationship request. */
	public void approveRequest(final Request request, final ServerResponseListener listener) {
		if(!isAuthenticated()) {
			listener.onServerResponded(new Object(), new NotLoggedInException());
			return;
		}
		
		if(!request.isSentTo(pUser.getUsername())) {
			listener.onServerResponded(new Object(), new IllegalRequestApprovalException());
			return;
		}
		
		ParseQuery<ParseObject> query = ParseQuery.getQuery("Request");
		
		query.whereEqualTo("requester", request.getRequester());
		query.whereEqualTo("targeted",  request.getTargeted());
		
		query.findInBackground(new FindCallback<ParseObject>() {
			public void done(List<ParseObject> objects, ParseException e) {
				if (e == null && !objects.isEmpty()) {
					for(int i = 0; i < objects.size(); i++) {
						ParseObject pRequest = objects.get(i);
						
						final ParseACL acl = pRequest.getACL();
						
						if (i == objects.size() - 1) {
							pRequest.deleteInBackground(new DeleteCallback() {
								public void done(ParseException e) {
									if(e == null) {
										addRelationship(request.getCarer(), request.getAssisted(), acl, new ServerResponseListener() {
											public void onServerResponded(Object o, Exception e) {
												if(e == null) {
													try {
									    				push(request, Action.APPROVE);
									    				listener.onServerResponded(new Object(), null);
									    			} catch (Exception pe) {
									    				listener.onServerResponded(new Object(), pe);
									    			}
												} else {
													listener.onServerResponded(new Object(), e);
												}
											}
										});
									} else {
										listener.onServerResponded(new Object(), e);
									}
								}
							});
						} else {
							pRequest.deleteInBackground();
						}
					}
				} else {
					listener.onServerResponded(new Object(), e);
				}
			}
		});
		
		return;
	}
	
	/** Cancel an existing relationship request. */
	public void cancelRequest(final Request request, final ServerResponseListener listener) {
		if(!isAuthenticated()) {
			listener.onServerResponded(new Object(), new NotLoggedInException());
			return;
		}
		
		ParseQuery<ParseObject> query = ParseQuery.getQuery("Request");
		
		query.whereEqualTo("requester", request.getRequester());
		query.whereEqualTo("targeted",  request.getTargeted());
		
		query.findInBackground(new FindCallback<ParseObject>() {
			public void done(List<ParseObject> objects, ParseException e) {
				if (e == null && !objects.isEmpty()) {
					for(int i = 0; i < objects.size(); i++) {
						ParseObject pRequest = objects.get(i);
						
						if (i == objects.size() - 1) {
							pRequest.deleteInBackground(new DeleteCallback() {
								public void done(ParseException e) {
									if(e == null) {
										try {
						    				push(request, Action.DELETE);
						    				listener.onServerResponded(new Object(), null);
						    			} catch (Exception pe) {
						    				listener.onServerResponded(new Object(), pe);
						    			}
									} else {
										listener.onServerResponded(new Object(), e);
									}
								}
							});
						} else {
							pRequest.deleteInBackground();
						}
					}
				} else {
					listener.onServerResponded(new Object(), e);
				}
			}
		});
		
		return;
	}
	
	
	/** Add a new relationship object into the database. */
	private void addRelationship(String carer, String assisted, ParseACL acl, final ServerResponseListener listener) {
		if(!isAuthenticated()) {
			listener.onServerResponded(new Object(), new NotLoggedInException());
			return;
		}
		
		ParseObject pRelation = parse(carer, assisted, acl);
		pRelation.saveInBackground(new SaveCallback() {
    		public void done(ParseException e) {
    			listener.onServerResponded(new Object(), e);
    		}
    	});
		
		return;
	}
	
	/** Remove a relationship object from the database. */
	private void removeRelationship(String carer, String assisted, final ServerResponseListener listener) {
		if(!isAuthenticated()) {
			listener.onServerResponded(new Object(), new NotLoggedInException());
			return;
		}
		
		ParseQuery<ParseObject> query = ParseQuery.getQuery("Relationship");
		
		query.whereEqualTo("carer",    carer);
		query.whereEqualTo("assisted", assisted);
		
		query.findInBackground(new FindCallback<ParseObject>() {
			public void done(List<ParseObject> objects, ParseException e) {
				if (e == null && !objects.isEmpty()) {
					for(int i = 0; i < objects.size(); i++) {
						ParseObject pRelation = objects.get(i);
						
						if (i == objects.size() - 1) {
							pRelation.deleteInBackground(new DeleteCallback() {
								public void done(ParseException e) {
									if(e == null) {
										listener.onServerResponded(new Object(), null);
									} else {
										listener.onServerResponded(new Object(), e);
									}
								}
							});
						} else {
							pRelation.deleteInBackground();
						}
					}
				} else {
					listener.onServerResponded(new Object(), e);
				}
			}
		});
		
		return;
	}
	
	/** Move all replies from an old status to a new status. */
	private void graftReplies(String author, Date old_time, final Date new_time, final ServerResponseListener listener) {
		if(!isAuthenticated()) {
			listener.onServerResponded(new Object(), new NotLoggedInException());
			return;
		}
		
		ParseQuery<ParseObject> query = ParseQuery.getQuery("Reply");
		
		query.whereEqualTo("s_author", author);
		query.whereEqualTo("s_time",   old_time);
		
		query.findInBackground(new FindCallback<ParseObject>() {
		    public void done(List<ParseObject> pReplies, ParseException e) {
		        if (e == null) {
		        	for(ParseObject pReply : pReplies) {
		        		pReply.put("s_time", new_time);
		        		try {
		        			pReply.save();
		        		} catch(ParseException pe) {
		        			listener.onServerResponded(new Object(), pe);
		        			return;
		        		}
		        	}
		        	listener.onServerResponded(new Object(), null);
		        } else {
		        	listener.onServerResponded(new Object(), e);
		        }
		    }
		});
		
		return;
	}
	
	
	//////////////////
	// Push Service //
	//////////////////
	
	/** Subscribe to the push channels of an ArrayList of users. */
	public void subscribe(Context context, ArrayList<User> users) {
		for(User user : users) {
			PushService.subscribe(context, user.getUsername(), Symbols.activityPushRelations);
		}
		return;
	}
	
	/** Subscribes to personal push channel. */
	public void enablePush(Context context) {
		PushService.subscribe(context, channelOf(pUser.getUsername()), Symbols.activityPushPersonal);
		return;
	}
	
	/** Unsubscribe from all push channels. */
	public void unsubscribeAll(Context context) {
		Set<String> subs = PushService.getSubscriptions(context);
		
		for(String sub : subs) {
			PushService.unsubscribe(context, sub);
		}
		
		return;
	}
	
	/** Send a push notification when publishing a status. */
	private void push(Status status) throws JSONException {
		ParsePush push = new ParsePush();
		
		String channel = status.getUsername();
		String title   = Symbols.NOTIFICATION_TITLE_STATUS;
		String alert   = status.getPushMessage();
		int    task    = PushNotificationReceiver.UPDATE_STATUS;
		
		JSONObject data = jsonify(title, alert, task);
		
		int    signal  = Symbols.decryptS(status.getSignal());
		int    urgency = Symbols.decryptU(status.getUrgency());
		String message = (status.getMessage() == null) ? "NONE" : status.getMessage();
		
		data.put("username", status.getUsername());
		data.put("fullname", status.getFullName());
		data.put("message",  message);
		data.put("signal",   signal);
		data.put("urgency",  urgency);
		
		push.setData(data);
		push.setChannel(channel);
		push.sendInBackground();
		
		return;
	}
	
	/** Send a push notification when publishing a reply. */
	private void push(Reply reply, Status status) throws JSONException {
		if(status.getUsername().equals(pUser.getUsername())) {
			return;
		}
		
		ParsePush push = new ParsePush();
		
		String channel = channelOf(status.getUsername());
		String title   = Symbols.NOTIFICATION_TITLE_REPLY;
		String message = reply.getPushMessage();
		
		int    task     = PushNotificationReceiver.UPDATE_STATUS;
		
		JSONObject data = jsonify(title, message, task);
		data.put("owner", status.getUsername());
		
		push.setData(data);
		push.setChannel(channel);
		push.sendInBackground();
		
		return;
	}
	
	/** Send a push notification when making, approving or cancelling a Relationship request. */
	private void push(Request request, Action action) throws JSONException, IllegalRequestException {
		ParsePush push = new ParsePush();
		
		String channel, title, message;
		
		switch(action) {
			case SEND:
				channel = channelOf(request.getTargeted());
				title   = Symbols.NOTIFICATION_TITLE_REQUEST;
				message = request.getRequestMessage();
				break;
			case APPROVE:
				channel = channelOf(request.getRequester());
				title   = Symbols.NOTIFICATION_TITLE_REQUEST;
				message = request.getApprovalMessage();
				break;
			case DELETE:
				String pushTarget = request.getOtherUser(pUser.getUsername());
				channel = channelOf(pushTarget);
				title   = ""; // Don't show in the notif bar
				message = ""; // Don't show in the notif bar
				break;
			default:
				throw new IllegalRequestException();
		}
		
		int task = PushNotificationReceiver.UPDATE_RELATION;
		
		JSONObject data = jsonify(title, message, task);
		
		push.setData(data);
		push.setChannel(channel);
		push.sendInBackground();
		
		return;
	}
	
	/** Send a push notification removing an existing Relationship. */
	private void push(String removedRelationUsername) throws JSONException {
		ParsePush push = new ParsePush();
		
		String channel  = channelOf(removedRelationUsername);
		int    task     = PushNotificationReceiver.UPDATE_RELATION;
		String message  = "";
		String title    = "";
		
		JSONObject data = jsonify(title, message, task);
		
		push.setData(data);
		push.setChannel(channel);
		push.sendInBackground();
		
		return;
	}
	
	/** Constructs the personal channel of the given username. */
	private String channelOf(String username) {
		return Symbols.channelprefix.concat(username);
	}
	
	
	////////////////////////
	// Account Management //
	////////////////////////
	
	/** Updated the pUser field and checks whether the user is authenticated. */
	public boolean isAuthenticated() {
		pUser = ParseUser.getCurrentUser();
		return (pUser == null) ? false : pUser.isAuthenticated();
	}
	
	/** Simply logs the user out. */
	public void logout(Context context) {
		unsubscribeAll(context);
		pUser = null;
		ParseUser.logOut();
		return;
	}
	
	/** Attempts to log in using the provided credentials. */
	public void login(String username, String password,
					   final ServerResponseListener listener) {
		ParseUser.logInInBackground(username, password, new LogInCallback() {
			public void done(ParseUser user, ParseException e) {
				if (user != null) {
					pUser = user;
					listener.onServerResponded(new Object(), null);
				} else {
					listener.onServerResponded(new Object(), e);
				}
			}
		});
		
		return;
	}
	
	/** Attempts to register using the provided credentials. */
	public void register(String username, String fullname, String password,
						  final	ServerResponseListener listener) {
		ParseUser user = new ParseUser();
		
		user.setUsername(username);
		user.setPassword(password);
		user.put("fullname", fullname);
		
		user.signUpInBackground(new SignUpCallback() {
			public void done(ParseException e) {
				if (e == null) {
					pUser = ParseUser.getCurrentUser();
					listener.onServerResponded(new Object(), null);
				} else {
					listener.onServerResponded(new Object(), e);
				}
			}
		});
		
		return;
	}
	
	
	////////////////////////
	//      PRIVATE       //
	// Object Translators //
	////////////////////////
	
	/** Creates a JSON object for push notifications. */
	private JSONObject jsonify(String title, String alert, int task) throws JSONException {
		JSONObject data = new JSONObject();
		
		data.put("action", Symbols.INTENT_JSON_RECEIVED);
		if (!title.equals("") || !alert.equals("")) {
			// Put title and alert only if at least one of them is not
			// empty string.
			data.put("title",  title);
			data.put("alert",  alert);
		}
		data.put(PushNotificationReceiver.TASK_KEY, Integer.toString(task));
		
		return data;
	}
	
	/** Converts a ParseUser Object to a KIT User Object */
	private User compose(ParseUser pUser) {
		String username = pUser.getUsername();
		String fullname = pUser.getString("fullname");
		
		return new User(username, fullname, new Status());
	}
	
	/** Converts a ParseObject to a KIT Status Object */
	private Status composeStatus(ParseObject pStatus) {
		Date    timestamp = pStatus.getUpdatedAt();
		String  username  = pStatus.getString("username");
		String  fullname  = pStatus.getString("fullname");
		String  message   = pStatus.getString("message");
		Signal  signal    = Symbols.encryptS(pStatus.getInt("signal"));
		Urgency urgency   = Symbols.encryptU(pStatus.getInt("urgency"));
		
		return new Status(timestamp, username, fullname, message, signal, urgency);
	}
	
	/** Converts a ParseObject to a KIT Reply Object */
	private Reply composeReply(ParseObject pReply) {
		Date   timestamp = pReply.getCreatedAt();
		String username  = pReply.getString("username");
		String fullname  = pReply.getString("fullname");
		String message   = pReply.getString("message");
		
		return new Reply(timestamp, username, fullname, message);
	}
	
	/** Converts a ParseObject to a KIT Request Object */
	private Request composeRequest(ParseObject pRequest) {
		Date        timestamp = pRequest.getCreatedAt();
		String      requester = pRequest.getString("requester");
		String      targeted  = pRequest.getString("targeted");
		RequestType type      = Symbols.encryptR(pRequest.getInt("type"));
		
		return new Request(requester, targeted, type, timestamp);
	}
	
	/** Converts a KIT Status Object to a ParseObject */
	private ParseObject parse(Status status) {
		ParseObject pStatus = new ParseObject("Status");
		
		pStatus.put("username", status.getUsername());
		pStatus.put("fullname", status.getFullName());
		pStatus.put("message",  status.getMessage());
		pStatus.put("signal",   Symbols.decryptS(status.getSignal()));
		pStatus.put("urgency",  Symbols.decryptU(status.getUrgency()));
		
		return pStatus;
	}
	
	/** Updates a ParseObject with details from a KIT Status Object */
	private ParseObject parse(ParseObject pStatus, Status status) {
		pStatus.put("message", status.getMessage());
		pStatus.put("signal",  Symbols.decryptS(status.getSignal()));
		pStatus.put("urgency", Symbols.decryptU(status.getUrgency()));
		
		return pStatus;
	}
	
	/** Converts a KIT Reply Object to a ParseObject */
	private ParseObject parse(Reply reply, Status status) {
		ParseObject pReply = new ParseObject("Reply");
		
		pReply.put("s_author", status.getUsername());
		pReply.put("s_time",   status.getTimestamp());
		pReply.put("username", reply.getUsername());
		pReply.put("fullname", reply.getFullName());
		pReply.put("message",  reply.getMessage());
		
		return pReply;
	}
	
	/** Converts a Relationship to a ParseObject */
	private ParseObject parse(String carer, String assisted, ParseACL acl) {
		ParseObject pRelation = new ParseObject("Relationship");
		
		pRelation.setACL(acl);
		pRelation.put("carer",    carer);
		pRelation.put("assisted", assisted);
		
		return pRelation;
	}
	
	/** Converts a Request to a ParseObject */
	private ParseObject parse(Request request, ParseUser target) {
		ParseObject pRequest = new ParseObject("Request");
		ParseACL    acl      = new ParseACL(pUser);
		
		acl.setPublicReadAccess(true);
		acl.setWriteAccess(target,true);
		
		pRequest.setACL(acl);
		pRequest.put("requester", request.getRequester());
		pRequest.put("targeted",  request.getTargeted());
		pRequest.put("type",      Symbols.decryptR(request.getType()));
		
		return pRequest;
	}
	
}
