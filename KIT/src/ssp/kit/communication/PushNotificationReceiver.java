package ssp.kit.communication;

import org.json.JSONException;
import org.json.JSONObject;

import ssp.kit.KIT;
import ssp.kit.ServerResponseListener;
import ssp.kit.ui.ActivityNotification;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

public class PushNotificationReceiver extends BroadcastReceiver {
	public static final String TASK_KEY        = "KIT.PUSH.RECEIVE";
	public static final int    DO_NOTHING      = 0;
	public static final int    UPDATE_RELATION = 1;
	public static final int    UPDATE_STATUS   = 2;
	
	/** Handle incoming push notification. */
	@Override
	public void onReceive(Context context, Intent intent) {
		try {
			// Determine which task to perform
			JSONObject json = new JSONObject(intent.getExtras().getString("com.parse.Data"));
			
			switch (json.getInt(TASK_KEY)) {
				case DO_NOTHING:
					break;
				case UPDATE_RELATION:
					updateRelation(context);
					break;
				case UPDATE_STATUS:
					updateStatus(json.getString("username"));
					notifyIfNecessary(json, context);
					break;
				default:
					android.util.Log.e("ssp.kit", "Invalid task number from push notification");
					break;
			}
		} catch (JSONException e) {
			android.util.Log.e("ssp.kit", "JSONException: " + e.getMessage());
		}
		
		return;
	}
	
	/** Update relation lists. */
	private void updateRelation(Context context) {
		if (!KIT.app().isLoggedIn()) {
			return;
		}
		
		KIT.app().loadRelations(context, new ServerResponseListener() {
			@Override
			public void onServerResponded(Object data, Exception e) {
				if (e == null) {
					KIT.app().loadAllRequests(new ServerResponseListener() {
						@Override
						public void onServerResponded(Object data, Exception e) {
							if (e != null) {
								android.util.Log.e("ssp.kit", "Failed to load requests: "+e.getLocalizedMessage());
							}
						}
					});
				} else {
					android.util.Log.e("ssp.kit", "Failed to load relations: "+e.getLocalizedMessage());
				}
			}
		});
		
		return;
	}
	
	/** Update the relevant status to the pushed object. */
	private void updateStatus(final String username) {
		if (!KIT.app().isLoggedIn()) {
			return;
		}
		
		KIT.app().loadRepliesOf(username, new ServerResponseListener() {
			public void onServerResponded(Object data, Exception e) {
				if (e != null) {
					android.util.Log.e("ssp.kit", "Failed to load user '"+username+"': "+e.getLocalizedMessage());
				}
			}
		});
		return;
	}
	
	private void notifyIfNecessary(JSONObject json, Context context) throws JSONException {
		String username, fullname, message, signal, urgency;
		
		switch(json.getInt("signal")) {
			case Symbols.HELP:
				signal = "HELP";
				break;
			case Symbols.OK:
			default:
				return;
		}
		
		switch(json.getInt("urgency")) {
			case Symbols.NLT:
				urgency = "NLT";
				break;
			case Symbols.LT:
				urgency = "LT";
				break;
			default:
				urgency = "NONE";
				break;
		}
	
		username = json.getString("username");
		fullname = json.getString("fullname");
		message  = json.getString("message");
		
		Intent activityNotificationIntent = new Intent(context, ActivityNotification.class);
		activityNotificationIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
		activityNotificationIntent.putExtra(ActivityNotification.EXTRA_STATUS_AUTHOR_USERNAME, username);
		activityNotificationIntent.putExtra(ActivityNotification.EXTRA_STATUS_AUTHOR_FULLNAME, fullname);
		activityNotificationIntent.putExtra(ActivityNotification.EXTRA_STATUS_STATUS_SIGNAL,   signal);
		activityNotificationIntent.putExtra(ActivityNotification.EXTRA_STATUS_STATUS_URGENCY,  urgency);
		activityNotificationIntent.putExtra(ActivityNotification.EXTRA_STATUS_STATUS_MESSAGE,  message);
		context.startActivity(activityNotificationIntent);
	}

}
