package ssp.kit.communication;

import java.util.ArrayList;

import ssp.kit.ServerResponseListener;
import ssp.kit.exception.IllegalRequestApprovalException;
import ssp.kit.exception.IllegalRequestException;
import ssp.kit.exception.InvalidRelationException;
import ssp.kit.exception.NotLoggedInException;
import ssp.kit.exception.StillLoadingException;
import ssp.kit.objects.Reply;
import ssp.kit.objects.Request;
import ssp.kit.objects.Status;
import ssp.kit.objects.User;
import android.content.Context;

public class Account {
	private Communication      comm;
	
	private User               user;
	private ArrayList<User>    carers;
	private ArrayList<User>    assisteds;
	private ArrayList<Request> requests;
	
	private User               tmp_user;
	private ArrayList<User>    tmp_carers;
	private ArrayList<User>    tmp_assisteds;
	private ArrayList<Request> tmp_requests;
	
	private boolean loading_login;
	private boolean loading_user;
	private boolean loading_relations;
	private boolean loading_requests;
	private boolean loading_active;
	
	public Account(Communication comm) {
		this.comm = comm;
		
		this.user      = null;
		this.carers    = new ArrayList<User>();
		this.assisteds = new ArrayList<User>();
		this.requests  = new ArrayList<Request>();
		
		this.tmp_user      = null;
		this.tmp_carers    = new ArrayList<User>();
		this.tmp_assisteds = new ArrayList<User>();
		this.tmp_requests  = new ArrayList<Request>();
		
		this.loading_login     = false;
		this.loading_user      = false;
		this.loading_relations = false;
		this.loading_requests  = false;
		this.loading_active    = false;
	}
	
	/** Checks whether an active connection is present. */
	public boolean isLoggedIn() {
		return user != null && isCachedOnDisk();
	}
	
	/** Checks whether an authenticated user is cached on disk. */
	public boolean isCachedOnDisk() {
		return comm.isAuthenticated();
	}
	
	/** Logs out and clears all data from device. */
	public void logout(Context context) {
		comm.logout(context);
		
		this.user      = null;
		this.carers    = new ArrayList<User>();
		this.assisteds = new ArrayList<User>();
		this.requests  = new ArrayList<Request>();
		return;
	}
	
	/** Attempts to log in using the provided credentials.
	 * If log-in is a success, the user lists are populated.
	 */
	public void login(String username, String password, final Context context, final ServerResponseListener listener) {
		if(loading_login) {
			listener.onServerResponded(new Object(), new StillLoadingException());
			return;
		}
		
		loading_login = true;
		
		logout(context);
		comm.login(username, password, new ServerResponseListener() {
			public void onServerResponded(Object o, Exception e) {
				if(e == null) {
					loadEverything(context, new ServerResponseListener() {
						public void onServerResponded(Object o, Exception e) {
							loading_login = false;
							listener.onServerResponded(o, e);
						}
					});
				} else {
					loading_login = false;
					listener.onServerResponded(o, e);
				}
			}
		});
		
		return;
	}
	
	/** Attempts to register a new user. 
	 * If registration is a success, the user lists are populated.
	 */
	public void register(final String username, final String fullname, String password, final Context context, final ServerResponseListener listener) {
		if(loading_login) {
			listener.onServerResponded(new Object(), new StillLoadingException());
			return;
		}
		
		loading_login = true;
		
		logout(context);
		comm.register(username, fullname, password, new ServerResponseListener() {
			public void onServerResponded(Object o, Exception e) {
				if(e == null) {
					Status status = new Status(username, fullname);
					
					user = new User(username, fullname, status);
					updateCacheUser();
					
					publishStatus(status, new ServerResponseListener() {
						public void onServerResponded(Object o, Exception e) {
							if(e == null) {
								loadEverything(context, new ServerResponseListener() {
									public void onServerResponded(Object o, Exception e) {
										loading_login = false;
										listener.onServerResponded(o, e);
									}
								});
							} else {
								loading_login = false;
								listener.onServerResponded(o, e);
							}
						}
					});
				} else {
					loading_login = false;
					listener.onServerResponded(o, e);
				}
			}
		});
		
		return;
	}
	
	
	///////////////////////////
	// Account Re-population //
	///////////////////////////
	
	/** Reloads everything except the assisted persons' replies. */
	public void loadEverything(final Context context, final ServerResponseListener listener) {
		loadUser(new ServerResponseListener() {
			public void onServerResponded(Object o, Exception e) {
				if(e == null) {
					loadRelations(context, new ServerResponseListener() {
						public void onServerResponded(Object o, Exception e) {
							if(e == null) {
								loadRequests(new ServerResponseListener() {
									public void onServerResponded(Object o, Exception e) {
										if(e == null) {
											loadPushService(context);
											
											updateCacheAll();
											
											listener.onServerResponded(o, null);
										} else {
											listener.onServerResponded(o, e);
										}
									}
								});
							} else {
								listener.onServerResponded(o, e);
							}
						}
					});
				} else {
					listener.onServerResponded(o, e);
				}
			}
		});
		
		return;
	}
	
	/** Reloads the user's status and replies. */
	public void loadUser(final ServerResponseListener listener) {
		if(loading_user) {
			listener.onServerResponded(new Object(), new StillLoadingException());
			return;
		}
		
		loading_user = true;
		
		comm.getUser(new ServerResponseListener() {
			public void onServerResponded(Object o, Exception e) {
				if(o instanceof User) {
					user = (User) o;
					user.loadStatusReplies(comm, new ServerResponseListener() {
						public void onServerResponded(Object o, Exception e) {
							if(e == null) {
								updateCacheUser();
								loading_user = false;
								listener.onServerResponded(o, null);
							} else {
								loading_user = false;
								listener.onServerResponded(o, e);
							}
						}
					});
				} else {
					loading_user = false;
					listener.onServerResponded(o, e);
				}
			}
		});
		return;
	}
	
	/** Reloads the relations' names, as well as the statuses of all assisted persons. */
	public void loadRelations(final Context context, final ServerResponseListener listener) {
		if(loading_relations) {
			listener.onServerResponded(new Object(), new StillLoadingException());
			return;
		}
		
		loading_relations = true;
		
		loadCarers(new ServerResponseListener() {
			public void onServerResponded(Object o, Exception e) {
				if(e == null) {
					loadStatusesOfAssisteds(context, new ServerResponseListener() {
						public void onServerResponded(Object o, Exception e) {
							if(e == null) {
								updateCacheRelations();
								loading_relations = false;
								listener.onServerResponded(o, null);
							} else {
								loading_relations = false;
								listener.onServerResponded(o, e);
							}
						}
					});
				} else {
					loading_relations = false;
					listener.onServerResponded(o, e);
				}
			}
		});
		
		return;
	}
	
	/** Reloads the status and replies of the current status of either the user or an assisted person with the given username. */
	public void loadRepliesOf(final String username, final ServerResponseListener listener) {
		if(user.getUsername().equals(username)) {
			while(loading_user) {}
			
			loading_user = true;
			
			user.loadStatusReplies(comm, new ServerResponseListener() {
				public void onServerResponded(Object o, Exception e) {
					updateCacheUser();
					loading_user = false;
					listener.onServerResponded(o, e);
				}
			});
			
			return;
		} else {
			for(User assisted : assisteds) {
				if(assisted.getUsername().equals(username)) {
					while(loading_relations) {}
					
					loading_relations = true;
					
					assisted.loadStatusReplies(comm, new ServerResponseListener() {
						public void onServerResponded(Object o, Exception e) {
							if(e == null) {
								updateCacheRepliesOf(username);
								loading_relations = false;
								listener.onServerResponded(o, null);
							} else {
								loading_relations = false;
								listener.onServerResponded(o, e);
							}
						}
					});
					return;
				}
			}
			listener.onServerResponded(new Object(), new InvalidRelationException());
		}
		return;
	}
	
	/** Reloads the pending requests list. */
	public void loadRequests(final ServerResponseListener listener) {
		if(loading_requests) {
			listener.onServerResponded(new Object(), new StillLoadingException());
			return;
		}
		
		loading_requests = true;
		
		comm.getRequests(new ServerResponseListener() {
			@SuppressWarnings("unchecked")
			public void onServerResponded(Object o, Exception e) {
				if(o instanceof ArrayList) {
					requests = (ArrayList<Request>) o;
					
					updateCacheRequests();
					loading_requests = false;
					listener.onServerResponded(o, null);
				} else {
					loading_requests = false;
					listener.onServerResponded(o, e);
				}
			}
		});
		
		return;
	}
	
	
	/** Reloads the list of carers. */
	private void loadCarers(final ServerResponseListener listener) {
		comm.getCarerNames(new ServerResponseListener() {
			@SuppressWarnings("unchecked")
			public void onServerResponded(Object o, Exception e) {
				if(o instanceof ArrayList) {
					carers = (ArrayList<User>) o;
					
					loadNextCarerFullName(0, listener);
				} else {
					listener.onServerResponded(o, e);
				}
			}
		});
		
		return;
	}
	
	/** Reloads the list of assisted persons and their statuses. */
	private void loadStatusesOfAssisteds(final Context context, final ServerResponseListener listener) {
		loadAssisteds(new ServerResponseListener() {
			public void onServerResponded(Object o, Exception e) {
				if(e == null) {
					loadNextStatusOfAssisted(0, listener);
				} else {
					loadPushService(context);
					listener.onServerResponded(o, e);
				}
			}
		});
		
		return;
	}
	
	/** Reloads the list of assisted persons. */
	private void loadAssisteds(final ServerResponseListener listener) {
		comm.getAssistedNames(new ServerResponseListener() {
			@SuppressWarnings("unchecked")
			public void onServerResponded(Object o, Exception e) {
				if(o instanceof ArrayList) {
					assisteds = (ArrayList<User>) o;
					
					loadNextAssistedFullName(0, listener);
				} else {
					listener.onServerResponded(o, e);
				}
			}
		});
		
		return;
	}
	
	/** Loads a list of users recursively. */
	private void loadNextCarerFullName(final int index, final ServerResponseListener listener) {
		if(carers.size() == 0) {
			listener.onServerResponded(new Object(), null);
			return;
		} else if(carers.size() <= index) {
			listener.onServerResponded(new Object(), new ArrayIndexOutOfBoundsException());
			return;
		}
		
		comm.getUser(carers.get(index).getUsername(), new ServerResponseListener() {
			public void onServerResponded(Object o, Exception e) {
				if(o instanceof String) {
					User c = (User) o;
					carers.get(index).updateCacheFullName(c.getFullName());
					if(carers.size() == index + 1) {
						listener.onServerResponded(o, null);
					} else {
						loadNextCarerFullName(index + 1, listener);
					}
				} else {
					listener.onServerResponded(o, e);
				}
			}
		});
		
		return;
	}
	
	/** Loads a list of users recursively. */
	private void loadNextAssistedFullName(final int index, final ServerResponseListener listener) {
		if(assisteds.size() == 0) {
			listener.onServerResponded(new Object(), null);
			return;
		} else if(assisteds.size() <= index) {
			listener.onServerResponded(new Object(), new ArrayIndexOutOfBoundsException());
			return;
		}
		
		comm.getUser(assisteds.get(index).getUsername(), new ServerResponseListener() {
			public void onServerResponded(Object o, Exception e) {
				if(o instanceof String) {
					User a = (User) o;
					assisteds.get(index).updateCacheFullName(a.getFullName());
					if(assisteds.size() == index + 1) {
						listener.onServerResponded(o, null);
					} else {
						loadNextAssistedFullName(index + 1, listener);
					}
				} else {
					listener.onServerResponded(o, e);
				}
			}
		});
		
		return;
	}
	
	/** Loads a list of statuses recursively. */
	private void loadNextStatusOfAssisted(final int index, final ServerResponseListener listener) {
		if(assisteds.size() == 0) {
			listener.onServerResponded(new Object(), null);
			return;
		} else if(assisteds.size() <= index) {
			listener.onServerResponded(new Object(), new ArrayIndexOutOfBoundsException());
			return;
		}
		
		User assisted = assisteds.get(index);
		assisted.loadStatus(comm, new ServerResponseListener() {
			public void onServerResponded(Object o, Exception e) {
				if(e == null) {
					if(assisteds.size() == index + 1) {
						listener.onServerResponded(o, null);
					} else {
						loadNextStatusOfAssisted(index + 1, listener);
					}
				} else {
					listener.onServerResponded(o, e);
				}
			}
		});
		
		return;
	}
	
	/** Reloads the push subscriptions for the user.
	 * 
	 * The channels subscribed to are as following:
	 *   Replies to the user's own status.
	 *   Statuses of assisted persons.
	 */
	private void loadPushService(Context context) {
		comm.unsubscribeAll(context);
		comm.enablePush(context);
		comm.subscribe(context, assisteds);
		return;
	}
	
	
	///////////////////////
	// Active Operations //
	///////////////////////
	
	/** Publish a new status.
	 * 
	 * First, the cache is temporarily updated.
	 * On success, reloads the status in cache.
	 */
	public void publishStatus(Status status, final ServerResponseListener listener) {
		if(!isLoggedIn()) {
			listener.onServerResponded(new Object(), new NotLoggedInException());
			return;
		} else if(loading_active) {
			listener.onServerResponded(new Object(), new StillLoadingException());
			return;
		}
		
		loading_active = true;
		
		tmp_user.updateCacheStatus(status);
		comm.publishStatus(status, new ServerResponseListener() {
			public void onServerResponded(Object o, Exception e) {
				if(e == null) {
					loadUser(new ServerResponseListener() {
						public void onServerResponded(Object o, Exception e) {
							loading_active = false;
							listener.onServerResponded(o, e);
						}
					});
				} else {
					while(loading_user) {}
					loading_user = true;
					updateCacheUser();
					loading_user = false;
					
					loading_active = false;
					listener.onServerResponded(o, e);
				}
			}
		});
		
		return;
	}
	
	/** Publish a new status, but preserves all existing replies.
	 * 
	 * First, the cache is temporarily updated.
	 * On success, reloads the status in cache.
	 */
	public void publishStatusPreserveReplies(Status status, final ServerResponseListener listener) {
		if(!isLoggedIn()) {
			listener.onServerResponded(new Object(), new NotLoggedInException());
			return;
		} else if(loading_active) {
			listener.onServerResponded(new Object(), new StillLoadingException());
			return;
		}
		
		loading_active = true;
		
		tmp_user.updateCacheStatusPreserveReplies(status);
		comm.publishStatusPreserveReplies(status, new ServerResponseListener() {
			public void onServerResponded(Object o, Exception e) {
				if(e == null) {
					loadUser(new ServerResponseListener() {
						public void onServerResponded(Object o, Exception e) {
							loading_active = false;
							listener.onServerResponded(o, e);
						}
					});
				} else {
					while(loading_user) {}
					loading_user = true;
					updateCacheUser();
					loading_user = false;
					
					loading_active = false;
					listener.onServerResponded(o, e);
				}
			}
		});
		
		return;
	}
	
	/** Send a reply to the given status if it belongs to self or an assisted person.
	 *
	 * First, the cache is temporarily updated.
	 * On success, reloads the status in cache.
	 */
	public void publishReply(Reply reply, final Status status, final ServerResponseListener listener) {
		if(!isLoggedIn()) {
			listener.onServerResponded(new Object(), new NotLoggedInException());
			return;
		} else if(loading_active) {
			listener.onServerResponded(new Object(), new StillLoadingException());
			return;
		}
		
		loading_active = true;
		
		final String author = status.getUsername();
		
		if(user.getUsername().equals(author)) {
			tmp_user.addCacheReply(reply);
			comm.publishReply(reply, status, new ServerResponseListener() {
				public void onServerResponded(Object o, Exception e) {
					if(e == null) {
						loadRepliesOf(author, new ServerResponseListener() {
							public void onServerResponded(Object o, Exception e) {
								loading_active = false;
								listener.onServerResponded(o, e);
							}
						});
					} else {
						while(loading_user) {}
						loading_user = true;
						updateCacheUser();
						loading_user = false;
						
						loading_active = false;
						listener.onServerResponded(o, e);
					}
				}
			});
			return;
		} else {
			for(User assisted : tmp_assisteds) {
				if(assisted.getUsername().equals(author)) {
					assisted.addCacheReply(reply);
					comm.publishReply(reply, status, new ServerResponseListener() {
						public void onServerResponded(Object o, Exception e) {
							if(e == null) {
								loadRepliesOf(author, new ServerResponseListener() {
									public void onServerResponded(Object o, Exception e) {
										loading_active = false;
										listener.onServerResponded(o, e);
									}
								});
							} else {
								while(loading_relations) {}
								loading_relations = true;
								updateCacheRepliesOf(author);
								loading_relations = false;
								
								loading_active = false;
								listener.onServerResponded(o, e);
							}
						}
					});
					return;
				}
			}
			loading_active = false;
			listener.onServerResponded(new Object(), new InvalidRelationException());
		}
		
		return;
	}
	
	/** Delete a carer from your relationship list. */
	public void deleteCarer(String username, final ServerResponseListener listener) {
		if(!isLoggedIn()) {
			listener.onServerResponded(new Object(), new NotLoggedInException());
			return;
		}
		
		for(User carer : carers) {
			if(carer.getUsername().equals(username)) {
				if(loading_active) {
					listener.onServerResponded(new Object(), new StillLoadingException());
					return;
				}
				
				loading_active = true;
				
				tmp_carers.remove(carer);
				
				while(loading_relations) {}
				loading_relations = true;
				
				comm.deleteCarer(username, new ServerResponseListener() {
					public void onServerResponded(Object o, Exception e) {
						if(e == null) {
							loadCarers(new ServerResponseListener() {
								public void onServerResponded(Object o, Exception e) {
									updateCacheRelations();
									loading_relations = false;
									
									loading_active = false;
									listener.onServerResponded(o, e);
								}
							});
						} else {
							updateCacheRelations();
							loading_relations = false;
							
							loading_active = false;
							listener.onServerResponded(o, e);
						}
					}
				});
				return;
			}
		}
		
		listener.onServerResponded(new Object(), new InvalidRelationException());
		
		return;
	}
	
	/** Delete an assisted person from your relationship list. */
	public void deleteAssisted(final Context context, String username, final ServerResponseListener listener) {
		if(!isLoggedIn()) {
			listener.onServerResponded(new Object(), new NotLoggedInException());
			return;
		}
		
		for(User assisted : assisteds) {
			if(assisted.getUsername().equals(username)) {
				if(loading_active) {
					listener.onServerResponded(new Object(), new StillLoadingException());
					return;
				}
				
				loading_active = true;
				
				tmp_assisteds.remove(assisted);
				comm.deleteAssisted(username, new ServerResponseListener() {
					public void onServerResponded(Object o, Exception e) {
						if(e == null) {
							while(loading_relations) {}
							loading_relations = true;
							
							loadStatusesOfAssisteds(context, new ServerResponseListener() {
								public void onServerResponded(Object o, Exception e) {
									updateCacheRelations();
									loading_relations = false;
									
									loading_active = false;
									listener.onServerResponded(o, e);
								}
							});
						} else {
							while(loading_relations) {}
							loading_relations = true;
							updateCacheRelations();
							loading_relations = false;
							
							loading_active = false;
							listener.onServerResponded(o, e);
						}
					}
				});
				return;
			}
		}
		
		listener.onServerResponded(new Object(), new InvalidRelationException());
		
		return;
	}
	
	/** Send a new relationship request.
	 * 
	 * First, the cache is temporarily updated.
	 * On success, reloads the request list in cache.
	 */
	public void sendRequest(final Request request, final ServerResponseListener listener) {
		if(!isLoggedIn()) {
			listener.onServerResponded(new Object(), new NotLoggedInException());
			return;
		} else if(loading_active) {
			listener.onServerResponded(new Object(), new StillLoadingException());
			return;
		} else if (request.getTargeted().equals(getUser().getUsername())) {
			String txt = (request.getType() == Request.RequestType.BE_MY_CARER) ? "a carer" : "an assisted person";
			String msg = "Cannot add self as " + txt;
			listener.onServerResponded(new Object(), new IllegalRequestException(msg));
			return;
		} else if (alreadyApproved(request)) {
			String txt = (request.getType() == Request.RequestType.BE_MY_CARER) ? "a carer" : "an assisted person";
			String msg = request.getTargeted() + " is already " + txt;
			listener.onServerResponded(new Object(), new IllegalRequestException(msg));
			return;
		} else if (alreadyRequested(request)) {
			listener.onServerResponded(new Object(), null);
			return;
		}
		
		loading_active = true;
		
		tmp_requests.add(request);
		comm.sendRequest(request, new ServerResponseListener() {
			public void onServerResponded(Object o, Exception e) {
				if(e == null) {
					loadRequests(new ServerResponseListener() {
						public void onServerResponded(Object o, Exception e) {
							loading_active = false;
							listener.onServerResponded(o, e);
						}
					});
				} else {
					while(loading_requests) {}
					loading_requests = true;
					updateCacheRequests();
					loading_requests = false;
					
					loading_active = false;
					listener.onServerResponded(o, e);
				}
			}
		});
		
		return;
	}
	
	/** Approve a relationship request.
	 * 
	 * First, the cache is temporarily updated.
	 * On success, reloads the request and relationship lists in cache.
	 */
	public void approveRequest(final Context context, Request request, final ServerResponseListener listener) {
		if(!isLoggedIn()) {
			listener.onServerResponded(new Object(), new NotLoggedInException());
			return;
		} else if(loading_active) {
			listener.onServerResponded(new Object(), new StillLoadingException());
			return;
		}
		
		loading_active = true;
		
		tmp_requests.remove(request);
		if (!request.isSentTo(user.getUsername())) {
			while(loading_requests) {}
			loading_requests = true;
			updateCacheRequests();
			loading_requests = false;
			
			loading_active = false;
			listener.onServerResponded(new Object(), new IllegalRequestApprovalException());
			return;
		}
		
		comm.approveRequest(request, new ServerResponseListener() {
			public void onServerResponded(Object o, Exception e) {
				if(e == null) {
					loadRequests(new ServerResponseListener() {
						public void onServerResponded(Object o, Exception e) {
							if(e == null) {
								loadRelations(context, new ServerResponseListener() {
									public void onServerResponded(Object o, Exception e) {
										loading_active = false;
										listener.onServerResponded(o, e);
									}
								});
							} else {
								loading_active = false;
								listener.onServerResponded(o, e);
							}
						}
					});
				} else {
					while(loading_requests) {}
					loading_requests = true;
					updateCacheRequests();
					loading_requests = false;
					
					loading_active = false;
					listener.onServerResponded(o, e);
				}
			}
		});
		
		return;
	}
	
	/** Decline or cancel a relationship request.
	 * 
	 * First, the cache is temporarily updated.
	 * On success, reloads the request list in cache.
	 */ 
	public void cancelRequest(Request request, final ServerResponseListener listener) {
		if(!isLoggedIn()) {
			listener.onServerResponded(new Object(), new NotLoggedInException());
			return;
		} else if(loading_active) {
			listener.onServerResponded(new Object(), new StillLoadingException());
			return;
		}
		
		loading_active = true;
		
		tmp_requests.remove(request);
		comm.cancelRequest(request, new ServerResponseListener() {
			public void onServerResponded(Object o, Exception e) {
				if(e == null) {
					loadRequests(new ServerResponseListener() {
						public void onServerResponded(Object o, Exception e) {
							loading_active = false;
							listener.onServerResponded(o, e);
						}
					});
				} else {
					while(loading_requests) {}
					loading_requests = true;
					updateCacheRequests();
					loading_requests = false;
					
					loading_active = false;
					listener.onServerResponded(o, e);
				}
			}
		});
		
		return;
	}
	
	
	/** Check whether the requested relation already exists in primary (inner) cache. */
	private boolean alreadyApproved(Request request) {
		String username = user.getUsername();
		
		if (request.getAssisted().equals(username)) {
			String relation = request.getCarer();
			for(User carer : carers) {
				if(carer.getUsername().equals(relation)) {
					return true;
				}
			}
		} else if (request.getCarer().equals(username)) {
			String relation = request.getAssisted();
			for(User assisted: assisteds ) {
				if(assisted.getUsername().equals(relation)) {
					return true;
				}
			}
		}
		
		return false;
	}
	
	/** Check whether the request already exists in primary (inner) cache. */
	private boolean alreadyRequested(Request request) {
		for (Request r : requests) {
			if (r.sameAs(request)) {
				return true;
			}
		}
		return false;
	}
	
	
	//////////////////
	// Cache Update // 
	//////////////////
	
	/** Attempt to update the entire cache. */
	public void updateCacheAll() {
		updateCacheUser();
		updateCacheRelations();
		updateCacheRequests();
		return;
	}
	
	/** Attempt to update the cached User object. */
	public void updateCacheUser() {
		if(user != null) {
			User tmp = user.clone();
			tmp_user = tmp;
		}
		return;
	}
	
	/** Attempt to update the cached Relation lists. */
	public void updateCacheRelations() {
		ArrayList<User> tmp_c = new ArrayList<User>();
		ArrayList<User> tmp_a = new ArrayList<User>();
		
		for(User carer : carers) {
			if(carer != null) {
				tmp_c.add(carer.clone());
			}
		}
		
		for(User assisted : assisteds) {
			if(assisted != null) {
				tmp_a.add(assisted.clone());
			}
		}
		
		tmp_carers    = tmp_c;
		tmp_assisteds = tmp_a;
		
		return;
	}
	
	/** Attempt to update the cached status and replies for the user or the given assisted person. */
	public void updateCacheRepliesOf(String username) {
		if(tmp_user.getUsername().equals(username)) {
			updateCacheUser();
			return;
		} else {
			for(User assisted : assisteds) {
				if(assisted != null){
					if(assisted.getUsername().equals(username)) {
						for(int i = 0; i < tmp_assisteds.size(); i++) {
							User tmp_assisted = tmp_assisteds.get(i);
							if(tmp_assisted != null){
								if(tmp_assisted.getUsername().equals(assisted.getUsername())) {
									User     tmp = assisted.clone();
									tmp_assisteds.set(i, tmp);
									return;
								}
							}
						}
						updateCacheRelations();
						return;
					}
				}
			}
		}
		return;
	}
	
	/** Attempt to update the cached Request list. */
	public void updateCacheRequests() {
		ArrayList<Request> tmp_r = new ArrayList<Request>();
		
		for(Request request : requests) {
			if(request != null) {
				tmp_r.add(request.clone());
			}
		}
		
		tmp_requests = tmp_r;
		
		return;
	}
	
	
	///////////////
	// Accessors //
	///////////////
	
	/** Return a deep copy of the account's user. */
	public User getUser() {
		return (tmp_user == null) ? null : tmp_user.clone();
	}
	
	/** Return a deep copy of a requested carer. */
	public User getCarer(String username) {
		for (User carer : tmp_carers) {
			if ((carer != null) && (carer.getUsername().equals(username))) {
				return carer.clone();
			}
		}
		
		return null;
	}
	
	/** Return a deep copy of a requested assisted person. */
	public User getAssistedPerson(String username) {
		for (User assisted : tmp_assisteds) {
			if ((assisted != null) && (assisted.getUsername().equals(username))) {
				return assisted.clone();
			}
		}
		
		return null;
	}
	
	/** Return a deep copy of the user's carers list. */
	public ArrayList<User> getCarers() {
		ArrayList<User> deepcopy = new ArrayList<User>();
		
		for (User carer : tmp_carers) {
			if(carer != null) {
				deepcopy.add(carer.clone());
			}
		}
		
		return deepcopy;
	}
	
	/** Return a deep copy of the user's assisted persons list. */
	public ArrayList<User> getAssisteds() {
		ArrayList<User> deepcopy = new ArrayList<User>();
		
		for (User assisted : tmp_assisteds) {
			if(assisted != null) {
				deepcopy.add(assisted.clone());
			}
		}
		
		return deepcopy;
	}
	
	/** Return a deep copy of the user's pending requestslist. */
	public ArrayList<Request> getRequests() {
		ArrayList<Request> deepcopy = new ArrayList<Request>();
		
		for (Request request : tmp_requests) {
			if(request != null) {
				deepcopy.add(request.clone());
			}
		}
		
		return deepcopy;
	}
	
}
