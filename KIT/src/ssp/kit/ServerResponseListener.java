package ssp.kit;

public interface ServerResponseListener {

	/**
	 * Actions to perform when the server has responded the request.
	 * 
	 * @param data
	 *            Relevant data to operate with.
	 * @param e
	 *            Indicates whether the server has responded properly or not. If
	 *            there is no exception, this parameter is set to null.
	 */
	public void onServerResponded(Object data, Exception e);

}
