package ssp.kit.exception;

public class IllegalRequestException extends Exception {
	private static final long serialVersionUID = 1L;
	
	public IllegalRequestException() {}
	
	public IllegalRequestException(String detailMessage) {
		super(detailMessage);
	}
	
	public IllegalRequestException(Throwable throwable) {
		super(throwable);
	}
	
	public IllegalRequestException(String detailMessage, Throwable throwable) {
		super(detailMessage, throwable);
	}
	
}
