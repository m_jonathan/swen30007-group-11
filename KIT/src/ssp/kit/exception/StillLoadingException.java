package ssp.kit.exception;

public class StillLoadingException extends Exception {
	private static final long serialVersionUID = 1L;
	
	public StillLoadingException() {
		this("Some background process is still loading");
	}
	
	public StillLoadingException(String detailMessage) {
		super(detailMessage);
	}
	
	public StillLoadingException(Throwable throwable) {
		super(throwable);
	}
	
	public StillLoadingException(String detailMessage, Throwable throwable) {
		super(detailMessage, throwable);
	}
	
}
