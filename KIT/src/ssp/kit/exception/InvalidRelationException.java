package ssp.kit.exception;

public class InvalidRelationException extends Exception {
	private static final long serialVersionUID = 1L;
	
	public InvalidRelationException() {}
	
	public InvalidRelationException(String detailMessage) {
		super(detailMessage);
	}
	
	public InvalidRelationException(Throwable throwable) {
		super(throwable);
	}
	
	public InvalidRelationException(String detailMessage, Throwable throwable) {
		super(detailMessage, throwable);
	}
	
}
