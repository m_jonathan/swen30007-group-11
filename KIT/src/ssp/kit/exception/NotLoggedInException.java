package ssp.kit.exception;

public class NotLoggedInException extends Exception {
	private static final long serialVersionUID = 1L;
	
	public NotLoggedInException() {
		this("Error: login is required");
	}
	
	public NotLoggedInException(String detailMessage) {
		super(detailMessage);
	}
	
	public NotLoggedInException(Throwable throwable) {
		super(throwable);
	}
	
	public NotLoggedInException(String detailMessage, Throwable throwable) {
		super(detailMessage, throwable);
	}
	
}
