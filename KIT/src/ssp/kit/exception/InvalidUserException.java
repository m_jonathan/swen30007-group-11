package ssp.kit.exception;

public class InvalidUserException extends Exception {
	private static final long serialVersionUID = 1L;
	
	public InvalidUserException() {}
	
	public InvalidUserException(String detailMessage) {
		super(detailMessage);
	}
	
	public InvalidUserException(Throwable throwable) {
		super(throwable);
	}
	
	public InvalidUserException(String detailMessage, Throwable throwable) {
		super(detailMessage, throwable);
	}
	
}
