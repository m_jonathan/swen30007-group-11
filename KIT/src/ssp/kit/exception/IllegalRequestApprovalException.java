package ssp.kit.exception;

public class IllegalRequestApprovalException extends Exception {
	private static final long serialVersionUID = 1L;
	
	public IllegalRequestApprovalException() {}
	
	public IllegalRequestApprovalException(String detailMessage) {
		super(detailMessage);
	}
	
	public IllegalRequestApprovalException(Throwable throwable) {
		super(throwable);
	}
	
	public IllegalRequestApprovalException(String detailMessage, Throwable throwable) {
		super(detailMessage, throwable);
	}
	
}
