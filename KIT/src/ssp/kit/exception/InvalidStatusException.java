package ssp.kit.exception;

public class InvalidStatusException extends Exception {
	private static final long serialVersionUID = 1L;
	
	public InvalidStatusException() {}
	
	public InvalidStatusException(String detailMessage) {
		super(detailMessage);
	}
	
	public InvalidStatusException(Throwable throwable) {
		super(throwable);
	}
	
	public InvalidStatusException(String detailMessage, Throwable throwable) {
		super(detailMessage, throwable);
	}
	
}
