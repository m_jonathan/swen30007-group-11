package ssp.kit.ui;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

import ssp.kit.KIT;
import ssp.kit.R;
import ssp.kit.ServerResponseListener;
import ssp.kit.objects.Reply;
import ssp.kit.objects.Status;
import ssp.kit.objects.Status.Signal;
import ssp.kit.objects.Status.Urgency;
import ssp.kit.objects.User;
import ssp.kit.utilities.Utilities;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.text.format.DateUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;
//import android.util.Log;

public class ActivityReplies extends Activity {

	public static final String EXTRA_STATUS_AUTHOR_USERNAME = "ssp.kit.statusAuthorUsername";

	private LinearLayout linearLayoutIndividualReplies;
	private TextView textViewRepliesStatusSignal;
	private TextView textViewRepliesStatusMessage;
	private TextView textViewRepliesStatusTimestamp;
	private TextView textViewRepliesStatusAuthorFullName;
	private TextView textViewRepliesStatusAuthorUsername;
	private EditText editTextRepliesReplyPublisherComposer;
	private Button buttonRepliesReplyPublisherPublish;
	private String stringButtonRepliesReplyPublisherPublish;

	private String statusAuthorUsername;

	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_replies);

		getActionBar().setDisplayHomeAsUpEnabled(true);

		this.textViewRepliesStatusSignal = (TextView) findViewById(R.id.textViewRepliesStatusSignal);
		this.textViewRepliesStatusMessage = (TextView) findViewById(R.id.textViewRepliesStatusMessage);
		this.textViewRepliesStatusTimestamp = (TextView) findViewById(R.id.textViewRepliesStatusTimestamp);
		this.textViewRepliesStatusAuthorFullName = (TextView) findViewById(R.id.textViewRepliesStatusAuthorFullName);
		this.textViewRepliesStatusAuthorUsername = (TextView) findViewById(R.id.textViewRepliesStatusAuthorUsername);
		this.editTextRepliesReplyPublisherComposer = (EditText) findViewById(R.id.editTextRepliesReplyPublisherComposer);
		this.buttonRepliesReplyPublisherPublish = (Button) findViewById(R.id.buttonRepliesReplyPublisherPublish);
		this.stringButtonRepliesReplyPublisherPublish = this.buttonRepliesReplyPublisherPublish.getText().toString();
		this.linearLayoutIndividualReplies = (LinearLayout) findViewById(R.id.linearLayoutIndividualReplies);

		Intent intent = getIntent();
		this.statusAuthorUsername = intent.getStringExtra(EXTRA_STATUS_AUTHOR_USERNAME);

		refreshAll();
	}

	private void refreshStatusAuthor() {
		this.textViewRepliesStatusAuthorFullName.setText(this.getStatusAuthor().getStatus().getFullName());
		this.textViewRepliesStatusAuthorUsername.setText(this.getStatusAuthor().getStatus().getUsername());
	}

	private void refreshStatusSignal() {
		Status status = this.getStatusAuthor().getStatus();
				
		String signalString;
		int statusSignalColor;
		
		if (status.getSignal() == Signal.HELP) {
			signalString = getString(R.string.textViewStatusStatusSignal_help);
			if (status.getUrgency() == Urgency.LIFE_THREATENING) {
				signalString += " " + getString(R.string.textViewRepliesStatusLifeThreateningDetail);
			} else if (status.getUrgency() == Urgency.NON_LIFE_THREATENING) {
				signalString += " " + getString(R.string.textViewRepliesStatusNotLifeThreateningDetail);
			}
			statusSignalColor = R.color.status_help;
		} else {
			signalString = getString(R.string.textViewStatusStatusSignal_ok);			
			if (status.getTimestamp() != null && Utilities.moreThan24hoursAgo(status.getTimestamp().getTime())) {
				statusSignalColor = R.color.status_attention;
			} else {
				statusSignalColor = R.color.status_okay;
			}
		} 
		
		this.textViewRepliesStatusSignal.setText(signalString);
		this.textViewRepliesStatusSignal.setBackgroundColor(getResources().getColor(statusSignalColor));		
	}

	private void refreshStatusMessage() {
		String message = this.getStatusAuthor().getStatus().getMessage();
		if ((message != null) && (!message.isEmpty())) {
			this.textViewRepliesStatusMessage.setText(message);
		} else {
			if (this.getStatusAuthor().getStatus().getSignal() == Signal.OK) {
				this.textViewRepliesStatusMessage.setText(this.getStatusAuthor().getStatus().getFullName() + " " + getString(R.string.generalTextIsOK));
			} else {
				this.textViewRepliesStatusMessage.setText(R.string.textViewStatusMessageEnterAMessage);
			}
		}
	}

	private void refreshStatusTimestamp() {
		this.textViewRepliesStatusTimestamp.setText(DateUtils.getRelativeTimeSpanString(this.getStatusAuthor().getStatus().getTimestamp().getTime()));
	}

	private void refreshStatusReplies(boolean reloadFromServer) {
		// Refresh replies
		displayReplies(getStatusAuthor().getStatus().getReplies());

		// Reload replies from server (if needed)
		if (reloadFromServer) {
			// Prepare listener to handle when replies has been loaded
			ServerResponseListener repliesLoadedListener = new ServerResponseListener() {
				@Override
				public void onServerResponded(Object data, Exception e) {
					if (e == null) {
						findViewById(R.id.textViewLoadingReplies).setVisibility(View.GONE);
						displayReplies(getStatusAuthor().getStatus().getReplies());
					} else {
						// TODO Perform more specific tasks for each kind of
						// exceptions?
						String errorMessage = e.getMessage() == null?"Oops.. Something went wrong":e.getMessage();
						Toast.makeText(ActivityReplies.this, errorMessage, Toast.LENGTH_LONG).show();
					}
				}
			};

			// Reload from server
			if (KIT.app().getUser().getUsername().equals(this.statusAuthorUsername)) {
				KIT.app().loadUser(repliesLoadedListener);
			} else {
				KIT.app().loadRepliesOf(this.statusAuthorUsername, repliesLoadedListener);
			}

			// Display loading text
			findViewById(R.id.textViewLoadingReplies).setVisibility(View.VISIBLE);
		}
	}

	private void refreshAll() {
		this.refreshStatusAuthor();
		this.refreshStatusSignal();
		this.refreshStatusMessage();
		this.refreshStatusTimestamp();
		this.refreshStatusReplies(true);
	}

	private void displayReplies(ArrayList<Reply> replies) {
		LayoutInflater inflater = (LayoutInflater) getApplicationContext().getSystemService(LAYOUT_INFLATER_SERVICE);

		View replyView;
		TextView textViewRepliesReplyAuthorFullName;
		TextView textViewRepliesReplyAuthorUsername;
		TextView textViewRepliesReplyTimestamp;
		TextView textViewRepliesReplyMessage;
		
		// Clear displayed replies
		this.linearLayoutIndividualReplies.removeAllViews();

		// Sort replies
		sortReplies(replies);
		
		// Repopulate replies
		for (Reply reply : replies) {
			
			replyView = inflater.inflate(R.layout.activity_replies_reply, null);

			// Full name
			textViewRepliesReplyAuthorFullName = (TextView) replyView.findViewById(R.id.textViewRepliesReplyAuthorFullName);
			textViewRepliesReplyAuthorFullName.setText(reply.getFullName());
			// Username
			textViewRepliesReplyAuthorUsername = (TextView) replyView.findViewById(R.id.textViewRepliesReplyAuthorUsername);
			textViewRepliesReplyAuthorUsername.setText(reply.getUsername());
			// Timestamp
			textViewRepliesReplyTimestamp = (TextView) replyView.findViewById(R.id.textViewRepliesReplyTimestamp);
			if (reply.getTimestamp() != null) {
				textViewRepliesReplyTimestamp.setText(DateUtils.getRelativeTimeSpanString(reply.getTimestamp().getTime()));
			} else {
				textViewRepliesReplyTimestamp.setText(R.string.generalTextPublishing);
			}
			// Message
			textViewRepliesReplyMessage = (TextView) replyView.findViewById(R.id.textViewRepliesReplyMessage);
			textViewRepliesReplyMessage.setText(reply.getMessage());

			this.linearLayoutIndividualReplies.addView(replyView);
		}
	}

	private User getStatusAuthor() {
		User currentUser = KIT.app().getUser();
		if (currentUser.getUsername().equals(this.statusAuthorUsername)) {
			return currentUser;
		} else {
			return KIT.app().getAssistedPerson(this.statusAuthorUsername);
		}
	}

	public void publishReply(View view) {
		String replyMessage = this.editTextRepliesReplyPublisherComposer.getText().toString();

		KIT.app().publishReply(replyMessage, this.statusAuthorUsername, this.getStatusAuthor().getStatus().getTimestamp(), new ServerResponseListener() {
			@Override
			public void onServerResponded(Object data, Exception e) {
				ActivityReplies.this.buttonRepliesReplyPublisherPublish.setEnabled(true);
				ActivityReplies.this.buttonRepliesReplyPublisherPublish.setText(ActivityReplies.this.stringButtonRepliesReplyPublisherPublish);
				ActivityReplies.this.editTextRepliesReplyPublisherComposer.setEnabled(true);
				if (e == null) {
					ActivityReplies.this.editTextRepliesReplyPublisherComposer.setText("");
					refreshStatusReplies(true);
				} else {
					// TODO Perform more specific tasks for each kind of
					// exceptions?
					String errorMessage = e.getMessage() == null?"Oops.. Something went wrong":e.getMessage();
					Toast.makeText(ActivityReplies.this, errorMessage, Toast.LENGTH_LONG).show();
				}
			}
		});

		this.buttonRepliesReplyPublisherPublish.setEnabled(false);
		ActivityReplies.this.buttonRepliesReplyPublisherPublish.setText(R.string.generalTextPublishing);
		ActivityReplies.this.editTextRepliesReplyPublisherComposer.setEnabled(false);
		refreshStatusReplies(false);
	}

	/** Sorts replies by timestamp, newest first */
	public static void sortReplies(ArrayList<Reply> replies) {
		Collections.sort(replies, new Comparator<Reply>() {
			public int compare(Reply r1, Reply r2) {
				if (r1.getTimestamp() == null) {
					return -1;
				}
				if (r2.getTimestamp() == null) {
					return 1;
				}
				return r2.getTimestamp().compareTo(r1.getTimestamp());
			}
		});
	}
	
}
