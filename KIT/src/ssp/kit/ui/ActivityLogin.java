package ssp.kit.ui;

import ssp.kit.KIT;
import ssp.kit.R;
import ssp.kit.ServerResponseListener;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;

public class ActivityLogin extends Activity {
	private String username;
	private String password;
	private EditText usernameEditText;
	private EditText passwordEditText;
	
	private ProgressDialog progressDialog;

	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		if (KIT.app().isLoggedIn()) {
			toActivityMain();
			return;
		}
		
		setContentView(R.layout.activity_login);
		this.usernameEditText = (EditText) findViewById(R.id.editTextUsername);
		this.passwordEditText = (EditText) findViewById(R.id.editTextPassword);
	}

	public void login(View view) {
		// Get user credentials from the UI components
		this.username = usernameEditText.getText().toString();
		this.password = passwordEditText.getText().toString();
		
		// Prepare ServerResponseListener to pass to the login method
		ServerResponseListener successfulLoginListener = new ServerResponseListener() {
			@Override
			public void onServerResponded(Object data, Exception e) {
				dismissLoggingIn();
				if (e == null) {
					toActivityMain();
				} else {
					showCantLogin(e.getLocalizedMessage());
				}
			}
		};

		KIT.app().login(this.username, this.password, this, successfulLoginListener);
		showLoggingIn();
	}
	
	public void register(View view) {
		Intent toActivityRegisterIntent = new Intent(this, ActivityRegister.class);
		startActivity(toActivityRegisterIntent);
	}

	private void toActivityMain() {
		// Go to main screen intent
		Intent toActivityMainIntent = new Intent(ActivityLogin.this, ActivityMain.class);
		startActivity(toActivityMainIntent);
		ActivityLogin.this.finish(); // Prevent the user to go back with the back button
	}
	
	private void showLoggingIn() {
		this.progressDialog = new ProgressDialog(this);
		this.progressDialog.setMessage(getString(R.string.textLoggingInProgress));
		this.progressDialog.setCancelable(false);
		this.progressDialog.setCanceledOnTouchOutside(false);
		this.progressDialog.show();
	}
	
	private void dismissLoggingIn() {	
		if (this.progressDialog != null) {
			this.progressDialog.dismiss();
		}
	}
	
	private void showCantLogin(String errorMessage) {
		// Create dialog
		AlertDialog.Builder dialog = new AlertDialog.Builder(this);
//		dialog.setTitle(R.string.textViewLoginCantLogin);

		// Error Message
		dialog.setMessage(errorMessage);
		
		// OK
		dialog.setNeutralButton(R.string.generalTextOK, new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int which) {
				dialog.dismiss();
			}
		});
		
		// Show dialog
		dialog.show();
	}
}
