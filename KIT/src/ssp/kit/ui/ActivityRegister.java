package ssp.kit.ui;

import ssp.kit.KIT;
import ssp.kit.R;
import ssp.kit.ServerResponseListener;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.View;
import android.widget.EditText;

public class ActivityRegister extends Activity {
	
	private EditText fullNameEditText;
	private EditText usernameEditText;
	private EditText passwordEditText;
	
	private ProgressDialog progressDialog;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_register);
		getActionBar().setDisplayHomeAsUpEnabled(true);
		
		this.fullNameEditText = (EditText) findViewById(R.id.editTextRegisterFullName);
		this.usernameEditText = (EditText) findViewById(R.id.editTextRegisterUsername);
		this.passwordEditText = (EditText) findViewById(R.id.editTextRegisterPassword);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.activity_register, menu);
		return true;
	}
	
	public void register(View view) {
		// Get user credentials from the UI components
		String fullName = fullNameEditText.getText().toString();
		String username = usernameEditText.getText().toString();
		String password = passwordEditText.getText().toString();
		
		// Prepare ServerResponseListener to pass to the register method
		ServerResponseListener successfulRegisterListener = new ServerResponseListener() {
			public void onServerResponded(Object data, Exception e) {
				dismissRegistering();				
				if (e == null) {
					// Go to main screen intent
					Intent toActivityMainIntent = new Intent(ActivityRegister.this, ActivityMain.class);
					startActivity(toActivityMainIntent);
					ActivityRegister.this.finish(); // Prevent the user to go back with the back button
				} else {
					// TODO Do something
					showCantRegister(e.getLocalizedMessage());
				}
			}
		};
		
		KIT.app().register(username, fullName, password, this, successfulRegisterListener);
		showRegistering();
	}
	
	private void showRegistering() {
		this.progressDialog = new ProgressDialog(this);
		this.progressDialog.setMessage(getString(R.string.textRegisteringInProgress));
		this.progressDialog.setCancelable(false);
		this.progressDialog.setCanceledOnTouchOutside(false);
		this.progressDialog.show();
	}
	
	private void dismissRegistering() {
		if (this.progressDialog != null) {
			this.progressDialog.dismiss();
		}
	}

	private void showCantRegister(String errorMessage) {
		// Create dialog
		AlertDialog.Builder dialog = new AlertDialog.Builder(this);
//		dialog.setTitle(R.string.textViewRegisterCantRegister);

		// Error Message
		dialog.setMessage(errorMessage);
		
		// OK
		dialog.setNeutralButton(R.string.generalTextOK, new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int which) {
				dialog.dismiss();
			}
		});
		
		// Show dialog
		dialog.show();
	}
}
