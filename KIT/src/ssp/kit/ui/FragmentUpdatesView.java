package ssp.kit.ui;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

import ssp.kit.KIT;
import ssp.kit.R;
import ssp.kit.communication.Symbols;
import ssp.kit.objects.Status;
import ssp.kit.objects.Status.Signal;
import ssp.kit.objects.Status.Urgency;
import ssp.kit.objects.User;
import ssp.kit.utilities.Utilities;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.format.DateUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

public class FragmentUpdatesView extends Fragment implements android.view.View.OnClickListener {

	private View rootView;
	private LinearLayout linearLayout;
	private LayoutInflater inflater;
	public FragmentUpdatesView() {
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		this.inflater = inflater;
		this.rootView = inflater.inflate(R.layout.fragment_updates_view, container, false);
		this.linearLayout = (LinearLayout) this.rootView.findViewById(R.id.linear_carer_layout);
		this.refresh();
		return rootView;
	}
	
	@Override
	public void onResume() {
		super.onResume();
		this.refresh();
	}

	public void refresh() {
		View aUserLayout;
		TextView username;
		TextView fullname;
		TextView signal;
		TextView message;
		TextView timestamp;
		LinearLayout entryIndicator;
		int entryIndicatorColor;
		String tempMessage;
		String tempSignalText;
		Status.Signal tempSignal;
		
		//Getting the list of assisted persons
		ArrayList<User> assistedPersons = KIT.app().getAssisteds();
		
		//Sorting mechanism to sort the statuses of the assisted people based on the urgency:
		//1st rank: people who have a HELP signal and a LIFE THREATENING urgency
		//2nd rank: people who have a HELP signal, but no urgency
		//3rd rank: people who have a HELP signal and a NON LIFE THREATENING urgency
		//4th rank: people who have an OK signal but have not updated for 24 hours or more
		//5th rank: people who have an OK signal and the last update is within 24 hours
		Collections.sort(assistedPersons, new Comparator<User>() {
			public int compare(User u1, User u2) {
				if (u1.getStatus().getSignal() == Signal.HELP && u1.getStatus().getUrgency() == Urgency.LIFE_THREATENING) {
					return -1;
				}
				if (u2.getStatus().getSignal() == Signal.HELP && u2.getStatus().getUrgency() == Urgency.LIFE_THREATENING) {
					return 1;
				}
				if (u1.getStatus().getSignal() == Signal.HELP && u1.getStatus().getUrgency() == null) {
					return -1;
				}
				if (u2.getStatus().getSignal() == Signal.HELP && u1.getStatus().getUrgency() == null) {
					return 1;
				}
				if (u1.getStatus().getSignal() == Signal.HELP && u1.getStatus().getUrgency() == Urgency.NON_LIFE_THREATENING) {
					return -1;
				}
				if (u2.getStatus().getSignal() == Signal.HELP && u2.getStatus().getUrgency() == Urgency.NON_LIFE_THREATENING) {
					return 1;
				}
				if (u1.getStatus().getSignal() == Signal.OK && Utilities.moreThan24hoursAgo(u1.getStatus().getTimestamp().getTime())) {
					return -1;
				}
				if (u1.getStatus().getSignal() == Signal.OK && Utilities.moreThan24hoursAgo(u2.getStatus().getTimestamp().getTime())) {
					return 1;
				}
				return u1.getStatus().getTimestamp().compareTo(u2.getStatus().getTimestamp());
			}
		});
		
		//Clearing the linearLayout first
		this.linearLayout.removeAllViews();

		for (User user : assistedPersons) {
			aUserLayout = this.inflater.inflate(R.layout.fragment_updates_view_update_entry, null);
			aUserLayout.setTag(user.getUsername());
			aUserLayout.setOnClickListener(this);
			
			//Adding margin for each different assisted person's statuses
			LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
			layoutParams.setMargins(0, 0, 0, 10); //Bottom margin of 10 pixels
			
			//Set the text in the Updates view for the assisted person's username
			username = (TextView) aUserLayout.findViewById(R.id.textViewCarerTabUsername);
			username.setText(user.getUsername());

			//Set the text in the Updates view for the assisted person's full name
			fullname = (TextView) aUserLayout.findViewById(R.id.textViewCarerTabFullName);
			fullname.setText(user.getStatus().getFullName());
			
			//Set the text in the Updates view for the assisted person's signal and urgency
			//If the signal of the status is a Help signal, display the text like this: 
			//"HELP (LIFE THREATENING)" or like this "HELP (NON-LIFE THREATENING) or like this HELP"
			//Else if the signal is a OK signal, just display the text like this: "OK"
			signal = (TextView) aUserLayout.findViewById(R.id.textViewCarerTabSignal);
			tempSignal = user.getStatus().getSignal();
			if (tempSignal == Status.Signal.HELP) {
				String signalText = Symbols.STATUS_SIGNAL_HELP;
				String urgencyText = ""; 
				Status.Urgency urgency = user.getStatus().getUrgency();
				if (urgency == Status.Urgency.LIFE_THREATENING) {
					urgencyText = " (" + Symbols.STATUS_URGENCY_LIFE_THREATENING + ")";
				}
				else if (urgency == Status.Urgency.NON_LIFE_THREATENING) {
					urgencyText = " (" + Symbols.STATUS_URGENCY_NON_LIFE_THREATENING + ")";
				}
				tempSignalText = signalText + urgencyText;
			}
			else {
				tempSignalText = Symbols.STATUS_SIGNAL_OK;
				
			}
			signal.setText(tempSignalText);
			
			//Set the text in the Updates view for the assisted person's message
			//If the signal of the status is a Help signal and there is no message, 
			//then display the message of the status as: "NO MESSAGE"
			//Else if the signal of the status is a OK signal and there is no message,
			//then display the message of the status as: "<assisted_person_fullname> is OK"
			message = (TextView) aUserLayout.findViewById(R.id.textViewCarerTabMessage);
			tempMessage = user.getStatus().getMessage();
			if (tempSignal == Status.Signal.HELP && (tempMessage == null || tempMessage.equals(""))) {
				tempMessage = Symbols.STATUS_NO_MESSAGE;
			}
			else if (tempSignal == Status.Signal.OK && (tempMessage == null || tempMessage.equals(""))){
				tempMessage = user.getStatus().getFullName() + " " + getString(R.string.generalTextIsOK);
			}
			message.setText(tempMessage);

			//Set the text in the Updates view for the assisted person's timestamp
			timestamp = (TextView) aUserLayout.findViewById(R.id.textViewCarerTabTimestamp);
			if (user.getStatus().getTimestamp() == null) {
				timestamp.setText(R.string.generalTextUpdating);
			} else {
				timestamp.setText(DateUtils.getRelativeTimeSpanString(user.getStatus().getTimestamp().getTime()));
			}
			
			// Setting the indicator
			entryIndicator = (LinearLayout) aUserLayout.findViewById(R.id.linearLayoutUpdateEntryIndicator);
			if (tempSignal == Signal.HELP) {
				entryIndicatorColor = R.color.status_help;
			} else {
				if (user.getStatus().getTimestamp() != null && Utilities.moreThan24hoursAgo(user.getStatus().getTimestamp().getTime())) {
					entryIndicatorColor = R.color.status_attention;
				} else {
					entryIndicatorColor = R.color.status_okay;
				}
			}
			entryIndicator.setBackgroundColor(getResources().getColor(entryIndicatorColor));

			//Add the this particular layout to the main view
			this.linearLayout.addView(aUserLayout, layoutParams);
		}
	}
	
	@Override
	public void onClick(View v) {
		if (v.getTag() != null && v.getTag() instanceof String) {
			Intent intent = new Intent(getActivity(), ActivityReplies.class);
			intent.putExtra(ActivityReplies.EXTRA_STATUS_AUTHOR_USERNAME, (String) v.getTag());
			startActivity(intent);
		}
	}
}
