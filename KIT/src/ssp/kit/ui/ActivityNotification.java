package ssp.kit.ui;

import ssp.kit.R;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.Button;
import android.widget.TextView;

public class ActivityNotification extends Activity implements OnClickListener {
	
	public static final String EXTRA_STATUS_AUTHOR_USERNAME = "ssp.kit.statusAuthorUsername";
	public static final String EXTRA_STATUS_AUTHOR_FULLNAME = "ssp.kit.statusAuthorFullname";
	public static final String EXTRA_STATUS_STATUS_SIGNAL = "ssp.kit.statusStatusSignal";
	public static final String EXTRA_STATUS_STATUS_URGENCY = "ssp.kit.statusStatusUrgency";
	public static final String EXTRA_STATUS_STATUS_MESSAGE = "ssp.kit.statusStatusMessage";
	
	TextView textViewNotificationStatusAuthorUsername;
	TextView textViewNotificationStatusAuthorFullName;
	TextView textViewNotificationStatusSignal;
	TextView textViewNotificationStatusMessage;
	TextView textViewNotificationStatusTimestamp;
	
	Button buttonNotificationOK;
	Button buttonNotificationDismiss;
	
	String statusAuthorUsername;
	String statusAuthorFullname;
	String statusStatusSignal;
	String statusStatusUrgency;
	String statusStatusMessage;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);		
		
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		
		// Prepare activity
		setContentView(R.layout.activity_notification);
		
		this.textViewNotificationStatusAuthorFullName = (TextView) findViewById(R.id.textViewNotificationStatusAuthorFullName);
		this.textViewNotificationStatusAuthorUsername = (TextView) findViewById(R.id.textViewNotificationStatusAuthorUsername);
		this.textViewNotificationStatusSignal = (TextView) findViewById(R.id.textViewNotificationStatusSignal);
		this.textViewNotificationStatusMessage = (TextView) findViewById(R.id.textViewNotificationStatusMessage);
		this.textViewNotificationStatusTimestamp = (TextView) findViewById(R.id.textViewNotificationStatusTimestamp);
		
		this.buttonNotificationOK = (Button) findViewById(R.id.buttonNotificationOK);
		this.buttonNotificationOK.setOnClickListener(this);
		this.buttonNotificationDismiss = (Button) findViewById(R.id.buttonNotificationDismiss);
		this.buttonNotificationDismiss.setOnClickListener(this);
		
		// Read off intent
		Intent intent = getIntent();
		this.statusAuthorUsername = intent.getStringExtra(EXTRA_STATUS_AUTHOR_USERNAME);
		this.statusAuthorFullname = intent.getStringExtra(EXTRA_STATUS_AUTHOR_FULLNAME);
		this.statusStatusSignal = intent.getStringExtra(EXTRA_STATUS_STATUS_SIGNAL);
		this.statusStatusMessage = intent.getStringExtra(EXTRA_STATUS_STATUS_MESSAGE);
		this.statusStatusUrgency = intent.getStringExtra(EXTRA_STATUS_STATUS_URGENCY);
		
		refreshAll();
	}

	private void refreshAll() {
		
		// Full name
		this.textViewNotificationStatusAuthorFullName.setText(statusAuthorFullname);
		
		// Username
		this.textViewNotificationStatusAuthorUsername.setText(statusAuthorUsername);
		
		// Signal
		String signalString;
		int statusSignalColor;
		
		if (statusStatusSignal.equals("HELP")) {
			signalString = getString(R.string.textViewStatusStatusSignal_help);
			if (statusStatusUrgency.equals("LT")) {
				signalString += " " + getString(R.string.textViewRepliesStatusLifeThreateningDetail);
			} else if (statusStatusUrgency.equals("NLT")) {
				signalString += " " + getString(R.string.textViewRepliesStatusNotLifeThreateningDetail);
			}
			statusSignalColor = R.color.status_help;
		} else {
			signalString = getString(R.string.textViewStatusStatusSignal_ok);
			statusSignalColor = R.color.status_okay;
		} 
		
		this.textViewNotificationStatusSignal.setText(signalString);
		this.textViewNotificationStatusSignal.setBackgroundColor(getResources().getColor(statusSignalColor));
		
		// Message
		if ((!statusStatusMessage.equals("NONE")) && (!statusStatusMessage.isEmpty())) {
			this.textViewNotificationStatusMessage.setText(statusStatusMessage);
		} else {
			if (statusStatusSignal.equals("OK")) {
				this.textViewNotificationStatusMessage.setText(statusAuthorFullname + " " + getString(R.string.generalTextIsOK));
			} else {
				this.textViewNotificationStatusMessage.setText(R.string.textViewStatusMessageEnterAMessage);
			}
		}
		
		// Timestamp
		this.textViewNotificationStatusTimestamp.setText("");

	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
			case (R.id.buttonNotificationOK):
				Intent activityRepliesIntent = new Intent(this, ActivityInitial.class);
				activityRepliesIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
				activityRepliesIntent.putExtra(ActivityInitial.EXTRA_NEXT_ACTIVITY, ActivityInitial.NEXT_ACTIVITY_REPLIES);
				activityRepliesIntent.putExtra(ActivityInitial.NEXT_ACTIVITY_REPLIES_USERNAME, statusAuthorUsername);			
				startActivity(activityRepliesIntent);
				finish();
				break;
			case (R.id.buttonNotificationDismiss):
				finish();
				break;
			default:
				break;
		}
	}

}
