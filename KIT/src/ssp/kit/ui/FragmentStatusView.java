package ssp.kit.ui;

import ssp.kit.KIT;
import ssp.kit.R;
import ssp.kit.ServerResponseListener;
import ssp.kit.objects.Status;
import ssp.kit.objects.Status.Signal;
import ssp.kit.objects.Status.Urgency;
import ssp.kit.objects.User;
import ssp.kit.utilities.Utilities;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.InputType;
import android.text.format.DateUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class FragmentStatusView extends Fragment implements android.view.View.OnClickListener {
	public FragmentStatusView() {
	}

	private Status status;

	private TextView textViewStatusStatusAuthorFullName;
	private TextView textViewStatusStatusAuthorUsername;
	private TextView textViewStatusStatusSignal;
	private TextView textViewStatusStatusMessage;
	private TextView textViewStatusStatusMessageTapToEdit;
	private TextView textViewStatusStatusTimestamp;
	private Button buttonStatusStatusReplies;
	private TextView textViewUrgencyPointer;
	private Button buttonStatusStatusUrgencyLifeThreatening;
	private Button buttonStatusStatusUrgencyNotLifeThreatening;
	private Button buttonStatusHelp;
	private Button buttonStatusOkay;

	private int urgencyButtonsCheckMarkPadding;
	private int urgencyButtonsStandardPadding;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		View rootView = inflater.inflate(R.layout.fragment_status_view, container, false);

		this.textViewStatusStatusAuthorFullName = (TextView) rootView.findViewById(R.id.textViewStatusStatusAuthorFullName);
		
		this.textViewStatusStatusAuthorUsername = (TextView) rootView.findViewById(R.id.textViewStatusStatusAuthorUsername);
		
		this.textViewStatusStatusSignal = (TextView) rootView.findViewById(R.id.textViewStatusStatusSignal);

		this.textViewStatusStatusMessage = (TextView) rootView.findViewById(R.id.textViewStatusStatusMessage);
		this.textViewStatusStatusMessage.setClickable(true);
		this.textViewStatusStatusMessage.setOnClickListener(this);

		this.textViewStatusStatusMessageTapToEdit = (TextView) rootView.findViewById(R.id.textViewStatusStatusMessageTapToEdit);
		this.textViewStatusStatusMessageTapToEdit.setClickable(true);
		this.textViewStatusStatusMessageTapToEdit.setOnClickListener(this);

		this.textViewStatusStatusTimestamp = (TextView) rootView.findViewById(R.id.textViewStatusStatusTimestamp);

		this.buttonStatusStatusReplies = (Button) rootView.findViewById(R.id.buttonStatusStatusReplies);
		this.buttonStatusStatusReplies.setOnClickListener(this);

		this.textViewUrgencyPointer = (TextView) rootView.findViewById(R.id.textViewUrgencyPointer);
		
		this.buttonStatusStatusUrgencyLifeThreatening = (Button) rootView.findViewById(R.id.buttonStatusStatusUrgencyLifeThreatening);
		this.buttonStatusStatusUrgencyLifeThreatening.setOnClickListener(this);

		this.buttonStatusStatusUrgencyNotLifeThreatening = (Button) rootView.findViewById(R.id.buttonStatusStatusUrgencyNotLifeThreatening);
		this.buttonStatusStatusUrgencyNotLifeThreatening.setOnClickListener(this);

		this.urgencyButtonsCheckMarkPadding = this.buttonStatusStatusUrgencyLifeThreatening.getPaddingLeft();
		this.urgencyButtonsStandardPadding = this.buttonStatusStatusUrgencyLifeThreatening.getPaddingRight();

		this.buttonStatusHelp = (Button) rootView.findViewById(R.id.buttonStatusHelp);
		this.buttonStatusHelp.setOnClickListener(this);

		this.buttonStatusOkay = (Button) rootView.findViewById(R.id.buttonStatusOkay);
		this.buttonStatusOkay.setOnClickListener(this);

		this.refresh();
		
		return rootView;
	}
	
	@Override
	public void onResume() {
		super.onResume();
		this.refresh();
	}

	public void refresh() {
		User user = KIT.app().getUser();
		this.status = user.getStatus();

		// User identity
		this.textViewStatusStatusAuthorFullName.setText(user.getFullName());
		this.textViewStatusStatusAuthorUsername.setText(user.getUsername());
		
		// Status signal
		Signal statusSignal = this.status.getSignal();
		int statusSignalStringId;
		int statusSignalColor;
		if (statusSignal == Signal.HELP) {
			statusSignalStringId = R.string.textViewStatusStatusSignal_help_requested;
			statusSignalColor = R.color.status_help;
		} else {
			statusSignalStringId = R.string.textViewStatusStatusSignal_ok;
			if (this.status.getTimestamp() != null && Utilities.moreThan24hoursAgo(this.status.getTimestamp().getTime())) {
				statusSignalColor = R.color.status_attention;
			} else {
				statusSignalColor = R.color.status_okay;
			}
		}
		this.textViewStatusStatusSignal.setText(statusSignalStringId);
		this.textViewStatusStatusSignal.setBackgroundColor(getResources().getColor(statusSignalColor));

		// Status message
		String message = this.status.getMessage();
		if (this.status.getSignal() == Signal.OK) {
			if ((message != null) && (!message.isEmpty())) {
				this.textViewStatusStatusMessage.setText(message);
			} else {
				this.textViewStatusStatusMessage.setText(user.getFullName() + " " + getString(R.string.generalTextIsOK));
			}
			this.textViewStatusStatusMessageTapToEdit.setVisibility(View.GONE);
		} else {
			if ((message != null) && (!message.isEmpty())) {
				this.textViewStatusStatusMessage.setText(message);
			} else {
				this.textViewStatusStatusMessage.setText(R.string.textViewStatusMessageEnterAMessage);
			}
			this.textViewStatusStatusMessageTapToEdit.setVisibility(View.VISIBLE);
		}

		// Status timestamp
		if (this.status.getTimestamp() == null) {
			this.textViewStatusStatusTimestamp.setText(R.string.generalTextPublishing);
			this.buttonStatusStatusReplies.setEnabled(false);
		} else {
			this.textViewStatusStatusTimestamp.setText(DateUtils.getRelativeTimeSpanString(this.status.getTimestamp().getTime()));
			this.buttonStatusStatusReplies.setEnabled(true);
		}

		// Status replies
		int numberOfReplies = this.status.getReplies().size();
		switch (numberOfReplies) {
		case 0:
			this.buttonStatusStatusReplies.setText(R.string.buttonStatusStatusReplies_no_replies);
			break;
		case 1:
			this.buttonStatusStatusReplies.setText(Integer.toString(numberOfReplies) + " " + getString(R.string.buttonStatusStatusReplies_reply));
			break;
		default:
			this.buttonStatusStatusReplies.setText(Integer.toString(numberOfReplies) + " " + getString(R.string.buttonStatusStatusReplies_replies));
			break;
		}

		// Status urgency
		refreshUrgency();
		
		// Help button
		if (this.status.getSignal() == Signal.HELP) {
			this.buttonStatusHelp.setEnabled(false);
			this.buttonStatusOkay.setText(R.string.buttonStatusOkayNow);
		} else {
			this.buttonStatusHelp.setEnabled(true);
			this.buttonStatusOkay.setText(R.string.buttonStatusOkay);
		}
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
			case R.id.buttonStatusHelp:
				onClickButtonHelp();
				break;
			case R.id.buttonStatusOkay:
				onClickButtonOK();
				break;
			case R.id.textViewStatusStatusMessage:
			case R.id.textViewStatusStatusMessageTapToEdit:
				onClickTextViewStatusMessage();
				break;
			case R.id.buttonStatusStatusReplies:
				onClickButtonStatusReplies();
				break;
			case R.id.buttonStatusStatusUrgencyLifeThreatening:
				onClickButtonStatusUrgencyLifeThreatening();
				break;
			case R.id.buttonStatusStatusUrgencyNotLifeThreatening:
				onClickButtonStatusUrgencyNotLifeThreatening();
				break;
			default:
				return;
		}
	}

	private void onClickButtonHelp() {
		KIT.app().publishStatusHelp(new ServerResponseListener() {
			public void onServerResponded(Object data, Exception e) {
				FragmentStatusView.this.refresh();
			}
		});
		FragmentStatusView.this.refresh();
	}

	private void onClickButtonOK() {
		// Create dialog
		AlertDialog.Builder dialog = new AlertDialog.Builder(getActivity());
		dialog.setTitle(R.string.dialogTitleStatusOKMessage);

		// Message input
		final EditText editTextStatusOKMessage = new EditText(getActivity());
		editTextStatusOKMessage.setInputType(InputType.TYPE_CLASS_TEXT);
		editTextStatusOKMessage.setHint(R.string.editTextHintStatusOKMessage);
		dialog.setView(editTextStatusOKMessage);

		// Buttons
		dialog.setPositiveButton(R.string.generalTextPublish, new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int which) {
				String message = editTextStatusOKMessage.getText().toString();
				KIT.app().publishStatusOK(message, new ServerResponseListener() {
					public void onServerResponded(Object data, Exception e) {
						FragmentStatusView.this.refresh();
					}
				});
				FragmentStatusView.this.refresh();
			}
		});
		dialog.setNegativeButton(R.string.generalTextCancel, new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int which) {
				dialog.cancel();
			}
		});

		// Show dialog
		dialog.show();
	}

	private void onClickButtonStatusReplies() {
		Intent intent = new Intent(getActivity(), ActivityReplies.class);
		intent.putExtra(ActivityReplies.EXTRA_STATUS_AUTHOR_USERNAME, KIT.app().getUser().getUsername());
		startActivity(intent);
	}

	private void onClickTextViewStatusMessage() {
		if (this.status.getSignal() == Signal.OK) {
			return;
		} else {
			// Create dialog
			AlertDialog.Builder dialog = new AlertDialog.Builder(getActivity());
			dialog.setTitle(R.string.dialogTitleStatusEditMessage);

			// Message input
			final EditText editTextStatusEditMessage = new EditText(getActivity());
			editTextStatusEditMessage.setInputType(InputType.TYPE_CLASS_TEXT);
			String message = this.status.getMessage();
			if ((message != null) && (!message.isEmpty())) {
				editTextStatusEditMessage.setText(message);
			} else {
				editTextStatusEditMessage.setHint(R.string.editTextHintStatusEditMessage);
			}
			dialog.setView(editTextStatusEditMessage);

			// Buttons
			dialog.setPositiveButton("Publish", new DialogInterface.OnClickListener() {
				public void onClick(DialogInterface dialog, int which) {
					String message = editTextStatusEditMessage.getText().toString();
					KIT.app().updateStatusHelpMessage(message, new ServerResponseListener() {
						public void onServerResponded(Object data, Exception e) {
							FragmentStatusView.this.refresh();
						}
					});
					FragmentStatusView.this.refresh();
				}
			});
			dialog.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
				public void onClick(DialogInterface dialog, int which) {
					dialog.cancel();
				}
			});

			// Show dialog
			dialog.show();
		}
	}

	private void onClickButtonStatusUrgencyLifeThreatening() {
		KIT.app().updateStatusHelpUrgency(Urgency.LIFE_THREATENING, new ServerResponseListener() {
			public void onServerResponded(Object data, Exception e) {
				FragmentStatusView.this.refresh();
			}
		});
		FragmentStatusView.this.refresh();
	}

	private void onClickButtonStatusUrgencyNotLifeThreatening() {
		KIT.app().updateStatusHelpUrgency(Urgency.NON_LIFE_THREATENING, new ServerResponseListener() {
			public void onServerResponded(Object data, Exception e) {
				FragmentStatusView.this.refresh();
			}
		});
		FragmentStatusView.this.refresh();
	}

	private void refreshUrgency() {
		if (this.status.getSignal() == Signal.OK) {
			this.textViewUrgencyPointer.setVisibility(View.GONE);
			this.buttonStatusStatusUrgencyLifeThreatening.setVisibility(View.GONE);
			this.buttonStatusStatusUrgencyNotLifeThreatening.setVisibility(View.GONE);
		} else {
			this.textViewUrgencyPointer.setVisibility(View.VISIBLE);
			this.buttonStatusStatusUrgencyLifeThreatening.setVisibility(View.VISIBLE);
			this.buttonStatusStatusUrgencyNotLifeThreatening.setVisibility(View.VISIBLE);
			if (this.status.getUrgency() == Urgency.LIFE_THREATENING) {
				tickButtonStatusStatusUrgencyLifeThreatening();
				untickButtonStatusStatusUrgencyNotLifeThreatening();
			} else if (this.status.getUrgency() == Urgency.NON_LIFE_THREATENING) {
				untickButtonStatusStatusUrgencyLifeThreatening();
				tickButtonStatusStatusUrgencyNotLifeThreatening();
			} else {
				untickButtonStatusStatusUrgencyLifeThreatening();
				untickButtonStatusStatusUrgencyNotLifeThreatening();
			}
		}
	}
	
	private void tickButtonStatusStatusUrgencyLifeThreatening() {
		this.buttonStatusStatusUrgencyLifeThreatening.setTypeface(null, Typeface.BOLD);
		this.buttonStatusStatusUrgencyLifeThreatening.setEnabled(false);
		this.buttonStatusStatusUrgencyLifeThreatening.setPadding(urgencyButtonsStandardPadding, 0, 0, 0);
		this.buttonStatusStatusUrgencyLifeThreatening.setText(R.string.buttonStatusStatusUrgencyLifeThreateningActive);
	}
	
	private void untickButtonStatusStatusUrgencyLifeThreatening() {
		this.buttonStatusStatusUrgencyLifeThreatening.setTypeface(null, Typeface.NORMAL);
		this.buttonStatusStatusUrgencyLifeThreatening.setEnabled(true);
		this.buttonStatusStatusUrgencyLifeThreatening.setPadding(urgencyButtonsCheckMarkPadding, 0, 0, 0);
		this.buttonStatusStatusUrgencyLifeThreatening.setText(R.string.buttonStatusStatusUrgencyLifeThreateningInactive);
	}

	private void tickButtonStatusStatusUrgencyNotLifeThreatening() {
		this.buttonStatusStatusUrgencyNotLifeThreatening.setTypeface(null, Typeface.BOLD);
		this.buttonStatusStatusUrgencyNotLifeThreatening.setEnabled(false);
		this.buttonStatusStatusUrgencyNotLifeThreatening.setPadding(urgencyButtonsStandardPadding, 0, 0, 0);
		this.buttonStatusStatusUrgencyNotLifeThreatening.setText(R.string.buttonStatusStatusUrgencyNotLifeThreateningActive);
	}
	
	private void untickButtonStatusStatusUrgencyNotLifeThreatening() {
		this.buttonStatusStatusUrgencyNotLifeThreatening.setTypeface(null, Typeface.NORMAL);
		this.buttonStatusStatusUrgencyNotLifeThreatening.setEnabled(true);
		this.buttonStatusStatusUrgencyNotLifeThreatening.setPadding(urgencyButtonsCheckMarkPadding, 0, 0, 0);
		this.buttonStatusStatusUrgencyNotLifeThreatening.setText(R.string.buttonStatusStatusUrgencyNotLifeThreateningInactive);
	}

}
