package ssp.kit.ui;

import java.util.ArrayList;
import java.util.Date;

import ssp.kit.KIT;
import ssp.kit.R;
import ssp.kit.ServerResponseListener;
import ssp.kit.exception.InvalidUserException;
import ssp.kit.exception.StillLoadingException;
import ssp.kit.objects.Request;
import ssp.kit.objects.Request.RequestType;
import ssp.kit.objects.User;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.text.InputType;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

public class ActivityRelationSetting extends Activity {

	public static final String EXTRA_RELATION_SETTING_TYPE = "ssp.kit.relationSettingType";
	public static final int MANAGE_CARER = 0;
	public static final int MANAGE_ASSISTED_PERSONS = 1;
	
	private int relationSettingType;
	
	private View.OnClickListener onReceivedRequestEntryClickedListener;
	private View.OnClickListener onSentRequestEntryClickedListener;
	private View.OnClickListener onRemoveRelationEntryClickedListener;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_relation_setting);
		getActionBar().setDisplayHomeAsUpEnabled(true);
		
		Intent intent = getIntent();
		this.relationSettingType = intent.getIntExtra(EXTRA_RELATION_SETTING_TYPE, MANAGE_CARER);
		
		// Initialize data and Activity title based on relation setting type
		if (this.relationSettingType == MANAGE_CARER) {
			setTitle("Carers"); // TODO Magic number?
		} else {
			setTitle("Assisted Persons"); // TODO Magic number?
		}
		
		// Initialize onClick listeners
		this.onReceivedRequestEntryClickedListener = new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				final Request request = (Request) v.getTag();
				String requesterName = request.getRequester();
				String relationRole = relationSettingType == MANAGE_CARER?"carer":"assisted person";
				
				// Prepare dialog box
				AlertDialog.Builder dialog = new AlertDialog.Builder(ActivityRelationSetting.this);
				dialog.setTitle("Accept "+requesterName+" as "+relationRole+"?");
				
				// Set dialog box buttons
				dialog.setPositiveButton(R.string.dialogRespondReceivedRelationRequestPositiveButton, new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int which) {
						KIT.app().approveRequest(ActivityRelationSetting.this, request, new ServerResponseListener() {
							@Override
							public void onServerResponded(Object data, Exception e) {
								if (e == null) {
									displayRelationRequests();
									displayRelations();
								} else {
									Toast.makeText(getApplicationContext(), e.getLocalizedMessage(), Toast.LENGTH_LONG).show();
								}
							}
						});
						displayRelationRequests();
						displayRelations();
						Toast.makeText(getApplicationContext(), "Accepted request from "+request.getRequester(), Toast.LENGTH_SHORT).show();
					}
				});
				dialog.setNeutralButton(R.string.dialogRespondReceivedRelationRequestNeutralButton, new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int which) {
						KIT.app().cancelRequest(request, new ServerResponseListener() {
							@Override
							public void onServerResponded(Object data, Exception e) {
								if (e == null) {
									displayRelationRequests();
								} else {
									Toast.makeText(getApplicationContext(), e.getLocalizedMessage(), Toast.LENGTH_LONG).show();
								}
							}
						});
						displayRelationRequests();
						Toast.makeText(getApplicationContext(), "Declined request from "+request.getRequester(), Toast.LENGTH_SHORT).show();
					}
				});
				dialog.setNegativeButton(R.string.dialogRespondReceivedRelationRequestNegativeButton, new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int which) {
						dialog.dismiss();
					}
				});
				
				// Show dialog box
				dialog.show();
			}
		};
		
		this.onSentRequestEntryClickedListener = new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				final Request request = (Request) v.getTag();
				String targetedName = request.getTargeted();
				String relationRole = relationSettingType == MANAGE_CARER?"carer":"assisted person";
				
				// Prepare dialog box
				AlertDialog.Builder dialog = new AlertDialog.Builder(ActivityRelationSetting.this);
				dialog.setTitle("Cancel request to "+targetedName+" as "+relationRole+"?");
				
				// Set dialog box buttons
				dialog.setPositiveButton(R.string.dialogRespondSentRelationRequestPositiveButton, new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int which) {
						Toast.makeText(getApplicationContext(), "Canceling request to "+request.getTargeted(), Toast.LENGTH_SHORT).show();

						KIT.app().cancelRequest(request, new ServerResponseListener() {
							@Override
							public void onServerResponded(Object data, Exception e) {
								if (e == null) {
									displayRelationRequests();
									Toast.makeText(getApplicationContext(), "Canceled request to "+request.getTargeted(), Toast.LENGTH_SHORT).show();
								} else if (e instanceof StillLoadingException) {
									Toast.makeText(getApplicationContext(), "App is busy. Please try again later.", Toast.LENGTH_SHORT).show();
								} else {
									Toast.makeText(getApplicationContext(), e.getLocalizedMessage(), Toast.LENGTH_LONG).show();
								}
							}
						});
					}
				});
				dialog.setNegativeButton(R.string.dialogRespondSentRelationRequestNegativeButton, new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int which) {
						dialog.dismiss();
					}
				});
				
				// Show dialog box
				dialog.show();
			}
		};
		
		this.onRemoveRelationEntryClickedListener = new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				final User relation = (User) v.getTag();
				String relationName = relation.getUsername();
				final String relationRole = relationSettingType == MANAGE_CARER?"carer":"assisted person";
				
				// Prepare dialog box
				AlertDialog.Builder dialog = new AlertDialog.Builder(ActivityRelationSetting.this);
				dialog.setTitle("Remove "+relationName+" as "+relationRole+"?");
				
				// Set dialog box buttons
				dialog.setPositiveButton(R.string.dialogDeleteRelationPositiveButton, new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int which) {
						// Prepare listener
						ServerResponseListener listener = new ServerResponseListener() {
							@Override
							public void onServerResponded(Object data, Exception e) {
								if (e == null) {
									displayRelations();
								} else {
									Toast.makeText(getApplicationContext(), e.getLocalizedMessage(), Toast.LENGTH_LONG).show();
								}
							}
						};
						
						// Perform relation deletion as appropriate
						if (ActivityRelationSetting.this.relationSettingType == ActivityRelationSetting.MANAGE_CARER) {
							KIT.app().deleteCarer(relation.getUsername(), listener);
						} else {
							KIT.app().deleteAssisted(ActivityRelationSetting.this, relation.getUsername(), listener);
						}
						
						displayRelations();
						Toast.makeText(getApplicationContext(), "Deleted "+relation.getUsername()+" from "+relationRole, Toast.LENGTH_SHORT).show();
					}
				});
				dialog.setNegativeButton(R.string.dialogDeleteRelationNegativeButton, new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int which) {
						dialog.dismiss();
					}
				});
				
				// Show dialog box
				dialog.show();
			}
		};
		
		// Set onClick listener for the "Add New" button
		View buttonAddNew = findViewById(R.id.buttonAddRelation);
		buttonAddNew.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				// Prepare dialog box
				AlertDialog.Builder dialog = new AlertDialog.Builder(ActivityRelationSetting.this);
				dialog.setTitle("Add "+(relationSettingType == MANAGE_CARER?"carer":"assisted person"));
				
				// Set dialog box contents: username input
				final EditText editTextAddNewRelation = new EditText(ActivityRelationSetting.this);
				editTextAddNewRelation.setInputType(InputType.TYPE_CLASS_TEXT);
				editTextAddNewRelation.setHint(R.string.dialogAddNewRelationUsernameEditTextHint);
				dialog.setView(editTextAddNewRelation);
				
				// Set dialog box buttons
				dialog.setPositiveButton(R.string.dialogAddNewRelationPositiveButton, new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int which) {
						String requester = KIT.app().getUser().getUsername();
						String targeted = editTextAddNewRelation.getText().toString();
						RequestType requestType = ActivityRelationSetting.this.relationSettingType == MANAGE_CARER?RequestType.BE_MY_CARER:RequestType.BE_MY_ASSISTED;
						
						KIT.app().sendRequest(requester, targeted, requestType, new Date(), new ServerResponseListener() {
							@Override
							public void onServerResponded(Object data, Exception e) {
								displayRelationRequests();
								if (e instanceof StillLoadingException) {
									Toast.makeText(getApplicationContext(), "App is busy. Please try again later.", Toast.LENGTH_SHORT).show();
								} else if (e instanceof InvalidUserException) {
									Toast.makeText(getApplicationContext(), "Invalid username", Toast.LENGTH_SHORT).show();
								} else if (e != null) {
									Toast.makeText(getApplicationContext(), e.getLocalizedMessage(), Toast.LENGTH_SHORT).show();
								}
							}
						});
						displayRelationRequests();
					}
				});
				dialog.setNegativeButton(R.string.dialogAddNewRelationNegativeButton, new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int which) {
						dialog.dismiss();
					}
				});
				
				// Show dialog box
				dialog.show();
			}
		});
		
		// Display data
		displayRelationRequests();
		displayRelations();
	}
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.activity_relation_setting, menu);
		return super.onCreateOptionsMenu(menu);
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
	    // Handle presses on the action bar items
	    switch (item.getItemId()) {
	        case R.id.action_refresh_list:
	        	refreshAll();
	            return true;
	        default:
	            return super.onOptionsItemSelected(item);
	    }
	}	
	
	private void refreshAll() {
		refreshRelationRequests();
		refreshRelations();
	}
	
	private void refreshRelationRequests() {
		KIT.app().loadAllRequests(new ServerResponseListener() {
			@Override
			public void onServerResponded(Object data, Exception e) {
				if (e == null) {
					displayRelationRequests();
				} else {
					Toast.makeText(getApplicationContext(), e.getLocalizedMessage(), Toast.LENGTH_LONG).show();
				}
			}
		});
		displayRelationRequests();
	}
	
	private void displayRelationRequests() {
		// Clears the list before re-populating list
		LinearLayout relationRequestList = (LinearLayout) findViewById(R.id.linearLayoutRelationRequestList);
		relationRequestList.removeAllViews();
		
		// Fetch received requests data
		RequestType requestType = this.relationSettingType == MANAGE_CARER?RequestType.BE_MY_ASSISTED:RequestType.BE_MY_CARER;
		ArrayList<Request> relationRequests = KIT.app().getReceivedRequests(requestType);
		
		// Re-populate received requests list
		for (Request request : relationRequests) {
			// Inflate View
			View requestEntry = getLayoutInflater().inflate(R.layout.activity_relation_setting_received_request_list_entry, relationRequestList, false);
			
			// Fill in entry details
			TextView requestFullName = (TextView) requestEntry.findViewById(R.id.textViewRelationSettingReceivedRequestListEntryFullName);
			TextView requestMessage = (TextView) requestEntry.findViewById(R.id.textViewRelationSettingReceivedRequestListEntryRequestMessage);
			requestFullName.setText(request.getRequester());
			requestMessage.setText(request.getRequestMessage());
			
			// Set listener
			requestEntry.setTag(request); // Attach request as tag on the View (used in listener)
			requestEntry.setOnClickListener(this.onReceivedRequestEntryClickedListener);
			
			// Attach relation entry
			relationRequestList.addView(requestEntry);
		}
		
		// Fetch sent requests data
		requestType = this.relationSettingType == MANAGE_CARER?RequestType.BE_MY_CARER:RequestType.BE_MY_ASSISTED;
		relationRequests = KIT.app().getSentRequests(requestType);
		
		// Re-populate sent requests list
		for (Request request : relationRequests) {
			// Inflate View
			View requestEntry = getLayoutInflater().inflate(R.layout.activity_relation_setting_sent_request_list_entry, relationRequestList, false);
			
			// Fill in entry details
			TextView requestUsername = (TextView) requestEntry.findViewById(R.id.textViewRelationSettingSentRequestListEntryUsername);
			requestUsername.setText(request.getTargeted());
			
			// Set listener
			requestEntry.setTag(request); // Attach request as tag on the View (used in listener)
			requestEntry.setOnClickListener(this.onSentRequestEntryClickedListener);
			
			// Attach relation entry
			relationRequestList.addView(requestEntry);
		}
	}
	
	private void refreshRelations() {
		// TODO Need to always refresh from server?
		KIT.app().loadRelations(this, new ServerResponseListener() {
			@Override
			public void onServerResponded(Object data, Exception e) {
				if (e == null) {
					displayRelations();
				} else {
					Toast.makeText(getApplicationContext(), e.getLocalizedMessage(), Toast.LENGTH_LONG).show();
				}
			}
		});
		displayRelations();
	}
	
	private void displayRelations() {
		// Fetch relations data
		ArrayList<User> relations;
		if (this.relationSettingType == MANAGE_CARER) {
			relations = KIT.app().getCarers();
		} else {
			relations = KIT.app().getAssisteds();
		}
		
		LinearLayout relationList = (LinearLayout) findViewById(R.id.linearLayoutRelationList);
		
		// Clears the list before re-populating list
		relationList.removeAllViews();
		
		// Re-populate list
		for (User relation : relations) {
			// Inflate view
			View relationEntry = getLayoutInflater().inflate(R.layout.activity_relation_setting_list_entry, relationList, false);
			
			// Fill in entry details
			TextView relationUsername = (TextView) relationEntry.findViewById(R.id.textViewRelationSettingListEntryUsername);
			TextView relationFullName = (TextView) relationEntry.findViewById(R.id.textViewRelationSettingListEntryFullName);
			relationUsername.setText(relation.getUsername());
			relationFullName.setText(relation.getFullName());
			
			// Set listener
			View removeRelation = relationEntry.findViewById(R.id.textViewRemoveRelationListSettingEntry);
			removeRelation.setTag(relation); // Attach relation as tag on the View (used in listener)
			removeRelation.setOnClickListener(this.onRemoveRelationEntryClickedListener);
			
			// Attach relation entry
			relationList.addView(relationEntry);
		}
	}

}
