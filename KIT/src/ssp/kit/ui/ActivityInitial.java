package ssp.kit.ui;

import ssp.kit.KIT;
import ssp.kit.R;
import ssp.kit.ServerResponseListener;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;

public class ActivityInitial extends Activity {
	
	public static final String EXTRA_NEXT_ACTIVITY = "ssp.kit.nextActivity";
	public static final String NEXT_ACTIVITY_REPLIES = "ssp.kit.nextActivityReplies";
	public static final String NEXT_ACTIVITY_REPLIES_USERNAME = "ssp.kit.nextActivityRepliesUsername";
	
	private ProgressDialog progressDialog;
	
	private Class<?> defaultActivity = ActivityLogin.class;
	private Class<?> nextActivity = ActivityMain.class;
	
	private Intent intent;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		intent = getIntent();
		if (intent != null && intent.hasExtra(EXTRA_NEXT_ACTIVITY)) {
			if (intent.getStringExtra(EXTRA_NEXT_ACTIVITY).equals(NEXT_ACTIVITY_REPLIES)) {
				nextActivity = ActivityReplies.class;
			}
		}
		
		if (KIT.app().isLoggedIn()) {
			goToActivity(nextActivity);
		} else if (KIT.app().isCachedOnDisk()) {
			KIT.app().loadEverything(this, new ServerResponseListener() {
				@Override				
				public void onServerResponded(Object data, Exception e) {
					dismissLoading();
					if (e == null) {
						goToActivity(nextActivity);
					} else {
						goToActivity(defaultActivity);
					}
				}
			});
			showLoading();
		} else {
			goToActivity(defaultActivity);
		}
		
		return;
	}
	
	private void goToActivity(Class<?> activityClass) {
		Intent toActivityIntent = new Intent(this, activityClass);
		if (activityClass == ActivityReplies.class) {
			toActivityIntent.putExtra(ActivityReplies.EXTRA_STATUS_AUTHOR_USERNAME, intent.getStringExtra(NEXT_ACTIVITY_REPLIES_USERNAME));
		}
		startActivity(toActivityIntent);
		this.finish();
	}

	
	private void showLoading() {
		this.progressDialog = new ProgressDialog(this);
		this.progressDialog.setMessage(getString(R.string.generalTextLoading));
		this.progressDialog.setCancelable(false);
		this.progressDialog.setCanceledOnTouchOutside(false);
		this.progressDialog.show();
	}
	
	private void dismissLoading() {	
		if (this.progressDialog != null) {
			this.progressDialog.dismiss();
		}
	}
	
}
