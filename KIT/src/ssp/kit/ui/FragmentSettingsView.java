package ssp.kit.ui;

import ssp.kit.KIT;
import ssp.kit.R;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.Toast;

public class FragmentSettingsView extends Fragment implements
		android.view.View.OnClickListener {
	public FragmentSettingsView() {
	}
	
	private RelativeLayout relativeLayoutCarerSetting;
	private RelativeLayout relativeLayoutAssistedSetting;
//	private RelativeLayout relativeLayoutRemindersSetting;
//	private RelativeLayout relativeLayoutNotificationsSetting;
	private RelativeLayout relativeLayoutAccountSetting;
	private RelativeLayout relativeLayoutAboutSetting;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View rootView = inflater.inflate(R.layout.fragment_settings_view,
				container, false);
		
		this.relativeLayoutCarerSetting = (RelativeLayout) rootView.findViewById(R.id.relativeLayoutCarerSetting);
		this.relativeLayoutCarerSetting.setOnClickListener(this);
		
		this.relativeLayoutAssistedSetting = (RelativeLayout) rootView.findViewById(R.id.relativeLayoutAssistedSetting);
		this.relativeLayoutAssistedSetting.setOnClickListener(this);
		
//		this.relativeLayoutRemindersSetting = (RelativeLayout) rootView.findViewById(R.id.relativeLayoutRemindersSetting);
//		this.relativeLayoutRemindersSetting.setOnClickListener(this);
//		
//		this.relativeLayoutNotificationsSetting = (RelativeLayout) rootView.findViewById(R.id.relativeLayoutNotificationsSetting);
//		this.relativeLayoutNotificationsSetting.setOnClickListener(this);
		
		this.relativeLayoutAccountSetting = (RelativeLayout) rootView.findViewById(R.id.relativeLayoutAccountSetting);
		this.relativeLayoutAccountSetting.setOnClickListener(this);
		
		this.relativeLayoutAboutSetting = (RelativeLayout) rootView.findViewById(R.id.relativeLayoutAboutSetting);
		this.relativeLayoutAboutSetting.setOnClickListener(this);

		return rootView;
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.relativeLayoutCarerSetting:
			onClickCarerSetting();
			break;
		case R.id.relativeLayoutAssistedSetting:
			onClickAssistedSetting();
			break;
//		case R.id.relativeLayoutRemindersSetting:
//			onClickRemindersSetting();
//			break;
//		case R.id.relativeLayoutNotificationsSetting:
//			onClickNotificationsSetting();
//			break;
		case R.id.relativeLayoutAccountSetting:
			onClickAccountSetting();
			break;
		case R.id.relativeLayoutAboutSetting:
			onClickAboutSetting();
			break;
		default:
			return;
		}
	}
	
	private void onClickCarerSetting() {
		// Move to relation setting activity
		Intent intent = new Intent(getActivity(), ActivityRelationSetting.class);
		intent.putExtra(ActivityRelationSetting.EXTRA_RELATION_SETTING_TYPE, ActivityRelationSetting.MANAGE_CARER);
		startActivity(intent);
	}
	
	private void onClickAssistedSetting() {
		// Move to relation setting activity
		Intent intent = new Intent(getActivity(), ActivityRelationSetting.class);
		intent.putExtra(ActivityRelationSetting.EXTRA_RELATION_SETTING_TYPE, ActivityRelationSetting.MANAGE_ASSISTED_PERSONS);
		startActivity(intent);
	}
	
//	private void onClickRemindersSetting() {
//		Toast.makeText(getActivity(), "Reminders setting", Toast.LENGTH_SHORT).show();
//	}
//	
//	private void onClickNotificationsSetting() {
//		Toast.makeText(getActivity(), "Notifications setting", Toast.LENGTH_SHORT).show();
//	}
	
	private void onClickAccountSetting() {
		Toast.makeText(getActivity(), "Logging out...", Toast.LENGTH_SHORT).show();
		
		// TODO Change this implementation later on. For now this just logs the user out
		KIT.app().logout(getActivity());
		Intent toInitialIntent = new Intent(getActivity(), ActivityInitial.class);
		startActivity(toInitialIntent);
		getActivity().finish();
	}
	
	private void onClickAboutSetting() {
		// Move to about setting activity
		Intent intent = new Intent(getActivity(), ActivityAboutSetting.class);
		startActivity(intent);
	}
}
