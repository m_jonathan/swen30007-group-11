package ssp.kit.objects;

import java.util.Date;

import ssp.kit.communication.Symbols;
import ssp.kit.exception.IllegalRequestException;



public class Request {
	public enum RequestType {
		BE_MY_CARER, BE_MY_ASSISTED
	}
	
	private String      requester;
	private String      targeted;
	private RequestType type;
	private Date        timestamp;
	
	public Request(String requester, String targeted, RequestType type, Date timestamp) {
		this.requester = requester;
		this.targeted  = targeted;
		this.type      = type;
		this.timestamp = timestamp;
	}
	
	public boolean isSentTo(String username) {
		return username.equals(targeted);
	}
	
	/** Check whether it's the same request. */
	public boolean sameAs(Request r) {
		if(r == null) {
			return false;
		}
		if(! targeted.equals(r.targeted)) {
			return false;
		}
		if(!requester.equals(r.requester)) {
			return false;
		}
		if(!     type.equals(r.type)) {
			return false;
		}
		return true;
	}
	
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (!(obj instanceof Request))
			return false;
		Request other = (Request) obj;
		if (requester == null) {
			if (other.requester != null)
				return false;
		} else if (!requester.equals(other.requester))
			return false;
		if (targeted == null) {
			if (other.targeted != null)
				return false;
		} else if (!targeted.equals(other.targeted))
			return false;
		if (timestamp == null) {
			if (other.timestamp != null)
				return false;
		} else if (!timestamp.equals(other.timestamp))
			return false;
		if (type != other.type)
			return false;
		return true;
	}
	
	///////////////
	// Accessors //
	///////////////

	public Request clone() {
		return new Request(requester, targeted, type, getTimestamp());
	}
	
	public String getOtherUser(String username) throws IllegalRequestException {
		if(targeted.equals(username) && !requester.equals(username)) {
			return requester;
		}
		if(!targeted.equals(username) && requester.equals(username)) {
			return targeted;
		}
		throw new IllegalRequestException();
	}
	
	public String getRequestMessage() {
		switch(type) {
			case BE_MY_CARER:
				return requester + Symbols.BMCreqmsg;
			case BE_MY_ASSISTED:
				return requester + Symbols.BMAreqmsg;
			default:
				return null;
		}
	}
	
	public String getApprovalMessage() {
		switch(type) {
			case BE_MY_CARER:
				return targeted + Symbols.BMCappmsg;
			case BE_MY_ASSISTED:
				return targeted + Symbols.BMAappmsg;
			default:
				return null;
		}
	}
	
	public String getCarer() {
		switch(type) {
			case BE_MY_CARER:
				return targeted;
			case BE_MY_ASSISTED:
				return requester;
			default:
				return null;
		}
	}
	
	public String getAssisted() {
		switch(type) {
			case BE_MY_CARER:
				return requester;
			case BE_MY_ASSISTED:
				return targeted;
			default:
				return null;
		}
	}
	
	public String getRequester() {
		return requester;
	}
	
	public String getTargeted() {
		return targeted;
	}
	
	public RequestType getType() {
		return type;
	}
	
	public Date getTimestamp() {
		return (timestamp == null) ? null : new Date(timestamp.getTime());
	}
}
