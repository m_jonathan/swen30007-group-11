package ssp.kit.objects;

import java.util.Date;

import ssp.kit.communication.Symbols;



public class Reply {
	private Date   timestamp;
	private String username;
	private String fullname;
	private String message;

	public Reply(Date timestamp, String username, String fullname, String message) {
		this.timestamp = timestamp;
		this.username  = username;
		this.fullname  = fullname;
		this.message   = message;
	}
	
	
	///////////////
	// Accessors //
	///////////////
	
	/** Return a deep copy of the object. */
	public Reply clone() {
		return new Reply(getTimestamp(), getUsername(), getFullName(), message);
	}
	
	public String getPushMessage() {
		return fullname + Symbols.replymsg + message;
	}
	
	/** Return a deep copy of the timestamp. */
	public Date getTimestamp() {
		return (timestamp == null) ? null : new Date(timestamp.getTime());
	}
		
	/** Return the reply's author's username. */
	public String getUsername() {
		return username;
	}
	
	/** Return the reply's author's fullname. */
	public String getFullName() {
		return fullname;
	}
	/** Return the reply's message. */
	public String getMessage() {
		return message;
	}
	
}
