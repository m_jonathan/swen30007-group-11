package ssp.kit.objects;

import java.util.ArrayList;
import java.util.Date;

import ssp.kit.ServerResponseListener;
import ssp.kit.communication.Communication;
import ssp.kit.communication.Symbols;

public class Status {
	public enum Signal {
		OK, HELP
	}
	public enum Urgency {
		LIFE_THREATENING, NON_LIFE_THREATENING
	}
	
	private Date    timestamp;
	private String  username;
	private String  fullname;
	private String  message;
	private Signal  signal;
	private Urgency urgency;
	private ArrayList<Reply> replies;
	
	public Status() {
		this.replies = new ArrayList<Reply>();
	}
	
	public Status(String username, String fullname) {
		this.timestamp = null;
		this.username  = username;
		this.fullname  = fullname;
		this.message   = Symbols.defaultmsg;
		this.signal    = Signal.OK;
		this.urgency   = null;
		this.replies   = new ArrayList<Reply>();
	}
	
	public Status(Date timestamp,
				  String username,
				  String fullname,
                  String message,
                  Signal signal,
                  Urgency urgency) {
		this.timestamp = timestamp;
		this.username  = username;
		this.fullname  = fullname;
		this.message   = message;
		this.signal    = signal;
		this.urgency   = urgency;
		this.replies   = new ArrayList<Reply>();
	}
	
	public Status(Date timestamp,
				  String username,
				  String fullname,
	              String message,
	              Signal signal,
	              Urgency urgency,
	              ArrayList<Reply> replies) {
		this.timestamp = timestamp;
		this.username  = username;
		this.fullname  = fullname;
		this.message   = message;
		this.signal    = signal;
		this.urgency   = urgency;
		this.replies   = replies;
	}
	
	
	/** Check whether it's the same status, excluding timestamp, urgency and replies. */
	public boolean sameAs(Status s) {
		if(s == null) {
			return false;
		}
		if(!username.equals(s.username)) {
			return false;
		}
		if(!fullname.equals(s.fullname)) {
			return false;
		}
		if(! message.equals(s.message)) {
			return false;
		}
		if(!  signal.equals(s.signal)) {
			return false;
		}
		return true;
	}
	
	
	/** Reloads the replies list for the status. */
	public void loadReplies(final Communication comm, final ServerResponseListener listener) {
		comm.getReplies(this, new ServerResponseListener() {
			@SuppressWarnings("unchecked")
			public void onServerResponded(Object o, Exception e) {
				if(o instanceof ArrayList) {
					replies = (ArrayList<Reply>) o;
					
					listener.onServerResponded(new Object(), null);
				} else {
					listener.onServerResponded(new Object(), e);
				}
			}
		});
		
		return;
	}
	
	
	//////////////////
	// Cache Update // 
	//////////////////
	
	/** Updates the status's message and signal to the new status.
	 * The timestamp becomes null, and the replies list becomes empty.
	 */
	public void updateCacheStatus(Status status) {
		this.timestamp = null;
		this.message   = status.getMessage();
		this.signal    = status.getSignal();
		this.urgency   = status.getUrgency();
		this.replies   = new ArrayList<Reply>();
		
		return;
	}
	
	/** Updates the status's message and signal to the new status.
	 * The timestamp becomes null, but the replies list is preserved.
	 */
	public void updateCacheStatusPreserveReplies(Status status) {
		this.timestamp = null;
		this.message   = status.getMessage();
		this.signal    = status.getSignal();
		this.urgency   = status.getUrgency();
		
		return;
	}
	
	/** Changes the status's urgency. */
	public void setCacheUrgency(Urgency urgency) {
		this.urgency = urgency;
		return;
	}
	
	/** Adds a new reply to the status. */
	public void addCacheReply(Reply reply) {
		replies.add(reply);
		return;
	}
	
	
	///////////////
	// Accessors //
	///////////////
	
	/** Return a deep copy of the object. */
	public Status clone() {
		return new Status(getTimestamp(), username, fullname, message, signal, urgency, getReplies());
	}
	
	public String getPushMessage() {
		switch(signal) {
			case OK:
				if(message.equals("")) {
					return fullname + Symbols.oknomsg;
				} else {
					return fullname + Symbols.okmsg + message;
				}
			case HELP:
				if(message.equals("")) {
					return fullname + Symbols.helpnomsg;
				} else {
					return fullname + Symbols.helpmsg + message;
				}
			default:
				return fullname + Symbols.statusmsg + message;
		}
	}
	
	/** Return a deep copy of the timestamp. */
	public Date getTimestamp() {
		return (timestamp == null) ? null : new Date(timestamp.getTime());
	}
	
	/** Return the author's username. */
	public String getUsername() {
		return username;
	}
	
	/** Return the author's fullname. */
	public String getFullName() {
		return fullname;
	}
	
	/** Return the status's message. */
	public String getMessage() {
		return message;
	}
	
	/** Return the status's signal. */
	public Signal getSignal() {
		return signal;
	}
	
	/** Return the status's urgency. */
	public Urgency getUrgency() {
		return urgency;
	}
	
	/** Return a deep copy of the replies list. */
	public ArrayList<Reply> getReplies() {
		ArrayList<Reply> deepcopy = new ArrayList<Reply>();
		
		for (Reply reply : replies) {
			if(reply != null) {
				deepcopy.add(reply.clone());
			}
		}
		
		return deepcopy;
	}
	
}
