package ssp.kit.objects;


import ssp.kit.ServerResponseListener;
import ssp.kit.communication.Communication;

public class User {
	private String username;
	private String fullname;
	private Status status;
	
	public User(String username) {
		this.username = username;
		this.fullname = "";
		this.status   = new Status();
	}
	
	public User(String username, String fullname) {
		this.username = username;
		this.fullname = fullname;
		this.status   = new Status();
	}
	
	public User(String username, String fullname, Status status) {
		this.username = username;
		this.fullname = fullname;
		this.status   = status;
	}
	
	
	/** Reloads both the user's status and its replies. */
	public void loadStatusReplies(final Communication comm, final ServerResponseListener listener) {
		comm.getStatus(this, new ServerResponseListener() {
			public void onServerResponded(Object o, Exception e) {
				if(o instanceof Status) {
					status = (Status) o;
					
					status.loadReplies(comm, listener);
				} else {
					listener.onServerResponded(new Object(), e);
				}
			}
		});
		
		return;
	}
	
	/** Reloads just the status for the user. Doesn't include replies. */
	public void loadStatus(final Communication comm, final ServerResponseListener listener) {
		comm.getStatus(this, new ServerResponseListener() {
			public void onServerResponded(Object o, Exception e) {
				if(o instanceof Status) {
					status = (Status) o;
					
					listener.onServerResponded(new Object(), null);
				} else {
					listener.onServerResponded(new Object(), e);
				}
			}
		});
		
		return;
	}
	
	
	//////////////////
	// Cache Update // 
	//////////////////
	
	/** Changes the user's real name. */
	public void updateCacheFullName(String fullname) {
		this.fullname = fullname;
		return;
	}
	
	/** Changes the user's status. */
	public void updateCacheStatus(Status status) {
		this.status.updateCacheStatus(status);
		return;
	}
	
	/** Changes the user's status, but preserve replies. */
	public void updateCacheStatusPreserveReplies(Status status) {
		this.status.updateCacheStatusPreserveReplies(status);
		return;
	}
	
	/** Changes the user's status's urgency. */
	public void updateCacheUrgency(Status.Urgency urgency) {
		this.status.setCacheUrgency(urgency);
		return;
	}
	
	/** Adds a new reply to this user's status. */
	public void addCacheReply(Reply reply) {
		status.addCacheReply(reply);
		return;
	}
	
	
	///////////////
	// Accessors //
	///////////////
	
	/** Return a deep copy of the object. */
	public User clone() {
		return new User(username, fullname, getStatus());
	}
	
	/** Return the user's username. */
	public String getUsername() {
		return username;
	}
	
	/** Return the user's real name. */
	public String getFullName() {
		return fullname;
	}
	
	/** Return a deep copy of the user's status. */
	public Status getStatus() {
		return (status == null) ? null : status.clone();
	}
	
}
