package ssp.kit;

import java.util.ArrayList;
import java.util.Date;

import ssp.kit.communication.Account;
import ssp.kit.communication.Communication;
import ssp.kit.communication.Symbols;
import ssp.kit.exception.InvalidStatusException;
import ssp.kit.exception.UserNotFoundException;
import ssp.kit.objects.Reply;
import ssp.kit.objects.Request;
import ssp.kit.objects.Request.RequestType;
import ssp.kit.objects.Status;
import ssp.kit.objects.Status.Signal;
import ssp.kit.objects.Status.Urgency;
import ssp.kit.objects.User;
import android.content.Context;

public class KIT {

	private static KIT instance = null;
	private Account account;
	
	/**
	 * Gets the Singleton KIT instance.
	 * 
	 * @return KIT Singleton object.
	 */
	public static KIT app() {
		if (instance == null) {
			instance = new KIT();
		}
		return instance;
	}

	/**
	 * Optional initialization with injected Account module.
	 * 
	 * @return True if KIT is successfully initialized with the injected
	 * account module; False if KIT has been initialized beforehand.
	 */
	public static boolean init(Account account) {
		if (instance == null) {
			instance = new KIT(account);
			return true;
		}
		return false;
	}
	
	/**
	 * Deletes the current instance of KIT.
	 * 
	 * @return True if the KIT instance is successfully deleted,
	 * False if KIT has not been initialized.
	 */
	public static boolean reset() {
		if (instance != null) {
			instance = null;
			return true;
		}
		return false;
	}

	/**
	 * Constructor with default Account (and Communication) modules.
	 * This should not be publicly accessible, as in the Singleton
	 * design pattern.
	 */
	private KIT() {
		this.account = new Account(Communication.instance());
	}
	
	/**
	 * Constructor allowing injected Account module. This should not be
	 * publicly accessible, as in the Singleton design pattern.
	 */
	private KIT(Account account) {
		this.account = account;
	}

	// ////////////
	// Accessors //
	// ////////////

	/** Return a deep copy of the account's user. */
	public User getUser() {
		return account.getUser();
	}

	/** Return a deep copy of a requested carer. */
	public User getCarer(String username) {
		return account.getCarer(username);
	}

	/** Return a deep copy of a requested assisted person. */
	public User getAssistedPerson(String username) {
		return account.getAssistedPerson(username);
	}

	/** Return a deep copy of the user's carers list. */
	public ArrayList<User> getCarers() {
		return account.getCarers();
	}

	/** Return a deep copy of the user's assisted persons list. */
	public ArrayList<User> getAssisteds() {
		return account.getAssisteds();
	}
	
	/** Returns true if an active connection is present. */
	public boolean isLoggedIn() {
		return this.account.isLoggedIn();
	}
	
	/** Returns true if an authenticated user is cached on disk. */
	public boolean isCachedOnDisk() {
		return this.account.isCachedOnDisk();
	}
	
	/** Return requests of the specified request type sent by the user. */
	public ArrayList<Request> getSentRequests(Request.RequestType requestType) {
		ArrayList<Request> sentRequests = new ArrayList<Request>();
		ArrayList<Request> allRequests = account.getRequests();
		String currentUserUsername = getUser().getUsername();
		
		for (Request request : allRequests) {
			// Skip the request if this is not a sent request
			if (!request.getRequester().equals(currentUserUsername)) {
				continue;
			}
			
			// Check if the request is of correct request type
			if (request.getType().equals(requestType)) {
				sentRequests.add(request.clone());
			}
		}
		
		return sentRequests;
	}
	
	/** Return requests of the specified request type sent to the user. */
	public ArrayList<Request> getReceivedRequests(Request.RequestType requestType) {
		ArrayList<Request> receivedRequests = new ArrayList<Request>();
		ArrayList<Request> allRequests = account.getRequests();
		String currentUserUsername = getUser().getUsername();
		
		for (Request request : allRequests) {
			// Skip the request if this is not a received request
			if (request.getRequester().equals(currentUserUsername)) {
				continue;
			}
			
			// Check if the request is of correct request type
			if (request.getType().equals(requestType)) {
				receivedRequests.add(request.clone());
			}
		}
		
		return receivedRequests;
	}
	
	///////////////////////////
	// Account Re-population //
	///////////////////////////
	
	/** Reloads everything except the assisted persons' replies. */
	public void loadEverything(Context context, ServerResponseListener listener) {
		account.loadEverything(context, listener);
	}
	
	/** Reloads the user's status and replies. */
	public void loadUser(ServerResponseListener listener) {
		account.loadUser(listener);
	}
	
	/** Reloads the relations' names, as well as the statuses of all assisted persons. */
	public void loadRelations(Context context, ServerResponseListener listener) {
		account.loadRelations(context, listener);
	}
	
	/** Reloads the status and replies of the assisted person with the given username. */
	public void loadRepliesOf(String username, ServerResponseListener listener) {
		account.loadRepliesOf(username, listener);
	}
	
	/** Reloads requests of all types that are both sent and received by the user. */
	public void loadAllRequests(ServerResponseListener listener) {
		account.loadRequests(listener);
	}

	// ////////////////////////////////////////////
	// Login View - Register View - Account View //
	// ////////////////////////////////////////////

	/** Logs a user in. */
	public void login(String username, String password,
			Context context, ServerResponseListener listener) {
		account.login(username, password, context, listener);
	}

	/** Logs a user out. */
	public void logout(Context context) {
		account.logout(context);
	}

	/** Register and log a user in. */
	public void register(String username, String fullname, String password,
			Context context, ServerResponseListener listener) {
		account.register(username, fullname, password, context, listener);
	}

	// /////////////////////////////////
	// Own Status View - Compose View //
	// /////////////////////////////////

	/** Publish a status with an OK signal. */
	public void publishStatusOK(String message, ServerResponseListener listener) {
		String username = account.getUser().getUsername();
		String fullname = account.getUser().getFullName();
		
		Status status = new Status(null, username, fullname, message, Signal.OK, null);
		account.publishStatus(status, listener);
	}

	/** Publish a status with a Help signal. */
	public void publishStatusHelp(ServerResponseListener listener) {
		String username = account.getUser().getUsername();
		String fullname = account.getUser().getFullName();
		
		Status status = new Status(null, username, fullname, "", Signal.HELP, null);
		account.publishStatus(status, listener);
	}

	/** Update the urgency of a status with a Help signal. */
	public void updateStatusHelpUrgency(Urgency urgency,
			ServerResponseListener listener) {
		User user = account.getUser();

		if (user.getStatus().getSignal() != Signal.HELP) {
			listener.onServerResponded(
					null,
					new InvalidStatusException(
							"You can only update the urgency of a status requesting for help."));
			return;
		}

		String username = account.getUser().getUsername();
		String fullname = account.getUser().getFullName();
		Status currentStatus = user.getStatus();
		Date timestamp = currentStatus.getTimestamp();
		String message = currentStatus.getMessage();
		Signal signal = currentStatus.getSignal();
		ArrayList<Reply> replies = currentStatus.getReplies();

		Status status = new Status(timestamp, username, fullname,
				message, signal, urgency, replies);
		account.publishStatus(status, listener);
	}

	/** Update the message of a status with a Help signal. */
	public void updateStatusHelpMessage(String message,
			ServerResponseListener listener) {
		User user = account.getUser();
		
		if (user.getStatus().getSignal() != Signal.HELP) {
			listener.onServerResponded(
					null,
					new InvalidStatusException(
							"You can only update the message of a status requesting for help."));
			return;
		}
		
		String username = account.getUser().getUsername();
		String fullname = account.getUser().getFullName();
		Status currentStatus = user.getStatus();
		Date timestamp = currentStatus.getTimestamp();
		Signal signal = currentStatus.getSignal();
		Urgency urgency = currentStatus.getUrgency();
		ArrayList<Reply> replies = currentStatus.getReplies();

		Status status = new Status(timestamp, username, fullname,
				message, signal, urgency, replies);
		account.publishStatus(status, listener);
	}

	// ///////////////
	// Replies View //
	// ///////////////

	/**
	 * Send a reply to the given status if it belongs to self or an assisted
	 * person. On success, reloads the status in cache.
	 */
	public void publishReply(String message, String statusAuthorUsername,
			Date statusTimestamp, ServerResponseListener listener) {

		// Find author
		User author;
		User currentUser = account.getUser();

		if (currentUser.getUsername().equals(statusAuthorUsername)) {
			author = currentUser;
		} else {
			author = getAssistedPerson(statusAuthorUsername);
		}

		if (author == null) {
			listener.onServerResponded(null, new UserNotFoundException());
			return;
		}

		// Verify timestamp
		if (!author.getStatus().getTimestamp().equals(statusTimestamp)) {
			listener.onServerResponded(null, new InvalidStatusException(Symbols.statexpmsg));
			return;
		}

		// Publish reply
		Reply reply = new Reply(null, currentUser.getUsername(), currentUser.getFullName(), message);
		account.publishReply(reply, author.getStatus(), listener);
	}
	
	/////////////////////////////
	// Relations Settings View //
	/////////////////////////////
	
	/** Send a new relationship request. */
	public void sendRequest(String requester, String targeted, RequestType requestType,
			Date timestamp, ServerResponseListener listener) {
		account.sendRequest(new Request(requester, targeted, requestType, timestamp),
				listener);
	}
	
	/** Approve a relationship request. */
	public void approveRequest(Context context, Request request, ServerResponseListener listener) {
		account.approveRequest(context, request, listener);
	}
	
	/** Decline or cancel a relationship request. */
	public void cancelRequest(Request request, ServerResponseListener listener) {
		account.cancelRequest(request, listener);
	}
	
	/** Delete a carer from your relationship list. */
	public void deleteCarer(String username, final ServerResponseListener listener) {
		account.deleteCarer(username, listener);
	}
	
	/** Delete an assisted person from your relationship list. */
	public void deleteAssisted(Context context, String username, final ServerResponseListener listener) {
		account.deleteAssisted(context, username, listener);
	}

}
