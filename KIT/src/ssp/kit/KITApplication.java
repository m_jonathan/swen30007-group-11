package ssp.kit;

import ssp.kit.ui.ActivityInitial;
import android.app.Application;

import com.parse.Parse;
import com.parse.ParseACL;
import com.parse.ParseInstallation;
import com.parse.PushService;

public class KITApplication extends Application {

	private static final String app_id     = "0OpYpu7AXqrFvkpm202VSqgYkeqQpvm7KKAr72rM";
	private static final String client_key = "zOz4L43aCLHwkjLrJxU9x1iUMsiXUVhvesIL7Jos";
	
	@Override
	public void onCreate() {
		super.onCreate();
		
		initializeParse();
		
		return;
	}
	
	private void initializeParse() {
		// Initialize Parse
		Parse.initialize(this, app_id, client_key);
		
		// Register for Push service
		PushService.setDefaultPushCallback(this, ActivityInitial.class);
		ParseInstallation.getCurrentInstallation().saveInBackground();
		
		ParseACL defaultACL = new ParseACL();
		
		// If you would like all objects to be private by default, remove this line.
		defaultACL.setPublicReadAccess(true);
		ParseACL.setDefaultACL(defaultACL, true);
		
		return;
	}

}
