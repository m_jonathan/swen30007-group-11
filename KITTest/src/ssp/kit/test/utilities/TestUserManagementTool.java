package ssp.kit.test.utilities;

import ssp.kit.ui.ActivityMain;

import com.jayway.android.robotium.solo.Solo;

public class TestUserManagementTool {
	
	private static final String stringUsername = "jawo"; 
	private static final String stringPassword = "qwer";
	private static final String stringLoginButton = "Login";
	
	public static boolean logInToActivityMainIfHaventDoneSo(Solo solo) {
		return logInToActivityMainIfHaventDoneSo(solo, stringUsername, stringPassword);
	}
	
	public static boolean logInToActivityMainIfHaventDoneSo(Solo solo, final String username, final String password) {
		
		if (!solo.searchButton(stringLoginButton)) {
			return false;
		}
		
		solo.enterText(0, username);
		solo.enterText(1, password);
		solo.clickOnButton(stringLoginButton);
		solo.waitForActivity(ActivityMain.class);
		return true;
	}
}
