package ssp.kit.test.utilities;

import java.io.InvalidClassException;

import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;

import ssp.kit.ServerResponseListener;

public class TestUtilities {
	
	public static Answer<Object> getAnswer(final Object o, final Exception e) {
		return new Answer<Object>() {
			public Object answer(InvocationOnMock invocation) throws Exception {
				Object[] argsObjects = invocation.getArguments();
				
				int last = invocation.getArguments().length - 1;
				if (!(argsObjects[last] instanceof ServerResponseListener)) {
					throw new InvalidClassException("Expected ServerResponseListener as last argument");
				}
				
				ServerResponseListener listener = (ServerResponseListener) argsObjects[last];
				
				listener.onServerResponded(o, e);
				
				return null;
			}
		};
	}

}
