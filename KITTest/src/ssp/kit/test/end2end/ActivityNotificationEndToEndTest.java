package ssp.kit.test.end2end;

import ssp.kit.ui.ActivityInitial;
import ssp.kit.ui.ActivityNotification;
import android.content.Intent;
import android.test.ActivityInstrumentationTestCase2;

import com.jayway.android.robotium.solo.Solo;
import com.jayway.android.robotium.solo.Timeout;

public class ActivityNotificationEndToEndTest extends ActivityInstrumentationTestCase2<ActivityNotification> {
	
	private String username = "testUsername";
	private String fullname = "testFullname";
	private String signal = "HELP";
	private String signalDisplay = "HELP";
	private String urgency = "LT";
	private String urgencyDisplay = "LIFE THREATENING";
	private String message = "ActivityNotificationEndToEndTest message";
	private String buttonOK = "Reply";
	
	private Solo solo;
	
	public ActivityNotificationEndToEndTest() {
		super(ActivityNotification.class);
	}
	
	@Override
	protected void setUp() throws Exception {
		super.setUp();
		solo = new Solo(getInstrumentation());
		Timeout.setLargeTimeout(45000);
		Timeout.setSmallTimeout(45000);
		setActivityIntent(getHelpNotificationIntent());
		getActivity();
		solo.assertCurrentActivity("ActivityNotification expected", "ActivityNotification");
	}
	
	@Override
	protected void tearDown() throws Exception {
		solo.finishOpenedActivities();
		super.tearDown();
	}
	
	public void testActivityNotification() {
		assertTrue("Username not displayed in notification", solo.searchText(username));
		assertTrue("Full name not displayed in notification", solo.searchText(fullname));
		assertTrue("Signal not displayed in notification", solo.searchText(signalDisplay));
		assertTrue("Urgency not displayed in notification", solo.searchText(urgencyDisplay));
		assertTrue("Message not displayed in notification", solo.searchText(message));
		
		solo.clickOnText(buttonOK);
		solo.waitForActivity(ActivityInitial.class);
		solo.assertCurrentActivity("Expected ActivityInitial", "ActivityInitial");
	}

	private Intent getHelpNotificationIntent() {
		Intent intent = new Intent(getInstrumentation().getContext(), ActivityNotification.class);
		intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
		intent.putExtra(ActivityNotification.EXTRA_STATUS_AUTHOR_USERNAME, username);
		intent.putExtra(ActivityNotification.EXTRA_STATUS_AUTHOR_FULLNAME, fullname);
		intent.putExtra(ActivityNotification.EXTRA_STATUS_STATUS_SIGNAL,   signal);
		intent.putExtra(ActivityNotification.EXTRA_STATUS_STATUS_URGENCY,  urgency);
		intent.putExtra(ActivityNotification.EXTRA_STATUS_STATUS_MESSAGE,  message);
		return intent;
	}
}
