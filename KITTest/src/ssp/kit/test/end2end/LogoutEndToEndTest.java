package ssp.kit.test.end2end;

import ssp.kit.R;
import ssp.kit.test.utilities.TestUserManagementTool;
import ssp.kit.ui.ActivityInitial;
import ssp.kit.ui.ActivityLogin;
import ssp.kit.ui.ActivityMain;
import android.test.ActivityInstrumentationTestCase2;

import com.jayway.android.robotium.solo.Solo;
import com.jayway.android.robotium.solo.Timeout;

public class LogoutEndToEndTest extends ActivityInstrumentationTestCase2<ActivityInitial> {

	private Solo solo;	
	
	public LogoutEndToEndTest() {
		super(ActivityInitial.class);
	}
	
	protected void setUp() throws Exception {
		super.setUp();
		getInstrumentation().waitForIdleSync();
		solo = new Solo(getInstrumentation());
		Timeout.setLargeTimeout(30000);
		Timeout.setSmallTimeout(30000);
		getActivity();
		TestUserManagementTool.logInToActivityMainIfHaventDoneSo(solo);
	}

	protected void tearDown() throws Exception {
		solo.finishOpenedActivities();
		super.tearDown();
	}
	
	public void testLogout() {
		String logoutText = "Logout";
		
		// Move to Settings tab
		solo.assertCurrentActivity("Did not start from ActivityMain", ActivityMain.class);
		solo.clickOnText(str(R.string.title_settings_tab).toUpperCase());
		
		// Verify Settings menu item to logout is available
		assertTrue("Cannot find Settings menu item to logout",solo.searchText(logoutText));
		
		// Logging out user
		solo.clickOnText(logoutText);
		
		// Verify confirmation Toast
		assertTrue("Did not find logout confirmation Toast",solo.waitForText("Logging out..."));
		
		// Verify current Activity is at ActivityLogin
		solo.assertCurrentActivity("Expected ActivityLogin", ActivityLogin.class);
	}
	
	private String str(int id) {
		return getActivity().getString(id);
	}
	
}
