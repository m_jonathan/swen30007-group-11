package ssp.kit.test.end2end;

import ssp.kit.test.utilities.TestUserManagementTool;
import ssp.kit.ui.ActivityLogin;
import ssp.kit.ui.ActivityMain;
import android.test.ActivityInstrumentationTestCase2;

import com.jayway.android.robotium.solo.Solo;
import com.jayway.android.robotium.solo.Timeout;

public class ActivityLoginEndToEndTest extends ActivityInstrumentationTestCase2<ActivityLogin> {
	
	private Solo solo;	
		
	public ActivityLoginEndToEndTest() {
		super(ActivityLogin.class);
	}

	protected void setUp() throws Exception {
		super.setUp();
		getInstrumentation().waitForIdleSync();
		solo = new Solo(getInstrumentation());
		Timeout.setLargeTimeout(30000);
		Timeout.setSmallTimeout(30000);
		getActivity();
		TestUserManagementTool.logInToActivityMainIfHaventDoneSo(solo);
	}

	protected void tearDown() throws Exception {
		solo.finishOpenedActivities();
		super.tearDown();
	}
	
	public void testActivityLogin() {
		solo.assertCurrentActivity("Expected ActivityMain", ActivityMain.class);
	}
	
}
