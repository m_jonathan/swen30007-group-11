package ssp.kit.test.end2end;

import ssp.kit.test.utilities.TestUserManagementTool;
import ssp.kit.ui.ActivityInitial;
import ssp.kit.ui.ActivityMain;
import ssp.kit.ui.ActivityReplies;
import android.test.ActivityInstrumentationTestCase2;

import com.jayway.android.robotium.solo.Solo;
import com.jayway.android.robotium.solo.Timeout;

public class ActivityRepliesEndToEndTest extends ActivityInstrumentationTestCase2<ActivityInitial> {

	private static final int longSleep = 10000;
	
	private Solo solo;
	
	public ActivityRepliesEndToEndTest() {
		super(ActivityInitial.class);
	}

	protected void setUp() throws Exception {
		super.setUp();
		getInstrumentation().waitForIdleSync();
		solo = new Solo(getInstrumentation());
		Timeout.setLargeTimeout(30000);
		Timeout.setSmallTimeout(30000);
		getActivity();
		TestUserManagementTool.logInToActivityMainIfHaventDoneSo(solo);
	}

	protected void tearDown() throws Exception {
		solo.finishOpenedActivities();
		super.tearDown();
	}
	
	public void testSendNewReply() {
		// Go to ActivityReplies from Status tab
		solo.assertCurrentActivity("Did not start from ActivityMain", ActivityMain.class);
		assertTrue("Cannot find replies button",solo.searchButton("^(1 reply|(\\d+|No) replies)$"));
		solo.clickOnButton("^(1 reply|(\\d+|No) replies)$");
		solo.assertCurrentActivity("Cannot get into ActivityReplies from Status tab", ActivityReplies.class);
		
		// Enter reply string
		java.util.Random rand = new java.util.Random();
		String replyString = "Test reply string "+rand.nextLong();
		solo.enterText(0, replyString);
		solo.clickOnButton("Publish");
		
		// Verifying that the Publish button is disabled
		assertTrue("Publish button is not turned to 'Publishing...'",solo.searchButton("Publishing\\.\\.\\."));
		assertFalse("Publish button is not disabled",solo.getButton("Publishing\\.\\.\\.").isEnabled());

		// Verifying published reply
		solo.sleep(longSleep);
		assertTrue("Cannot find the newly published reply in the list",solo.searchText(replyString));
		
		// Verifying that the replies list is being refreshed
		assertTrue("",solo.waitForText("Refreshing replies\\.\\.\\."));
	}
	
}
