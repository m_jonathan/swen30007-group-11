package ssp.kit.test.end2end;

import ssp.kit.R;
import ssp.kit.test.utilities.TestUserManagementTool;
import ssp.kit.ui.ActivityInitial;
import ssp.kit.ui.ActivityReplies;
import android.test.ActivityInstrumentationTestCase2;
import android.view.View;

import com.jayway.android.robotium.solo.Solo;
import com.jayway.android.robotium.solo.Timeout;

public class FragmentStatusViewEndToEndTest extends ActivityInstrumentationTestCase2<ActivityInitial>  {
	
	private Solo solo;
	
	private int updateWaitTime = 10000;
	private int quickWaitTime = 2000;
	
	public FragmentStatusViewEndToEndTest() {
		super(ActivityInitial.class);
	}
	
	@Override
	protected void setUp() throws Exception {
		super.setUp();
		getInstrumentation().waitForIdleSync();
		solo = new Solo(getInstrumentation());
		Timeout.setLargeTimeout(30000);
		Timeout.setSmallTimeout(30000);
		getActivity();
		TestUserManagementTool.logInToActivityMainIfHaventDoneSo(solo);
		solo.assertCurrentActivity("Expected ActivityMain", "ActivityMain");
	}

	@Override
	protected void tearDown() throws Exception {
		solo.finishOpenedActivities();
		super.tearDown();
	}

	public void testPublishStatusOKwithMessage() throws InterruptedException {		
		String message = "testPublishStatusOK message";
		String recentlyUpdatedTimestampIndicator = "ago";
		
		solo.clickOnButton("("+str(R.string.buttonStatusOkay)+").*");
		solo.typeText(0, message);
		solo.clickOnButton(str(R.string.generalTextPublish));
		
		solo.waitForText(recentlyUpdatedTimestampIndicator, 1, updateWaitTime);
		waitForAWhile();
		
		assertTrue("Status not published or timestamp not updated", solo.searchText(recentlyUpdatedTimestampIndicator));
		assertTrue("Signal not updated", solo.searchText(str(R.string.textViewStatusStatusSignal_ok)));
		assertTrue("Message not updated", solo.searchText(message));
		assertTrue("Replies button not updated", solo.searchText(str(R.string.buttonStatusStatusReplies_no_replies)));
		
		assertEquals("Urgency pointer still visible", View.GONE, solo.getView(R.id.textViewUrgencyPointer).getVisibility());
		assertEquals("Urgency LT button still visible", View.GONE, solo.getView(R.id.buttonStatusStatusUrgencyLifeThreatening).getVisibility());
		assertEquals("Urgency NLT button still visible", View.GONE, solo.getView(R.id.buttonStatusStatusUrgencyNotLifeThreatening).getVisibility());
	}
	
	public void testPublishStatusOKwithoutMessage() throws InterruptedException {
		String updatedOKStatusWithNoMessageIndicator = "is OK";	
		
		solo.clickOnButton("("+str(R.string.buttonStatusOkay)+").*");
		solo.clickOnButton(str(R.string.generalTextPublish));
		
		solo.waitForText("ago", 1, updateWaitTime);
		waitForAWhile();
		
		assertTrue("Status not published or timestamp not updated", solo.searchText("ago"));
		assertTrue("Signal not updated", solo.searchText(str(R.string.textViewStatusStatusSignal_ok)));
		assertTrue("Message not updated", solo.searchText(updatedOKStatusWithNoMessageIndicator));
		assertTrue("Replies button not updated", solo.searchText(str(R.string.buttonStatusStatusReplies_no_replies)));
		
		assertEquals("Urgency pointer still visible", View.GONE, solo.getView(R.id.textViewUrgencyPointer).getVisibility());
		assertEquals("Urgency LT button still visible", View.GONE, solo.getView(R.id.buttonStatusStatusUrgencyLifeThreatening).getVisibility());
		assertEquals("Urgency NLT button still visible", View.GONE, solo.getView(R.id.buttonStatusStatusUrgencyNotLifeThreatening).getVisibility());
	}
	
	public void testPublishStatusSOS() throws InterruptedException {
		subtestPublishStatusSOSInitial();
		subtestUpdateStatusSOSMessage();
		subtestUpdateStatusUrgencyLifeThreatening();
		subtestUpdateStatusUrgencyNotLifeThreatening();
	}
	
	private void subtestPublishStatusSOSInitial() throws InterruptedException {
		solo.clickOnButton("("+str(R.string.buttonStatusHelp)+").*");
		
		solo.waitForText("ago", 1, updateWaitTime);
		waitForAWhile();
		
		assertTrue("Status not published or timestamp not updated", solo.searchText("ago"));
		assertTrue("Signal not updated", solo.searchText(str(R.string.textViewStatusStatusSignal_help_requested)));
		assertTrue("Message not updated", solo.searchText(str(R.string.textViewStatusMessageEnterAMessage)));
		assertTrue("Replies button not updated", solo.searchText(str(R.string.buttonStatusStatusReplies_no_replies)));
		
		assertEquals("Urgency pointer still invisible", View.VISIBLE, solo.getView(R.id.textViewUrgencyPointer).getVisibility());
		assertEquals("Urgency LT button still invisible", View.VISIBLE, solo.getView(R.id.buttonStatusStatusUrgencyLifeThreatening).getVisibility());
		assertEquals("Urgency NLT button still invisible", View.VISIBLE, solo.getView(R.id.buttonStatusStatusUrgencyNotLifeThreatening).getVisibility());
		
		assertTrue("I'm OK Now button not updated", solo.searchText(str(R.string.buttonStatusOkayNow)));
	}
	
	private void subtestUpdateStatusSOSMessage() throws InterruptedException {
		String message = "testUpdateStatusSOSMessage message";
		
		solo.clickOnText(str(R.string.textViewStatusMessageEnterAMessage));
		solo.enterText(0, message);
		solo.clickOnButton(str(R.string.generalTextPublish));
		
		solo.waitForText("ago", 1, updateWaitTime);
		waitForAWhile();
		
		assertTrue("Message not updated", solo.searchText(message));
	}

	private void subtestUpdateStatusUrgencyLifeThreatening() throws InterruptedException {
		solo.clickOnButton(str(R.string.buttonStatusStatusUrgencyLifeThreateningInactive));
		
		solo.waitForText("ago", 1, updateWaitTime);
		waitForAWhile();
		
		assertTrue("LT urgency not updated", solo.searchText(str(R.string.buttonStatusStatusUrgencyLifeThreateningActive)));
		assertTrue("NLT urgency not updated", solo.searchText(str(R.string.buttonStatusStatusUrgencyNotLifeThreateningInactive)));
	}

	private void subtestUpdateStatusUrgencyNotLifeThreatening() throws InterruptedException {
		solo.clickOnButton(str(R.string.buttonStatusStatusUrgencyNotLifeThreateningInactive));
		
		solo.waitForText("ago", 1, updateWaitTime);
		waitForAWhile();
		
		assertTrue("LT urgency not updated", solo.searchText(str(R.string.buttonStatusStatusUrgencyLifeThreateningInactive)));
		assertTrue("NLT urgency not updated", solo.searchText(str(R.string.buttonStatusStatusUrgencyNotLifeThreateningActive)));
	}
	
	public void testAbleToMoveToRepliesView() throws InterruptedException {
		solo.clickOnButton(str(R.string.buttonStatusStatusReplies_no_replies));
		
		solo.waitForActivity(ActivityReplies.class);
		
		solo.assertCurrentActivity("Expected ActivityClass", "ActivityReplies");
	}
	
	private String str(int id) {
		return getActivity().getString(id);
	}
	
	private void waitForAWhile() throws InterruptedException {
		synchronized (solo) {
			solo.wait(quickWaitTime);
		};
	}
	
}
