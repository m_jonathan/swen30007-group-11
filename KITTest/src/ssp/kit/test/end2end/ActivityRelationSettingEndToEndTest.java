package ssp.kit.test.end2end;

import ssp.kit.KIT;
import ssp.kit.R;
import ssp.kit.test.utilities.TestUserManagementTool;
import ssp.kit.ui.ActivityInitial;
import ssp.kit.ui.ActivityMain;
import android.test.ActivityInstrumentationTestCase2;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.jayway.android.robotium.solo.Solo;

public class ActivityRelationSettingEndToEndTest extends ActivityInstrumentationTestCase2<ActivityInitial> {

	private static final int refreshWaitTime = 20000;
	private static final int refreshWaitTimeShort = 5000;
	private static final int toastWaitTime = 10000;
	
	private Solo solo;
	
	public ActivityRelationSettingEndToEndTest() {
		super(ActivityInitial.class);
	}
	
	@Override
	protected void setUp() throws Exception {
		getInstrumentation().waitForIdleSync();
		solo = new Solo(getInstrumentation());
		getActivity();
		TestUserManagementTool.logInToActivityMainIfHaventDoneSo(solo);
	}
	
	@Override
	protected void tearDown() throws Exception {
		solo.finishOpenedActivities();
		super.tearDown();
	}
	
	public void testAbleToMoveToActivityRelationSettingView() {
		toSettingsTab();
		
		// Try entering Carers settings view
		assertTrue("Cannot find Carers settings option", solo.searchText("Carers"));
		solo.clickOnText("Carers");
		assertTrue("Cannot go into Carers settings", solo.searchText("Carers") && solo.searchButton(str(R.string.buttonAddNewRelation)));
		
		solo.goBack();
		
		// Try entering Assisted Person settings view
		assertTrue("Cannot find Assisted Persons settings option", solo.searchText("People I care about"));
		solo.clickOnText("People I care about");
		assertTrue("Cannot go into Assisted Persons settings", solo.searchText("Assisted Persons") && solo.searchButton(str(R.string.buttonAddNewRelation)));
	}
	
	public void testSendValidNewRelationRequestThenCancelIt() {
		toSettingsTab();
		
		String requestUsername = "david";
		
		// Enter Carers settings view
		assertTrue("Cannot find Carers settings option", solo.searchText("Carers"));
		solo.clickOnText("Carers");
		assertTrue("Cannot go into Carers settings", solo.searchText("Carers") && solo.searchButton(str(R.string.buttonAddNewRelation)));
		
		// Send new relation request
		solo.clickOnButton(str(R.string.buttonAddNewRelation));
		solo.enterText(0, requestUsername);
		solo.clickOnButton(1);
		
		// Verifying that request is sent
		if (!solo.searchText(requestUsername)) {
			// Try hitting the refresh button
			solo.clickOnActionBarItem(R.id.action_refresh_list);
			solo.sleep(refreshWaitTime);
			assertTrue("Request was not successfully sent.", solo.searchText(requestUsername));
			
			fail("Sent request does not appear straightaway; manual refresh is needed");
		}
		assertTrue("Sent request is not displayed properly", solo.searchText("Waiting for approval"));
		
		solo.sleep(refreshWaitTimeShort);
		
		// Deleting sent request
		solo.clickOnText(requestUsername);
		assertTrue("Popup does not appear", solo.searchText("Cancel request"));
		solo.clickOnButton(1);
		
		// Waiting for "Canceling request to [username]" toast
		assertTrue("Did not find first cancellation Toast",solo.waitForText("Canceling request to ", 1, toastWaitTime));
		
		// Waiting for "Canceled request to [username]" toast
		assertTrue("Did not find second cancellation Toast",solo.waitForText("Canceled request to ", 1, toastWaitTime));
		
		// Verifying sent request has disappeared
		solo.sleep(refreshWaitTimeShort);
		assertFalse("Sent request has not disappeared from list", solo.searchText(requestUsername));
	}
	
	public void testSendNewRelationRequestToSelf() {
		toSettingsTab();
		
		String currentlyLoggedInUser = KIT.app().getUser().getUsername();
		
		// Enter Carers settings view
		assertTrue("Cannot find Carers settings option", solo.searchText("Carers"));
		solo.clickOnText("Carers");
		assertTrue("Cannot go into Carers settings", solo.searchText("Carers") && solo.searchButton(str(R.string.buttonAddNewRelation)));
		
		// Send request to self
		solo.clickOnButton(str(R.string.buttonAddNewRelation));
		solo.enterText(0, currentlyLoggedInUser);
		solo.clickOnButton(1);
		
		// Verifying error message Toast
		assertTrue("Did not find the Toast expected",solo.waitForText("Cannot add self", 1, toastWaitTime));
	}
	
	public void testSendNewRelationRequestToExistingRelation() {
		toSettingsTab();
		
		// Enter Carers settings view
		assertTrue("Cannot find Carers settings option", solo.searchText("Carers"));
		solo.clickOnText("Carers");
		assertTrue("Cannot go into Carers settings", solo.searchText("Carers") && solo.searchButton(str(R.string.buttonAddNewRelation)));
		
		// Get an existing relation name
		TextView existingRelation = (TextView) solo.getView(R.id.linearLayoutRelationList).findViewById(R.id.textViewRelationSettingListEntryUsername);
		assertNotNull("Cannot find an existing relation",existingRelation);
		
		// Send request to existing relation
		solo.clickOnButton(str(R.string.buttonAddNewRelation));
		solo.enterText(0, existingRelation.getText().toString());
		solo.clickOnButton(1);
		
		// Verifying error message Toast
		assertTrue("Did not find the Toast expected",solo.waitForText("is already a", 1, toastWaitTime));
	}
	
	public void testSendNewRelationRequestToInvalidUser() {
		toSettingsTab();
		
		// Enter Carers settings view
		assertTrue("Cannot find Carers settings option", solo.searchText("Carers"));
		solo.clickOnText("Carers");
		assertTrue("Cannot go into Carers settings", solo.searchText("Carers") && solo.searchButton(str(R.string.buttonAddNewRelation)));
		
		// Get an existing relation name
		String nonExistingRelation = "qwerty";
		
		// Send request to existing relation
		solo.clickOnButton(str(R.string.buttonAddNewRelation));
		solo.enterText(0, nonExistingRelation);
		solo.clickOnButton(1);
		
		// Verifying error message Toast
		assertTrue("Did not find the Toast expected",solo.waitForText("Invalid user", 1, toastWaitTime));
		
		// Verifying the request has been removed from the screen
		assertFalse("Sent request has not been removed from screen.",solo.searchText(nonExistingRelation));
	}
	
	public void testRemoveExistingRelation() {
		toSettingsTab();
		
		// Enter Carers settings view
		assertTrue("Cannot find Carers settings option", solo.searchText("Carers"));
		solo.clickOnText("Carers");
		assertTrue("Cannot go into Carers settings", solo.searchText("Carers") && solo.searchButton(str(R.string.buttonAddNewRelation)));
		
		// Get an existing relation details
		LinearLayout existingRelationEntry = (LinearLayout) solo.getView(R.id.linearLayoutRelationSettingListEntry);
		assertNotNull("Cannot find an existing relation entry",existingRelationEntry);
		TextView textViewExistingRelationUsername = (TextView) existingRelationEntry.findViewById(R.id.textViewRelationSettingListEntryUsername);
		assertNotNull("Cannot find username for existing relation to be deleted",textViewExistingRelationUsername);
		String existingRelationUsername = textViewExistingRelationUsername.getText().toString();
		TextView existingRelationDeleteButton = (TextView) existingRelationEntry.findViewById(R.id.textViewRemoveRelationListSettingEntry);
		assertNotNull("Cannot find delete button for the selected existing relation entry",existingRelationDeleteButton);
				
		// Delete existing relation
		solo.clickOnView(existingRelationDeleteButton);
		solo.clickOnButton(1);
		
		// Verify relation has been successfully deleted
		assertTrue("Did not find delete confirmation Toast",solo.waitForText("Deleted "));
		solo.sleep(refreshWaitTimeShort);
		assertFalse("Deleted relation is not removed from the list",solo.searchText(existingRelationUsername));
	}
	
	private void toSettingsTab() {
		solo.assertCurrentActivity("Did not start from ActivityMain", ActivityMain.class);
		solo.clickOnText(str(R.string.title_settings_tab).toUpperCase());
	}
	
	private String str(int id) {
		return getActivity().getString(id);
	}

}
