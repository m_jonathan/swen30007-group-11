package ssp.kit.test.end2end;

import java.util.ArrayList;

import com.jayway.android.robotium.solo.Solo;
import com.jayway.android.robotium.solo.Timeout;

import ssp.kit.R;
import ssp.kit.test.utilities.TestUserManagementTool;
import ssp.kit.ui.ActivityInitial;
import ssp.kit.ui.ActivityMain;
import ssp.kit.ui.ActivityReplies;
import android.test.ActivityInstrumentationTestCase2;
import android.widget.LinearLayout;
import android.widget.TextView;

public class FragmentUpdatesViewEndToEndTest extends ActivityInstrumentationTestCase2<ActivityInitial>{

	public FragmentUpdatesViewEndToEndTest() {
		super(ActivityInitial.class);
	}
	
	private Solo solo;
	private final String username = "jawo";
	private final String password = "qwer";
	private final String buttonPeople = "PEOPLE";
	private final int numAssisted = 8;
	private ArrayList<String> order;
	
	@Override
	protected void setUp() throws Exception {
		super.setUp();
		getInstrumentation().waitForIdleSync();
		solo = new Solo(getInstrumentation());
		Timeout.setLargeTimeout(30000);
		Timeout.setSmallTimeout(30000);
		getActivity();
		order = new ArrayList<String>();
		order.add("HELP (LIFE THREATENING)");
		order.add("HELP");
		order.add("HELP (NON LIFE THREATENING)");
		order.add("OK");
		TestUserManagementTool.logInToActivityMainIfHaventDoneSo(solo, username, password);
	}
	
	@Override
	protected void tearDown() throws Exception {
		solo.finishOpenedActivities();
		super.tearDown();
	}
	
	public void testFragmentUpdatesEndToEnd() {
		solo.waitForActivity(ActivityMain.class);
		solo.assertCurrentActivity("Expecting Activity Main Class.", ActivityMain.class);
		assertTrue(solo.searchText(buttonPeople));
		solo.clickOnText(buttonPeople);
		
		//Check number of contacts
		solo.waitForView(R.id.linear_carer_layout);
		ArrayList<LinearLayout> assisteds = totalAssistedPeople();
		assertEquals("Number of assisted persons not the same.", numAssisted, assisteds.size());
		assertTrue("The assisted people's statuses are not sorted correctly.", testSortedAssistedPeople(assisteds));
		
		//Select on one of the assisted persons, selected randomly
		int randomIndex = (int) Math.random() * (assisteds.size());
		LinearLayout selectedAssisted = assisteds.get(randomIndex);
		solo.clickOnView(selectedAssisted);
		
		//Check that we go the reply screen of that selected assisted person
		solo.waitForActivity(ActivityReplies.class);
		solo.assertCurrentActivity("Wrong activity after selecting on an assisted people's status. Expecting Activity Replies class.", ActivityReplies.class);
		TextView textViewUsername = (TextView) selectedAssisted.findViewById(R.id.textViewCarerTabUsername);
		TextView textViewFullname = (TextView) selectedAssisted.findViewById(R.id.textViewCarerTabFullName);
		assertNotNull("TextView username not found.", textViewUsername);
		assertNotNull("TextView fullname not found.", textViewFullname);
		assertNotNull("Textview for fullname is null.", solo.getView(R.id.textViewRepliesStatusAuthorFullName));
		assertNotNull("Textview for username is null.", solo.getView(R.id.textViewRepliesStatusAuthorUsername));
		assertEquals("Expected username does not match actual username.",textViewUsername.getText(), ((TextView)solo.getView(R.id.textViewRepliesStatusAuthorUsername)).getText());
		assertEquals("Expected username does not match actual username.",textViewFullname.getText(), ((TextView)solo.getView(R.id.textViewRepliesStatusAuthorFullName)).getText());
	}
	
	//A method to retrieve all the relative layout used to display the statuses of the assisted people
	private ArrayList<LinearLayout> totalAssistedPeople() {
		ArrayList<LinearLayout> assisteds = new ArrayList<LinearLayout>();
		for (int i = 0; i < ((LinearLayout)solo.getView(R.id.linear_carer_layout)).getChildCount(); i++) {
			assisteds.add((LinearLayout) ((LinearLayout)solo.getView(R.id.linear_carer_layout)).getChildAt(i));
		}
		return assisteds;
	}
	
	//A method used to tell if the statuses of the assisted people is sorted based on the signal and urgency
	private boolean testSortedAssistedPeople(ArrayList<LinearLayout> assisteds) {
		for (int i = 0; i < assisteds.size()-1; i++) {
			TextView signalCurrentAssisted = (TextView) assisteds.get(i).findViewById(R.id.textViewCarerTabSignal);
			TextView signalNextAssisted = (TextView) assisteds.get(i+1).findViewById(R.id.textViewCarerTabSignal);
			assertNotNull("Textview element number "+ i +" cannot be found.", signalCurrentAssisted);
			assertNotNull("Textview element number "+ (i+1) +" cannot be found.", signalNextAssisted);
			if (order.indexOf((String) signalCurrentAssisted.getText()) > order.indexOf((String) signalNextAssisted.getText())) {
				return false;
			}
		}
		return true;
	}
}
