package ssp.kit.test.end2end;

import java.util.UUID;

import com.jayway.android.robotium.solo.Solo;
import com.jayway.android.robotium.solo.Timeout;

import ssp.kit.R;
import ssp.kit.ui.ActivityMain;
import ssp.kit.ui.ActivityRegister;
import android.test.ActivityInstrumentationTestCase2;
import android.widget.TextView;

public class ActivityRegisterEndToEndTest extends ActivityInstrumentationTestCase2<ActivityRegister> {

	public ActivityRegisterEndToEndTest() {
		super(ActivityRegister.class);
	}
	private Solo solo;
	
	@Override
	protected void setUp() throws Exception {
		super.setUp();
		getInstrumentation().waitForIdleSync();
		solo = new Solo(getInstrumentation());
		Timeout.setLargeTimeout(30000);
		Timeout.setSmallTimeout(30000);
		getActivity();
	}
	
	@Override
	protected void tearDown() throws Exception {
		solo.finishOpenedActivities();
		super.tearDown();
	}
	
	public void testRegisterExistingUsernameEndToEnd() {
		solo.assertCurrentActivity("Expected Activity Register.", ActivityRegister.class);
		
		//Registering a user using an existing username 
		final String existingUsername = "david";
		final String existingFullname = "david";
		final String existingPassword = "david";
		
		solo.enterText(0, existingFullname);
		solo.enterText(1, existingUsername);
		solo.enterText(2, existingPassword);
		assertTrue("Register button not found", solo.searchButton("Register"));
		solo.clickOnButton("Register");
		assertTrue(solo.waitForText("username " + existingUsername + " already taken"));
		assertTrue("OK button not found", solo.searchButton("OK"));
		solo.clickOnButton("OK");
	}
	
	public void testRegisterNonExistingUsernameEndToEnd() {
		solo.assertCurrentActivity("Expected Activity Register.", ActivityRegister.class);
		
		//Registering a user using a randomly generated credentials
		final String username = UUID.randomUUID().toString();
		final String password = UUID.randomUUID().toString();
		final String fullname = UUID.randomUUID().toString();
		//Test case for a non-existing username
		solo.enterText(0, fullname);
		solo.enterText(1, username);
		solo.enterText(2, password);
		assertTrue("Register button not found", solo.searchButton("Register"));
		solo.clickOnButton("Register");
		
		//Checking that we have logged in with the generated user
		solo.waitForActivity(ActivityMain.class);
		solo.assertCurrentActivity("Wrong activity after registering. Expecting Activity Main.", ActivityMain.class);
		assertNotNull("Textview for fullname is null.", solo.getView(R.id.textViewStatusStatusAuthorFullName));
		assertNotNull("Textview for username is null.", solo.getView(R.id.textViewStatusStatusAuthorUsername));
		assertEquals("Registered fullname is not equal to logged in fullname.", fullname, ((TextView) solo.getView(R.id.textViewStatusStatusAuthorFullName)).getText());
		assertEquals("Registered username is not equal to logged in username.", username, ((TextView) solo.getView(R.id.textViewStatusStatusAuthorUsername)).getText());
	}
}
