package ssp.kit.test.unit;

import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyObject;
import static org.mockito.Matchers.anyString;
import static org.mockito.Matchers.isNull;
import static org.mockito.Mockito.atLeastOnce;
import static org.mockito.Mockito.doAnswer;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.ArrayList;

import org.mockito.stubbing.Answer;

import ssp.kit.ServerResponseListener;
import ssp.kit.communication.Account;
import ssp.kit.communication.Communication;
import ssp.kit.objects.Reply;
import ssp.kit.objects.Request;
import ssp.kit.objects.Status;
import ssp.kit.objects.User;
import ssp.kit.test.utilities.TestUtilities;
import android.content.Context;
import android.test.InstrumentationTestCase;

public class AccountUnitTest extends InstrumentationTestCase {
	private Account account;
	private Communication mockComm;
	
	
	private static final String username = "someUsername";
	private static final String password = "somePassword";
	private static final String fullname = "someFullname";
	
	private static final String relation = "relation";
	
	protected void setUp() throws Exception {
		System.setProperty("dexmaker.dexcache", getInstrumentation().getTargetContext().getCacheDir().getPath());
		mockComm = mock(Communication.class);
		account = new Account(mockComm);
		super.setUp();
	}
	
	@Override
	protected void tearDown() throws Exception {
		account = null;
		mockComm = null;
		super.tearDown();
	}
	
	public void testLogout() {
		account.logout(null);
		
		verify(mockComm).logout(any(Context.class));
		
		return;
	}
	
	public void testLogin() {
		ServerResponseListener mockListener = mock (ServerResponseListener.class);
		
		stubLoginSuccess();
		stubFetchSuccess();
		
		account.login(username, password, null, mockListener);
		
		verify(mockComm).logout(any(Context.class));
		verifyLoadEverything();
		verify(mockListener).onServerResponded(anyObject(), isNull(Exception.class));
		
		assertTrue(account.getUser() != null);
		assertTrue(account.getUser().getUsername().equals(username));
		
		return;
	}
	
	public void testRegister() {
		ServerResponseListener mockListener = mock (ServerResponseListener.class);
		
		Answer<Object> success = TestUtilities.getAnswer(new Object(), null);
		doAnswer(success).when(mockComm).register(anyString(), anyString(), anyString(), any(ServerResponseListener.class));
		
		stubActiveSuccess();
		stubFetchSuccess();
		
		account.register(username, fullname, password, null, mockListener);
		
		verify(mockComm).logout(any(Context.class));
		verify(mockComm).publishStatus(any(Status.class), any(ServerResponseListener.class));
		verifyLoadEverything();
		verify(mockListener).onServerResponded(anyObject(), isNull(Exception.class));
		
		assertTrue(account.getUser() != null);
		assertTrue(account.getUser().getUsername().equals(username));
		
		return;
	}
	
	
	public void testLoadEverything() {
		ServerResponseListener mockListener = mock (ServerResponseListener.class);
		
		stubFetchSuccess();
		
		account.loadEverything(null, mockListener);
		
		verifyLoadEverything();
		verify(mockListener).onServerResponded(anyObject(), isNull(Exception.class));
		
		return;
	}
	
	public void testLoadUser() {
		ServerResponseListener mockListener = mock (ServerResponseListener.class);
		
		stubFetchSuccess();
		
		account.loadUser(mockListener);
		
		verify(mockListener).onServerResponded(anyObject(), isNull(Exception.class));
		
		return;
	}
	
	public void testLoadRelations() {
		ServerResponseListener mockListener = mock (ServerResponseListener.class);
		
		stubFetchSuccess();
		
		account.loadRelations(null, mockListener);
		
		verify(mockListener).onServerResponded(anyObject(), isNull(Exception.class));
		
		return;
	}
	
	public void testLoadRepliesOf() {
		ServerResponseListener mockListener = mock (ServerResponseListener.class);
		
		stubLoginSuccess();
		stubFetchSuccess();
		
		account.login(username, password, null, mockListener);
		account.loadRepliesOf(username, mockListener);
		
		verify(mockListener, times(2)).onServerResponded(anyObject(), isNull(Exception.class));
		
		return;
	}
	
	public void testLoadRequests() {
		ServerResponseListener mockListener = mock (ServerResponseListener.class);
		
		stubFetchSuccess();
		
		account.loadRequests(mockListener);
		
		verify(mockListener).onServerResponded(anyObject(), isNull(Exception.class));
		
		return;
	}
	
	
	public void testPublishStatus() {
		ServerResponseListener mockListener = mock (ServerResponseListener.class);
		
		stubLoginSuccess();
		stubFetchSuccess();
		stubActiveSuccess();
		
		account.login(username, password, null, mockListener);
		
		Status status = mock(Status.class);
		
		account.publishStatus(status, mockListener);
		
		assertTrue(mockComm.isAuthenticated());
		
		verify(mockListener, times(2)).onServerResponded(anyObject(), isNull(Exception.class));
		verify(mockComm).publishStatus(any(Status.class), any(ServerResponseListener.class));
		
		return;
	}
	
	public void testPublishStatusPreserveReplies() {
		ServerResponseListener mockListener = mock (ServerResponseListener.class);
		
		stubLoginSuccess();
		stubFetchSuccess();
		stubActiveSuccess();
		
		account.login(username, password, null, mockListener);
		
		Status status = mock(Status.class);
		
		account.publishStatusPreserveReplies(status, mockListener);
		
		assertTrue(mockComm.isAuthenticated());
		
		verify(mockListener, times(2)).onServerResponded(anyObject(), isNull(Exception.class));
		verify(mockComm).publishStatusPreserveReplies(any(Status.class), any(ServerResponseListener.class));
		
		return;
	}
	
	public void testPublishReply() {
		ServerResponseListener mockListener = mock (ServerResponseListener.class);
		
		stubLoginSuccess();
		stubFetchSuccess();
		stubActiveSuccess();
		
		account.login(username, password, null, mockListener);
		
		Status status = new Status(username, fullname);
		Reply  reply  = mock(Reply.class);
		
		account.publishReply(reply, status, mockListener);
		
		assertTrue(mockComm.isAuthenticated());
		
		verify(mockListener, times(2)).onServerResponded(anyObject(), isNull(Exception.class));
		verify(mockComm).publishReply(any(Reply.class), any(Status.class), any(ServerResponseListener.class));
		
		return;
	}
	
	
	public void testDeleteCarer() {
		ServerResponseListener mockListener = mock (ServerResponseListener.class);
		
		stubLoginSuccess();
		stubFetchSuccess();
		stubActiveSuccess();
		stubRelationExists();
		
		account.login(username, password, null, mockListener);
		account.deleteCarer(relation, mockListener);
		
		assertTrue(mockComm.isAuthenticated());
		
		verify(mockListener, times(2)).onServerResponded(anyObject(), isNull(Exception.class));
		verify(mockComm).deleteCarer(anyString(), any(ServerResponseListener.class));
		
		return;
	}
	
	public void testDeleteAssisted() {
		ServerResponseListener mockListener = mock (ServerResponseListener.class);
		
		stubLoginSuccess();
		stubFetchSuccess();
		stubActiveSuccess();
		stubRelationExists();
		
		account.login(username, password, null, mockListener);
		account.deleteAssisted(null, relation, mockListener);
		
		assertTrue(mockComm.isAuthenticated());
		
		verify(mockListener, times(2)).onServerResponded(anyObject(), isNull(Exception.class));
		verify(mockComm).deleteAssisted(anyString(), any(ServerResponseListener.class));
		
		return;
	}
	
	
	public void testSendRequest() {
		ServerResponseListener mockListener = mock (ServerResponseListener.class);
		
		stubLoginSuccess();
		stubFetchSuccess();
		stubActiveSuccess();
		
		account.login(username, password, null, mockListener);
		
		Request bmc = new Request(username, "", Request.RequestType.BE_MY_CARER, null);
		Request bma = new Request(username, "", Request.RequestType.BE_MY_ASSISTED, null);
		
		account.sendRequest(bmc, mockListener);
		account.sendRequest(bma, mockListener);
		
		assertTrue(mockComm.isAuthenticated());
		
		verify(mockListener, times(3)).onServerResponded(anyObject(), isNull(Exception.class));
		verify(mockComm, times(2)).sendRequest(any(Request.class), any(ServerResponseListener.class));
		
		return;
	}
	
	public void testCancelRequest() {
		ServerResponseListener mockListener = mock (ServerResponseListener.class);
		
		Request request = new Request("", username, Request.RequestType.BE_MY_CARER, null);
		
		stubLoginSuccess();
		stubFetchSuccess();
		stubActiveSuccess();
		stubRequestExists(request);
		
		account.login(username, password, null, mockListener);
		account.cancelRequest(request, mockListener);
		
		assertTrue(mockComm.isAuthenticated());
		
		verify(mockListener, times(2)).onServerResponded(anyObject(), isNull(Exception.class));
		verify(mockComm).cancelRequest(any(Request.class), any(ServerResponseListener.class));
		
		return;
	}
	
	public void testApproveRequest() {
		ServerResponseListener mockListener = mock (ServerResponseListener.class);
		
		Request request = new Request("", username, Request.RequestType.BE_MY_CARER, null);
		
		stubLoginSuccess();
		stubFetchSuccess();
		stubActiveSuccess();
		stubRequestExists(request);
		
		account.login(username, password, null, mockListener);
		account.approveRequest(null, request, mockListener);
		
		assertTrue(mockComm.isAuthenticated());
		
		verify(mockListener, times(2)).onServerResponded(anyObject(), isNull(Exception.class));
		verify(mockComm).approveRequest(any(Request.class), any(ServerResponseListener.class));
		
		return;
	}
	
	
	
	@SuppressWarnings("unchecked")
	private void verifyLoadEverything() {
		verify(mockComm, atLeastOnce()).getUser(any(ServerResponseListener.class));
		
		verify(mockComm, atLeastOnce()).getCarerNames(any(ServerResponseListener.class));
		verify(mockComm, atLeastOnce()).getAssistedNames(any(ServerResponseListener.class));
		verify(mockComm, atLeastOnce()).getRequests(any(ServerResponseListener.class));
		
		verify(mockComm, atLeastOnce()).unsubscribeAll(any(Context.class));
		verify(mockComm, atLeastOnce()).enablePush(any(Context.class));
		verify(mockComm, atLeastOnce()).subscribe(any(Context.class), any(ArrayList.class));
		return;
	}
	
	
	private void stubLoggedIn() {
		when(mockComm.isAuthenticated()).thenReturn(true);
		
		return;
	}
	
	private void stubLoginSuccess() {
		Answer<Object> success = TestUtilities.getAnswer(new Object(), null);
		doAnswer(success).when(mockComm).login(anyString(), anyString(), any(ServerResponseListener.class));
		
		return;
	}
	
	private void stubFetchSuccess() {
		Answer<Object> userResponse = TestUtilities.getAnswer(new User(username, fullname), null);
		Answer<Object> relaResponse = TestUtilities.getAnswer(new User(relation), null);
		Answer<Object> statResponse = TestUtilities.getAnswer(new Status(username, fullname), null);
		Answer<Object> listResponse = TestUtilities.getAnswer(new ArrayList<Object>(), null); 
		
		doAnswer(userResponse).when(mockComm).getUser(any(ServerResponseListener.class));
		
		doAnswer(relaResponse).when(mockComm).getUser(anyString(), any(ServerResponseListener.class));
		
		doAnswer(statResponse).when(mockComm).getStatus(any(User.class), any(ServerResponseListener.class)); 
		
		doAnswer(listResponse).when(mockComm).getCarerNames(any(ServerResponseListener.class));
		doAnswer(listResponse).when(mockComm).getAssistedNames(any(ServerResponseListener.class));
		doAnswer(listResponse).when(mockComm).getReplies(any(Status.class), any(ServerResponseListener.class));
		doAnswer(listResponse).when(mockComm).getRequests(any(ServerResponseListener.class));
		
		return;
	}
	
	private void stubRelationExists() {
		ArrayList<User> namelist = new ArrayList<User>();
		namelist.add(new User(relation));
		
		Answer<Object> nameResponse = TestUtilities.getAnswer(namelist, null);
		
		doAnswer(nameResponse).when(mockComm).getCarerNames(any(ServerResponseListener.class));
		doAnswer(nameResponse).when(mockComm).getAssistedNames(any(ServerResponseListener.class));
		
		return;
	}
	
	private void stubRequestExists(Request request) {
		ArrayList<Request> reqlist = new ArrayList<Request>();
		reqlist.add(request);
		
		Answer<Object> reqResponse = TestUtilities.getAnswer(reqlist, null);
		
		doAnswer(reqResponse).when(mockComm).getRequests(any(ServerResponseListener.class));
		
		return;
	}
	
	private void stubActiveSuccess() {
		stubLoggedIn();
		
		Answer<Object> success = TestUtilities.getAnswer(new Object(), null);
		
		doAnswer(success).when(mockComm).publishStatus(any(Status.class), any(ServerResponseListener.class));
		doAnswer(success).when(mockComm).publishStatusPreserveReplies(any(Status.class), any(ServerResponseListener.class));
		doAnswer(success).when(mockComm).publishReply(any(Reply.class), any(Status.class), any(ServerResponseListener.class));
		doAnswer(success).when(mockComm).deleteCarer(anyString(), any(ServerResponseListener.class));
		doAnswer(success).when(mockComm).deleteAssisted(anyString(), any(ServerResponseListener.class));
		doAnswer(success).when(mockComm).sendRequest(any(Request.class), any(ServerResponseListener.class));
		doAnswer(success).when(mockComm).approveRequest(any(Request.class), any(ServerResponseListener.class));
		doAnswer(success).when(mockComm).cancelRequest(any(Request.class), any(ServerResponseListener.class));
		
		return;
	}
	
}
