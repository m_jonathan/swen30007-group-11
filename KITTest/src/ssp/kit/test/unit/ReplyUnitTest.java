package ssp.kit.test.unit;

import java.util.Date;
import ssp.kit.objects.Reply;
import ssp.kit.communication.Symbols;
import junit.framework.TestCase;

public class ReplyUnitTest extends TestCase {
	private final Date timestamp = new Date();
	private final Date timestampNull = null;
	private final String username = "username";
	private final String fullname = "username fullname";
	private final String message = "A message.";
	private Reply reply1;
	private Reply reply2;
	@Override
	public void setUp() throws Exception {
		super.setUp();
		this.reply1 = new Reply(timestamp, username, fullname, message);
		this.reply2 = new Reply(timestampNull, username, fullname, message);
		assertNotNull(this.reply1);
		assertNotNull(this.reply2);
	}
	
	public void testClone() {
		Object reply1Clone = reply1.clone();
		Object reply2Clone = reply2.clone();
		assertNotNull(reply1Clone);
		assertTrue(reply1Clone instanceof Reply);
		assertNotNull(reply2Clone);
		assertTrue(reply2Clone instanceof Reply);
		
		//Checking the attributes of the reply 1 clone
		assertEquals(reply1.getTimestamp(), ((Reply) reply1Clone).getTimestamp());
		assertEquals(reply1.getUsername(), ((Reply) reply1Clone).getUsername());
		assertEquals(reply1.getFullName(), ((Reply) reply1Clone).getFullName());
		assertEquals(reply1.getMessage(), ((Reply) reply1Clone).getMessage());
		
		//Checking the attributes of the reply 2 clone
		assertEquals(reply2.getTimestamp(), ((Reply) reply2Clone).getTimestamp());
		assertEquals(reply2.getUsername(), ((Reply) reply2Clone).getUsername());
		assertEquals(reply2.getFullName(), ((Reply) reply2Clone).getFullName());
		assertEquals(reply2.getMessage(), ((Reply) reply2Clone).getMessage());
	}
	
	public void testGetPushMessage() {
		String comparison = this.fullname + Symbols.replymsg + this.message;
		assertEquals(comparison, this.reply1.getPushMessage());
		assertEquals(comparison, this.reply2.getPushMessage());
	}
	
	public void testGetTimestamp() {
		assertTrue(timestamp.equals(this.reply1.getTimestamp()));
		assertTrue(timestamp != reply1.getTimestamp());
		assertNull(this.reply2.getTimestamp());
	}
	
	@Override
	public void tearDown() throws Exception {
		super.tearDown();
		this.reply1 = null;
		this.reply2 = null;
	}
}
