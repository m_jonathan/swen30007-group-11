package ssp.kit.test.unit;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyObject;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.*;

import java.util.Date;

import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;

import android.test.InstrumentationTestCase;
import ssp.kit.ServerResponseListener;
import ssp.kit.communication.Communication;
import ssp.kit.communication.Symbols;
import ssp.kit.objects.Reply;
import ssp.kit.objects.Status;
import ssp.kit.objects.Status.Signal;
import ssp.kit.objects.Status.Urgency;

public class StatusUnitTest extends InstrumentationTestCase {
	private final String username1 = "username1";
	private final String username2 = "username2";
	private final String fullname1 = "fullname1";
	private final String fullname2 = "fullname2";
	private final String message1 = "message1";
	private final String message2 = "message2";
	private final Signal signalOK = Signal.OK;
	private final Signal signalHelp = Signal.HELP;
	private final Urgency lt = Urgency.LIFE_THREATENING;
	private final Date timestamp = new Date();
	private Communication mockComm;
	
	
	@Override
	protected void setUp() throws Exception {
		System.setProperty("dexmaker.dexcache", getInstrumentation().getTargetContext().getCacheDir().getPath());
		mockComm = mock(Communication.class);
		super.setUp();
	}
	
	@Override
	protected void tearDown() throws Exception {
		mockComm = null;
		super.tearDown();
	}
	
	public void testSameAs() {
		Status status1 = new Status(this.timestamp, this.username1, this.fullname1, this.message1, this.signalOK, null);
		Status status2 = new Status(this.timestamp, this.username2, this.fullname1, this.message1, this.signalOK, null);
		Status status3 = new Status(this.timestamp, this.username1, this.fullname2, this.message1, this.signalOK, null);
		Status status4 = new Status(this.timestamp, this.username1, this.fullname1, this.message2, this.signalOK, null);
		Status status5 = new Status(this.timestamp, this.username1, this.fullname1, this.message1, this.signalHelp, null);
		Status status6 = new Status(this.timestamp, this.username1, this.fullname1, this.message1, this.signalOK, null);
		assertTrue(!(status1.sameAs(null)));
		assertTrue(!(status1.sameAs(status2)));
		assertTrue(!(status1.sameAs(status3)));
		assertTrue(!(status1.sameAs(status4)));
		assertTrue(!(status1.sameAs(status5)));
		assertTrue(status1.sameAs(status6));
	}
	
	public void testLoadReplies() {
		Status status = new Status(this.timestamp, this.username1, this.fullname1, this.message1, this.signalOK, null);
		doAnswer(getAnswer(new Object(), null)).when(mockComm).getReplies(any(Status.class), any(ServerResponseListener.class));
		ServerResponseListener mockServerResponseListener = mock (ServerResponseListener.class);
		status.loadReplies(mockComm,mockServerResponseListener);
		verify(mockServerResponseListener).onServerResponded(anyObject(), (Exception) eq(null));
	}
	
	public void testUpdateCacheStatus() {
		Status status = new Status(this.timestamp, this.username1, this.fullname1, this.message1, this.signalOK, null);
		Status updatedStatus = new Status(null, null, null, this.message2, this.signalHelp, this.lt);
		status.updateCacheStatus(updatedStatus);
		assertNull(status.getTimestamp());
		assertEquals(this.message2, status.getMessage());
		assertEquals(this.signalHelp, status.getSignal());
		assertEquals(this.lt, status.getUrgency());
		assertEquals(0, status.getReplies().size());
	}
	
	public void testUpdateCacheStatusPreserveReplies() {
		Status status = new Status(this.timestamp, this.username1, this.fullname1, this.message1, this.signalOK, null);
		Status updatedStatus = new Status(null, null, null, this.message2, this.signalHelp, this.lt);
		status.updateCacheStatusPreserveReplies(updatedStatus);
		assertNull(status.getTimestamp());
		assertEquals(this.message2, status.getMessage());
		assertEquals(this.signalHelp, status.getSignal());
		assertEquals(this.lt, status.getUrgency());
		assertEquals(0, status.getReplies().size());
	}
	
	public void testAddCacheReply() {
		Status status = new Status(this.timestamp, this.username1, this.fullname1, this.message1, this.signalOK, null);
		Reply mockReply = mock(Reply.class);
		status.addCacheReply(mockReply);
		assertEquals(1, status.getReplies().size());
	}
	
	public void testClone() {
		Status status = new Status(this.timestamp, this.username1, this.fullname1, this.message1, this.signalOK, null);
		Object statusClone = status.clone();
		assertNotNull(statusClone);
		assertTrue(statusClone instanceof Status);
		
		//Checking the attributes of the cloned status
		assertEquals(status.getTimestamp(), ((Status) statusClone).getTimestamp());
		assertEquals(status.getUsername(), ((Status) statusClone).getUsername());
		assertEquals(status.getFullName(), ((Status) statusClone).getFullName());
		assertEquals(status.getMessage(), ((Status) statusClone).getMessage());
		assertEquals(status.getSignal(), ((Status) statusClone).getSignal());
		assertEquals(status.getUrgency(), ((Status) statusClone).getUrgency());
	}
	
	public void testGetPushMessage() {
		Status status1 = new Status(null, this.username1, this.fullname1, "", this.signalOK, null);
		Status status2 = new Status(null, this.username1, this.fullname1, this.message1, this.signalOK, null);
		Status status3 = new Status(null, this.username1, this.fullname1, "", this.signalHelp, null);
		Status status4 = new Status(null, this.username1, this.fullname1, this.message1, this.signalHelp, null);
		String expected1 = this.fullname1 + Symbols.oknomsg;
		String expected2 = this.fullname1 + Symbols.okmsg + this.message1;
		String expected3 = this.fullname1 + Symbols.helpnomsg;
		String expected4 = this.fullname1 + Symbols.helpmsg + this.message1;
		assertEquals(expected1, status1.getPushMessage());
		assertEquals(expected2, status2.getPushMessage());
		assertEquals(expected3, status3.getPushMessage());
		assertEquals(expected4, status4.getPushMessage());
	}
	
	public void testGetTimestamp() {
		Status status1 = new Status(null, null);
		Status status2 = new Status(this.timestamp, null, null, null, null, null);
		assertNull(status1.getTimestamp());
		assertTrue(timestamp.equals(status2.getTimestamp()));
		assertTrue(timestamp != status1.getTimestamp());
	}
	
	public void testGetReplies() {
		Status status = new Status(null, null);
		Reply mockReply1 = mock(Reply.class);
		Reply mockReply2 = mock(Reply.class);
		status.addCacheReply(mockReply1);
		status.addCacheReply(mockReply2);
		assertEquals(2, status.getReplies().size());
	}
	
	private Answer<Object> getAnswer(final Object o, final Exception e) {
		return new Answer<Object>() {

			@Override
			public Object answer(InvocationOnMock invocation) throws Throwable {
				Object[] argsObjects = invocation.getArguments();
				
				int last = invocation.getArguments().length - 1;
				assertTrue(argsObjects[last] instanceof ServerResponseListener);
				
				ServerResponseListener listener = (ServerResponseListener) argsObjects[last];
				
				listener.onServerResponded(o, e);
				
				return null;
			}
		};
	}
}
