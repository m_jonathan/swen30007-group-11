package ssp.kit.test.unit;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyObject;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.*;

import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;

import android.test.InstrumentationTestCase;
import ssp.kit.ServerResponseListener;
import ssp.kit.communication.Communication;
import ssp.kit.objects.Status;
import ssp.kit.objects.User;

public class UserUnitTest extends InstrumentationTestCase {
	
	private Communication mockComm;
	private final String username = "username";
	private final String fullname = "fullname";
	private Status mockStatus;
	private User user;
	
	@Override
	protected void setUp() throws Exception {
		System.setProperty("dexmaker.dexcache", getInstrumentation().getTargetContext().getCacheDir().getPath());
		mockComm = mock(Communication.class);
		mockStatus = mock(Status.class);
		user = new User(username, fullname, mockStatus);
		super.setUp();
	}
	
	@Override
	protected void tearDown() throws Exception {
		user = null;
		mockComm = null;
		mockStatus = null;
		super.tearDown();
	}
	
	public void testLoadStatusReplies() {
		doAnswer(getAnswer(new Object(), null)).when(mockComm).getStatus(any(User.class), any(ServerResponseListener.class));
		ServerResponseListener mockServerResponseListener = mock (ServerResponseListener.class);
		user.loadStatusReplies(mockComm, mockServerResponseListener);
		verify(mockServerResponseListener).onServerResponded(anyObject(), (Exception) eq(null));
	}
	
	public void testLoadStatus() {
		doAnswer(getAnswer(new Object(), null)).when(mockComm).getStatus(any(User.class), any(ServerResponseListener.class));
		ServerResponseListener mockServerResponseListener = mock (ServerResponseListener.class);
		user.loadStatus(mockComm, mockServerResponseListener);
		verify(mockServerResponseListener).onServerResponded(anyObject(), (Exception) eq(null));
	}
	
	public void testClone() {
		Object userClone = user.clone();
		assertNotNull(userClone);
		assertTrue(userClone instanceof User);
		
		//Checking the attributes of the cloned user
		assertEquals(user.getUsername(), ((User) userClone).getUsername());
		assertEquals(user.getFullName(), ((User) userClone).getFullName());
		assertEquals(user.getStatus(), ((User) userClone).getStatus());
	}
	
	public void testGetStatus() {
		when(mockStatus.clone()).thenCallRealMethod();
		Status clonedStatus = user.getStatus();
		assertNotNull(clonedStatus);
		assertTrue(clonedStatus instanceof Status);
	}
	
	private Answer<Object> getAnswer(final Object o, final Exception e) {
		return new Answer<Object>() {

			@Override
			public Object answer(InvocationOnMock invocation) throws Throwable {
				Object[] argsObjects = invocation.getArguments();
				
				int last = invocation.getArguments().length - 1;
				assertTrue(argsObjects[last] instanceof ServerResponseListener);
				
				ServerResponseListener listener = (ServerResponseListener) argsObjects[last];
				
				listener.onServerResponded(o, e);
				
				return null;
			}
		};
	}
}
