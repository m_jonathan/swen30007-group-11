package ssp.kit.test.unit;

import static org.mockito.Mockito.*;

import java.util.ArrayList;
import java.util.Date;

import org.mockito.ArgumentCaptor;

import ssp.kit.KIT;
import ssp.kit.ServerResponseListener;
import ssp.kit.communication.Account;
import ssp.kit.exception.InvalidStatusException;
import ssp.kit.exception.UserNotFoundException;
import ssp.kit.objects.Reply;
import ssp.kit.objects.Request;
import ssp.kit.objects.Status;
import ssp.kit.objects.Status.Signal;
import ssp.kit.objects.Status.Urgency;
import ssp.kit.objects.User;
import android.test.InstrumentationTestCase;

public class KITUnitTest extends InstrumentationTestCase {
	
	Account mockAccount;
	
	@Override
	protected void setUp() throws Exception {
		super.setUp();
		System.setProperty("dexmaker.dexcache", getInstrumentation().getTargetContext().getCacheDir().getPath());
		mockAccount = mock(Account.class);
		KIT.init(mockAccount);
	}
	
	@Override
	protected void tearDown() throws Exception {
		super.tearDown();
		KIT.reset();
	}
	
	public void testPublishStatusOK() {
		// Arrange
		String username = "testUsername";
		String fullname = "testFullname";
		String message = "testMessage";
		ServerResponseListener listener = mock(ServerResponseListener.class);

		User mockUser = mock(User.class);
		when(mockUser.getUsername()).thenReturn(username);
		when(mockUser.getFullName()).thenReturn(fullname);
		when(mockAccount.getUser()).thenReturn(mockUser);
		
		// Act
		KIT.app().publishStatusOK(message, listener);
		
		// Assert
		ArgumentCaptor<Status> capturedStatus = ArgumentCaptor.forClass(Status.class);
		ArgumentCaptor<ServerResponseListener> capturedListener = ArgumentCaptor.forClass(ServerResponseListener.class);
		verify(mockAccount).publishStatus(capturedStatus.capture(), capturedListener.capture());
		assertNull(capturedStatus.getValue().getTimestamp());
		assertEquals(capturedStatus.getValue().getUsername(), username);
		assertEquals(capturedStatus.getValue().getFullName(), fullname);
		assertEquals(capturedStatus.getValue().getMessage(), message);
		assertEquals(capturedStatus.getValue().getSignal(), Signal.OK);
		assertNull(capturedStatus.getValue().getUrgency());
		assertEquals(capturedListener.getValue(), listener);
	}
	
	public void testPublishStatusHelp() {
		// Arrange
		String username = "testUsername";
		String fullname = "testFullname";
		String message = "";
		ServerResponseListener listener = mock(ServerResponseListener.class);

		User mockUser = mock(User.class);
		when(mockUser.getUsername()).thenReturn(username);
		when(mockUser.getFullName()).thenReturn(fullname);
		when(mockAccount.getUser()).thenReturn(mockUser);
		
		// Act
		KIT.app().publishStatusHelp(listener);
		
		// Assert
		ArgumentCaptor<Status> capturedStatus = ArgumentCaptor.forClass(Status.class);
		ArgumentCaptor<ServerResponseListener> capturedListener = ArgumentCaptor.forClass(ServerResponseListener.class);
		verify(mockAccount).publishStatus(capturedStatus.capture(), capturedListener.capture());
		assertNull(capturedStatus.getValue().getTimestamp());
		assertEquals(capturedStatus.getValue().getUsername(), username);
		assertEquals(capturedStatus.getValue().getFullName(), fullname);
		assertEquals(capturedStatus.getValue().getMessage(), message);
		assertEquals(capturedStatus.getValue().getSignal(), Signal.HELP);
		assertNull(capturedStatus.getValue().getUrgency());
		assertEquals(capturedListener.getValue(), listener);
	}
	
	public void testUpdateStatusHelpUrgency() {
		// Arrange
		String username = "testUsername";
		String fullname = "testFullname";
		Date timestamp = new Date();
		String message = "testMessage";
		Signal signal = Signal.HELP;
		ArrayList<Reply> replies = new ArrayList<Reply>();
		ServerResponseListener listener = mock(ServerResponseListener.class);
		
		Status mockStatus = mock(Status.class);
		when(mockStatus.getTimestamp()).thenReturn(timestamp);
		when(mockStatus.getMessage()).thenReturn(message);
		when(mockStatus.getSignal()).thenReturn(signal);
		when(mockStatus.getReplies()).thenReturn(replies);
		
		User mockUser = mock(User.class);
		when(mockUser.getUsername()).thenReturn(username);
		when(mockUser.getFullName()).thenReturn(fullname);
		when(mockUser.getStatus()).thenReturn(mockStatus);
		
		when(mockAccount.getUser()).thenReturn(mockUser);
		
		Urgency[] urgencies = {Urgency.LIFE_THREATENING, Urgency.NON_LIFE_THREATENING};
		
		for (Urgency urgency : urgencies) {
			// Act
			KIT.app().updateStatusHelpUrgency(urgency, listener);
			
			// Assert
			ArgumentCaptor<Status> capturedStatus = ArgumentCaptor.forClass(Status.class);
			ArgumentCaptor<ServerResponseListener> capturedListener = ArgumentCaptor.forClass(ServerResponseListener.class);
			verify(mockAccount, atLeast(1)).publishStatus(capturedStatus.capture(), capturedListener.capture());
			assertEquals(capturedStatus.getValue().getTimestamp(), timestamp);
			assertEquals(capturedStatus.getValue().getUsername(), username);
			assertEquals(capturedStatus.getValue().getFullName(), fullname);
			assertEquals(capturedStatus.getValue().getMessage(), message);
			assertEquals(capturedStatus.getValue().getSignal(), signal);
			assertEquals(capturedStatus.getValue().getUrgency(), urgency);
			assertEquals(capturedListener.getValue(), listener);
		}
	}
	
	public void testUpdateStatusHelpUrgencyOnlyAllowedDuringHelpRequesting() {
		// Arrange
		ServerResponseListener listener = mock(ServerResponseListener.class);
		
		Status mockStatus = mock(Status.class);
		when(mockStatus.getSignal()).thenReturn(Signal.OK);
		
		User mockUser = mock(User.class);
		when(mockUser.getStatus()).thenReturn(mockStatus);
		
		when(mockAccount.getUser()).thenReturn(mockUser);
		
		Urgency[] urgencies = {Urgency.LIFE_THREATENING, Urgency.NON_LIFE_THREATENING};
		
		for (Urgency urgency : urgencies) {
			// Act
			KIT.app().updateStatusHelpUrgency(urgency, listener);
			
			// Assert
			ArgumentCaptor<Object> capturedReturnedObject = ArgumentCaptor.forClass(Object.class);
			ArgumentCaptor<Exception> capturedException = ArgumentCaptor.forClass(Exception.class);
			verify(mockAccount, atLeast(1)).getUser();
			verifyNoMoreInteractions(mockAccount);
			verify(listener, atLeast(1)).onServerResponded(capturedReturnedObject.capture(), capturedException.capture());
			assertNull(capturedReturnedObject.getValue());
			assertTrue(capturedException.getValue() instanceof InvalidStatusException);
		}
	}
	
	public void testUpdateStatusHelpMessage() {
		// Arrange
		String username = "testUsername";
		String fullname = "testFullname";
		Date timestamp = new Date();
		String message = "testMessage";
		Signal signal = Signal.HELP;
		Urgency urgency = Urgency.LIFE_THREATENING;
		ArrayList<Reply> replies = new ArrayList<Reply>();
		ServerResponseListener listener = mock(ServerResponseListener.class);
		
		Status mockStatus = mock(Status.class);
		when(mockStatus.getTimestamp()).thenReturn(timestamp);
		when(mockStatus.getMessage()).thenReturn("someOldMessage");
		when(mockStatus.getSignal()).thenReturn(signal);
		when(mockStatus.getUrgency()).thenReturn(urgency);
		when(mockStatus.getReplies()).thenReturn(replies);
		
		User mockUser = mock(User.class);
		when(mockUser.getUsername()).thenReturn(username);
		when(mockUser.getFullName()).thenReturn(fullname);
		when(mockUser.getStatus()).thenReturn(mockStatus);
		
		when(mockAccount.getUser()).thenReturn(mockUser);
		
		// Act
		KIT.app().updateStatusHelpMessage(message, listener);
		
		// Assert
		ArgumentCaptor<Status> capturedStatus = ArgumentCaptor.forClass(Status.class);
		ArgumentCaptor<ServerResponseListener> capturedListener = ArgumentCaptor.forClass(ServerResponseListener.class);
		verify(mockAccount).publishStatus(capturedStatus.capture(), capturedListener.capture());
		assertEquals(capturedStatus.getValue().getTimestamp(), timestamp);
		assertEquals(capturedStatus.getValue().getUsername(), username);
		assertEquals(capturedStatus.getValue().getFullName(), fullname);
		assertEquals(capturedStatus.getValue().getMessage(), message);
		assertEquals(capturedStatus.getValue().getSignal(), signal);
		assertEquals(capturedStatus.getValue().getUrgency(), urgency);
		assertEquals(capturedListener.getValue(), listener);
	}

	public void testUpdateStatusHelpMessageOnlyAllowedDuringHelpRequesting() {
		// Arrange
		ServerResponseListener listener = mock(ServerResponseListener.class);
		
		Status mockStatus = mock(Status.class);
		when(mockStatus.getSignal()).thenReturn(Signal.OK);
		
		User mockUser = mock(User.class);
		when(mockUser.getStatus()).thenReturn(mockStatus);
		
		when(mockAccount.getUser()).thenReturn(mockUser);
		
		// Act
		KIT.app().updateStatusHelpMessage("testMessage", listener);
		
		// Assert
		ArgumentCaptor<Object> capturedReturnedObject = ArgumentCaptor.forClass(Object.class);
		ArgumentCaptor<Exception> capturedException = ArgumentCaptor.forClass(Exception.class);
		verify(mockAccount, atLeast(1)).getUser();
		verifyNoMoreInteractions(mockAccount);
		verify(listener, atLeast(1)).onServerResponded(capturedReturnedObject.capture(), capturedException.capture());
		assertNull(capturedReturnedObject.getValue());
		assertTrue(capturedException.getValue() instanceof InvalidStatusException);
	}
	
	public void testPublishReplyToOwnStatus() {
		// Arrange
		String username = "testUsername";
		String fullname = "testFullname";
		
		String message = "testReplyMessage";
		String statusAuthorUsername = username;
		Date statusTimestamp = new Date();
		ServerResponseListener listener = mock(ServerResponseListener.class);
		
		Status mockStatus = mock(Status.class);
		when(mockStatus.getSignal()).thenReturn(Signal.OK);
		when(mockStatus.getTimestamp()).thenReturn(statusTimestamp);
		
		User mockUser = mock(User.class);
		when(mockUser.getUsername()).thenReturn(username);
		when(mockUser.getFullName()).thenReturn(fullname);
		when(mockUser.getStatus()).thenReturn(mockStatus);
		
		when(mockAccount.getUser()).thenReturn(mockUser);
		
		// Act
		KIT.app().publishReply(message, statusAuthorUsername, statusTimestamp, listener);
		
		// Assert
		ArgumentCaptor<Reply> capturedReply = ArgumentCaptor.forClass(Reply.class);
		ArgumentCaptor<Status> capturedStatus = ArgumentCaptor.forClass(Status.class);
		ArgumentCaptor<ServerResponseListener> capturedListener = ArgumentCaptor.forClass(ServerResponseListener.class);
		verify(mockAccount).publishReply(capturedReply.capture(), capturedStatus.capture(), capturedListener.capture());
		assertNull(capturedReply.getValue().getTimestamp());
		assertEquals(username, capturedReply.getValue().getUsername());
		assertEquals(fullname, capturedReply.getValue().getFullName());
		assertEquals(message, capturedReply.getValue().getMessage());
		assertEquals(mockStatus, capturedStatus.getValue());
		assertEquals(listener, capturedListener.getValue());
	}
	
	public void testPublishReplyToAssistedStatus() {
		// Arrange
		String username = "testUsername";
		String fullname = "testFullname";
		String assistedUsername = "testAssistedUsername";
		
		String message = "testReplyMessage";
		String statusAuthorUsername = assistedUsername;
		Date statusTimestamp = new Date();
		ServerResponseListener listener = mock(ServerResponseListener.class);
		
		Status mockStatus = mock(Status.class);
		when(mockStatus.getSignal()).thenReturn(Signal.OK);
		when(mockStatus.getTimestamp()).thenReturn(statusTimestamp);
		
		User mockUser = mock(User.class);
		when(mockUser.getUsername()).thenReturn(username);
		when(mockUser.getFullName()).thenReturn(fullname);
		
		User mockAssistedUser = mock(User.class);
		when(mockAssistedUser.getStatus()).thenReturn(mockStatus);
		
		when(mockAccount.getUser()).thenReturn(mockUser);
		when(mockAccount.getAssistedPerson(anyString())).thenReturn(mockAssistedUser);
		
		// Act
		KIT.app().publishReply(message, statusAuthorUsername, statusTimestamp, listener);
		
		// Assert
		ArgumentCaptor<String> capturedString = ArgumentCaptor.forClass(String.class);
		ArgumentCaptor<Reply> capturedReply = ArgumentCaptor.forClass(Reply.class);
		ArgumentCaptor<Status> capturedStatus = ArgumentCaptor.forClass(Status.class);
		ArgumentCaptor<ServerResponseListener> capturedListener = ArgumentCaptor.forClass(ServerResponseListener.class);
		verify(mockAccount).getAssistedPerson(capturedString.capture());
		assertEquals(assistedUsername, capturedString.getValue());
		verify(mockAccount).publishReply(capturedReply.capture(), capturedStatus.capture(), capturedListener.capture());
		assertNull(capturedReply.getValue().getTimestamp());
		assertEquals(username, capturedReply.getValue().getUsername());
		assertEquals(fullname, capturedReply.getValue().getFullName());
		assertEquals(message, capturedReply.getValue().getMessage());
		assertEquals(mockStatus, capturedStatus.getValue());
		assertEquals(listener, capturedListener.getValue());
	}
	
	public void testPublishReplyToInvalidUser() {
		// Arrange
		String username = "testUsername";
		String nonExistentUsername = "testNonExistentUsername";
		
		String message = "testReplyMessage";
		String statusAuthorUsername = nonExistentUsername;
		Date statusTimestamp = new Date();
		ServerResponseListener listener = mock(ServerResponseListener.class);
		
		User mockUser = mock(User.class);
		when(mockUser.getUsername()).thenReturn(username);
		
		when(mockAccount.getUser()).thenReturn(mockUser);
		when(mockAccount.getAssistedPerson(anyString())).thenReturn(null);
		
		// Act
		KIT.app().publishReply(message, statusAuthorUsername, statusTimestamp, listener);
		
		// Assert
		ArgumentCaptor<String> capturedString = ArgumentCaptor.forClass(String.class);
		ArgumentCaptor<Object> capturedReturnedObject = ArgumentCaptor.forClass(Object.class);
		ArgumentCaptor<Exception> capturedException = ArgumentCaptor.forClass(Exception.class);
		verify(mockAccount).getUser();
		verify(mockAccount).getAssistedPerson(capturedString.capture());
		assertEquals(nonExistentUsername, capturedString.getValue());
		verify(listener).onServerResponded(capturedReturnedObject.capture(), capturedException.capture());
		assertNull(capturedReturnedObject.getValue());
		assertTrue(capturedException.getValue() instanceof UserNotFoundException);
		verifyNoMoreInteractions(mockAccount);
	}
	
	public void testPublishReplyToExpiredStatus() {
		// Arrange
		String username = "testUsername";
		String fullname = "testFullname";
		String assistedUsername = "testAssistedUsername";
		
		String message = "testReplyMessage";
		String statusAuthorUsername = assistedUsername;
		Date statusTimestamp = new Date();
		statusTimestamp.setTime(10);
		ServerResponseListener listener = mock(ServerResponseListener.class);
		
		Status mockStatus = mock(Status.class);
		when(mockStatus.getSignal()).thenReturn(Signal.OK);
		Date oldTimestamp = new Date();
		oldTimestamp.setTime(10000);
		when(mockStatus.getTimestamp()).thenReturn(oldTimestamp);
		
		User mockUser = mock(User.class);
		when(mockUser.getUsername()).thenReturn(username);
		when(mockUser.getFullName()).thenReturn(fullname);
		
		User mockAssistedUser = mock(User.class);
		when(mockAssistedUser.getStatus()).thenReturn(mockStatus);
		
		when(mockAccount.getUser()).thenReturn(mockUser);
		when(mockAccount.getAssistedPerson(anyString())).thenReturn(mockAssistedUser);
		
		// Act
		KIT.app().publishReply(message, statusAuthorUsername, statusTimestamp, listener);
		
		// Assert
		ArgumentCaptor<Object> capturedReturnedObject = ArgumentCaptor.forClass(Object.class);
		ArgumentCaptor<Exception> capturedException = ArgumentCaptor.forClass(Exception.class);
		verify(listener).onServerResponded(capturedReturnedObject.capture(), capturedException.capture());
		assertNull(capturedReturnedObject.getValue());
		assertTrue(capturedException.getValue() instanceof InvalidStatusException);
	}
	
	public void testGetSentRequests() {
		// Arrange
		// Setting up mockCurrentUser
		String expectedRequester = "expectedRequester";
		String otherRequester = "otherRequester";
		String expectedTargeted = "expectedTargeted";
		String otherTargeted = "otherTargeted";
		User mockCurrentUser = mock(User.class);
		when(mockCurrentUser.getUsername()).thenReturn(expectedRequester);

		// Setting up mockRequests
		Request mockRequest1 = mock(Request.class);
		when(mockRequest1.getRequester()).thenReturn(otherRequester);
		when(mockRequest1.getTargeted()).thenReturn(otherTargeted);
		when(mockRequest1.getType()).thenReturn(Request.RequestType.BE_MY_CARER);
		when(mockRequest1.clone()).thenReturn(mockRequest1);
		
		Request mockRequest2 = mock(Request.class);
		when(mockRequest2.getRequester()).thenReturn(expectedRequester);
		when(mockRequest2.getTargeted()).thenReturn(expectedTargeted);
		when(mockRequest2.getType()).thenReturn(Request.RequestType.BE_MY_CARER);
		when(mockRequest2.clone()).thenReturn(mockRequest2);
		
		Request mockRequest3 = mock(Request.class);
		when(mockRequest3.getRequester()).thenReturn(expectedRequester);
		when(mockRequest3.getTargeted()).thenReturn(otherTargeted);
		when(mockRequest3.getType()).thenReturn(Request.RequestType.BE_MY_ASSISTED);
		when(mockRequest3.clone()).thenReturn(mockRequest3);
		when(mockRequest3.clone()).thenReturn(mockRequest3);
		
		Request mockRequest4 = mock(Request.class);
		when(mockRequest4.getRequester()).thenReturn(otherRequester);
		when(mockRequest4.getTargeted()).thenReturn(otherTargeted);
		when(mockRequest4.getType()).thenReturn(Request.RequestType.BE_MY_ASSISTED);
		when(mockRequest4.clone()).thenReturn(mockRequest4);
		
		ArrayList<Request> mockRequests = new ArrayList<Request>();
		mockRequests.add(mockRequest1);
		mockRequests.add(mockRequest2);
		mockRequests.add(mockRequest3);
		mockRequests.add(mockRequest4);
		
		// Setting up mockAccount
		when(mockAccount.getUser()).thenReturn(mockCurrentUser);
		when(mockAccount.getRequests()).thenReturn(mockRequests);
		
		// Act
		ArrayList<Request> sentRequests = KIT.app().getSentRequests(Request.RequestType.BE_MY_CARER);
		
		// Assert
		assertEquals("Returned too many requests",1,sentRequests.size());
		Request returnedRequest = sentRequests.get(0);
		assertNotNull("Returned Request is Null",returnedRequest);
		assertEquals("Returned Request is not from the correct requester",expectedRequester,returnedRequest.getRequester());
		assertEquals("Returned Request is not to the correct targeted",expectedTargeted,returnedRequest.getTargeted());
		assertNotNull("Returned Request has Null type",returnedRequest.getType());
		assertEquals("Returned Request is not of the correct type",Request.RequestType.BE_MY_CARER,returnedRequest.getType());
	}
	
	public void testGetReceivedRequests() {
		// Arrange
		// Setting up mockCurrentUser
		String expectedRequester = "expectedRequester";
		String otherRequester = "otherRequester";
		String expectedTargeted = "expectedTargeted";
		String otherTargeted = "otherTargeted";
		User mockCurrentUser = mock(User.class);
		when(mockCurrentUser.getUsername()).thenReturn(otherRequester);

		// Setting up mockRequests
		Request mockRequest1 = mock(Request.class);
		when(mockRequest1.getRequester()).thenReturn(otherRequester);
		when(mockRequest1.getTargeted()).thenReturn(otherTargeted);
		when(mockRequest1.getType()).thenReturn(Request.RequestType.BE_MY_CARER);
		when(mockRequest1.clone()).thenReturn(mockRequest1);
		
		Request mockRequest2 = mock(Request.class);
		when(mockRequest2.getRequester()).thenReturn(expectedRequester);
		when(mockRequest2.getTargeted()).thenReturn(expectedTargeted);
		when(mockRequest2.getType()).thenReturn(Request.RequestType.BE_MY_CARER);
		when(mockRequest2.clone()).thenReturn(mockRequest2);
		
		Request mockRequest3 = mock(Request.class);
		when(mockRequest3.getRequester()).thenReturn(otherRequester);
		when(mockRequest3.getTargeted()).thenReturn(expectedTargeted);
		when(mockRequest3.getType()).thenReturn(Request.RequestType.BE_MY_ASSISTED);
		when(mockRequest3.clone()).thenReturn(mockRequest3);
		
		Request mockRequest4 = mock(Request.class);
		when(mockRequest4.getRequester()).thenReturn(otherRequester);
		when(mockRequest4.getTargeted()).thenReturn(otherTargeted);
		when(mockRequest4.getType()).thenReturn(Request.RequestType.BE_MY_ASSISTED);
		when(mockRequest4.clone()).thenReturn(mockRequest4);
		
		ArrayList<Request> mockRequests = new ArrayList<Request>();
		mockRequests.add(mockRequest1);
		mockRequests.add(mockRequest2);
		mockRequests.add(mockRequest3);
		mockRequests.add(mockRequest4);
		
		// Setting up mockAccount
		when(mockAccount.getUser()).thenReturn(mockCurrentUser);
		when(mockAccount.getRequests()).thenReturn(mockRequests);
		
		// Act
		ArrayList<Request> receivedRequests = KIT.app().getReceivedRequests(Request.RequestType.BE_MY_CARER);
		
		// Assert
		assertEquals("Returned too many requests",1,receivedRequests.size());
		Request returnedRequest = receivedRequests.get(0);
		assertNotNull("Returned Request is Null",returnedRequest);
		assertEquals("Returned Request is not from the correct requester",expectedRequester,returnedRequest.getRequester());
		assertEquals("Returned Request is not to the correct targeted",expectedTargeted,returnedRequest.getTargeted());
		assertNotNull("Returned Request has Null type",returnedRequest.getType());
		assertEquals("Returned Request is not of the correct type",Request.RequestType.BE_MY_CARER,returnedRequest.getType());
	}
	
}
