package ssp.kit.test.unit;

import java.util.Date;

import junit.framework.TestCase;
import ssp.kit.objects.Request;
import ssp.kit.communication.Symbols;

public class RequestUnitTest extends TestCase {
	private final String carer1 = "Carer1";
	private final String assisted1 = "Assisted1";
	private final String carer2 = "Carer2";
	private final String assisted2 = "Assisted2";
	private final Date timestamp = new Date();
	private final Date timestampNull = null;
	private final Request.RequestType beCarer = Request.RequestType.BE_MY_CARER;
	private final Request.RequestType beAssisted = Request.RequestType.BE_MY_ASSISTED;
	
	@Override
	public void setUp() throws Exception {
	}
	
	public void testIsSentTo() {
		Request request = new Request(assisted1, carer1, beCarer, timestampNull);
		assertTrue(request.isSentTo(carer1));
		assertTrue(!request.isSentTo(carer2));
	}
	
	public void testSameAs() {
		Request request1 = new Request(assisted1, carer1, beCarer, timestampNull); //using this as the base comparison
		Request request2 = null; //a null request
		Request request3 = new Request(assisted1, carer2, beCarer, timestampNull); //a different targeted username
		Request request4 = new Request(assisted2, carer1, beCarer, timestampNull); //a different requester username
		Request request5 = new Request(assisted1, carer1, beAssisted, timestampNull); //a different request type
		Request request6 = new Request(assisted1, carer1, beCarer, timestampNull); //a same request
		assertTrue(!request1.sameAs(request2));
		assertTrue(!request1.sameAs(request3));
		assertTrue(!request1.sameAs(request4));
		assertTrue(!request1.sameAs(request5));
		assertTrue(request1.sameAs(request6));
	}
	
	public void testEquals() {
		Request request1 = new Request(assisted1, carer1, beCarer, timestampNull);
		Request request2 = null;
		String request3 = "Test";
		Request request4 = new Request(null, carer1, beCarer, timestampNull);
		Request request5 = new Request(assisted2, carer2, beCarer, timestampNull);
		Request request6 = new Request(assisted1, null, beCarer, timestampNull);
		Request request7 = new Request(assisted1, carer2, beCarer, timestampNull);
		Request request8 = new Request(assisted1, carer1, beCarer, timestamp);
		Request request9 = new Request(assisted1, carer1, beCarer, new Date());
		Request request10 = new Request(assisted1, carer1, beAssisted, timestampNull);
		Request request11 = new Request(assisted1, carer1, beCarer, timestampNull);
		assertTrue(request1.equals(request1)); //Testing for the same object
		assertTrue(!request1.equals(request2)); //Testing for the other object being null
		assertTrue(!request1.equals(request3)); //Testing for the other object being being of a different type
		assertTrue(!request4.equals(request1)); //Testing for the objects to have a null for requester and a non-null requester for the other object
		assertTrue(!request1.equals(request5)); //Testing for one object to have different non-null requesters
		assertTrue(!request6.equals(request1)); //Testing for one object to have a null targeted and a non-null targeted for the other object
		assertTrue(!request1.equals(request7)); //Testing for the objects to have different non-null targeted
		assertTrue(!request1.equals(request8)); //Test for one object to have a null timestamp and a non-null timestamp for the other object
		assertTrue(!request8.equals(request9)); //Test for the objects to have different non-null timestamps
		assertTrue(!request1.equals(request10)); //Test for the objects to have a different request types
		assertTrue(request1.equals(request11)); //Test for the objects having the same values for each of the attributes
	}
	
	public void testClone() {
		Request request1 = new Request(assisted1, carer1, beCarer, timestampNull);
		Object request1Clone = request1.clone();
		assertNotNull(request1Clone);
		assertTrue(request1Clone instanceof Request);
		
		//Checking the attributes of the cloned request
		assertEquals(request1.getRequester(), ((Request) request1Clone).getRequester());
		assertEquals(request1.getTargeted(), ((Request) request1Clone).getTargeted());
		assertEquals(request1.getType(), ((Request) request1Clone).getType());
		assertEquals(request1.getTimestamp(), ((Request) request1Clone).getTimestamp());
	}
	
	public void testGetRequestMessage() {
		Request request1 = new Request(assisted1, carer1, beCarer, timestampNull);
		Request request2 = new Request(carer1, assisted1, beAssisted, timestampNull);
		String requester1 = request1.getRequester();
		assertNotNull(requester1);
		String requester2 = request2.getRequester();
		assertNotNull(requester2);
		String expectedRequestMessage1 = requester1 + Symbols.BMCreqmsg;
		String expectedRequestMessage2 = requester2 + Symbols.BMAreqmsg;
		assertEquals(expectedRequestMessage1, request1.getRequestMessage());
		assertEquals(expectedRequestMessage2, request2.getRequestMessage());
	}
	
	public void testGetApprovalMessage() {
		Request request1 = new Request(assisted1, carer1, beCarer, timestampNull);
		Request request2 = new Request(carer1, assisted1, beAssisted, timestampNull);
		String requester1 = request1.getTargeted();
		assertNotNull(requester1);
		String requester2 = request2.getTargeted();
		assertNotNull(requester2);
		String expectedApprovalMessage1 = requester1 + Symbols.BMCappmsg;
		String expectedApprovalMessage2 = requester2 + Symbols.BMAappmsg;
		assertEquals(expectedApprovalMessage1, request1.getApprovalMessage());
		assertEquals(expectedApprovalMessage2, request2.getApprovalMessage());
	}
	
	public void testGetCarer() {
		Request request1 = new Request(assisted1, carer1, beCarer, timestampNull);
		Request request2 = new Request(carer1, assisted1, beAssisted, timestampNull);
		assertEquals(carer1, request1.getCarer());
		assertEquals(carer1, request2.getCarer());
	}
	
	public void testGetAssisted() {
		Request request1 = new Request(assisted1, carer1, beCarer, timestampNull);
		Request request2 = new Request(carer1, assisted1, beAssisted, timestampNull);
		assertEquals(assisted1, request1.getAssisted());
		assertEquals(assisted1, request2.getAssisted());
	}
	
	public void testGetTimestamp() {
		Request request1 = new Request(assisted1, carer1, beCarer, timestamp);
		Request request2 = new Request(carer1, assisted1, beAssisted, timestampNull);
		assertTrue(timestamp.equals(request1.getTimestamp()));
		assertTrue(timestamp != request1.getTimestamp());
		assertNull(request2.getTimestamp());
	}
	
	@Override
	public void tearDown() throws Exception {
	}

}
