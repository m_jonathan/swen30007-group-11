package ssp.kit.test.unit;

import java.util.ArrayList;
import java.util.Date;

import ssp.kit.objects.Reply;
import ssp.kit.ui.ActivityReplies;
import junit.framework.TestCase;

public class ActivityRepliesUnitTest extends TestCase {
	
	public void testSortReplies() {
		// Arrange
		String username = "testUsername";
		String fullname = "testFullname";
		String message = "testMessage";
		Reply r1 = new Reply(new Date(100000), username, fullname, message);
		Reply r2 = new Reply(new Date(200000), username, fullname, message);
		Reply r3 = new Reply(new Date(300000), username, fullname, message);
		Reply r4 = new Reply(new Date(400000), username, fullname, message);
		Reply r5 = new Reply(new Date(500000), username, fullname, message);
		
		ArrayList<Reply> replies = new ArrayList<Reply>();
		replies.add(r3);
		replies.add(r5);
		replies.add(r1);
		replies.add(r4);
		replies.add(r2);
		
		// Act
		ActivityReplies.sortReplies(replies);
		
		// Assert
		assertEquals(r5, replies.get(0));
		assertEquals(r4, replies.get(1));
		assertEquals(r3, replies.get(2));
		assertEquals(r2, replies.get(3));
		assertEquals(r1, replies.get(4));
	}

}
