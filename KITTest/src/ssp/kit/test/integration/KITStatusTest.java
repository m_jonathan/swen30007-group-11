package ssp.kit.test.integration;

import static org.mockito.Matchers.anyObject;
import static org.mockito.Matchers.isNull;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.timeout;
import static org.mockito.Mockito.verify;
import ssp.kit.KIT;
import ssp.kit.ServerResponseListener;
import ssp.kit.objects.Status;
import ssp.kit.objects.Status.Signal;
import ssp.kit.objects.User;
import ssp.kit.ui.ActivityLogin;
import ssp.kit.ui.ActivityMain;
import android.test.InstrumentationTestCase;

public class KITStatusTest extends InstrumentationTestCase {

	private final String kitPackage = "ssp.kit";
	private final int generalTimeout = 20000;
	private final String userUsername = "mj";
	private final String userPassword = "mj";
	private boolean loggedIn = false;
	private ActivityMain activityMain;
	
	@Override
	public void setUp() {
		logInIfHaventDoneSo();
		activityMain = launchActivity(kitPackage, ActivityMain.class, null);
		assertNotNull("activityMain is null", activityMain);
	}
	
	public void testStatusOKWithMessage() {
		String message = "KITTest Status OK";
		
		ServerResponseListener mockServerResponseListener = mock(ServerResponseListener.class);
		
		KIT.app().publishStatusOK(message, mockServerResponseListener);
		
		verify(mockServerResponseListener, timeout(generalTimeout)).onServerResponded(anyObject(), (Exception)isNull());
		
		User loggedInUser = getLoggedInUser();
		Status currentStatus = loggedInUser.getStatus();
		assertNotNull(currentStatus);
		assertEquals(currentStatus.getUsername(), loggedInUser.getUsername());
		assertEquals(currentStatus.getFullName(), loggedInUser.getFullName());
		assertEquals(currentStatus.getMessage(), message);
		assertEquals(currentStatus.getSignal(), Signal.OK);
		assertNull(currentStatus.getUrgency());
		assertNotNull(currentStatus.getReplies());
		assertTrue(currentStatus.getReplies().isEmpty());
	}
	
	
	private void logInIfHaventDoneSo() {
		if (loggedIn) {
			return;
		}
		
		ActivityLogin activityLogin = launchActivity(kitPackage, ActivityLogin.class, null);
		assertNotNull("activityLogin is null", activityLogin);
		
		ServerResponseListener mockServerResponseListener = mock(ServerResponseListener.class);
		
		KIT.app().login(userUsername, userPassword, activityLogin, mockServerResponseListener);
		
		verify(mockServerResponseListener, timeout(generalTimeout)).onServerResponded(anyObject(), (Exception)isNull());
		
		loggedIn = true;
	}
	
	private User getLoggedInUser() {
		User user = KIT.app().getUser();
		assertNotNull(user);
		return user;
	}
//
//public void testSomething() {
//	User user = getLoggedInUser();
//	
//	String username = user.getUsername();
//	String fullName = user.getFullName();
//	String message = "KITTest Status OK";
//	Signal signal = Signal.OK;
//	Urgency urgency = null;
//	
//	Status status = new Status(null, username, fullName, message, signal, urgency);
//	ServerResponseListener mockServerResponseListener = mock(ServerResponseListener.class);
//	KIT.app().publishStatusOK(message, mockServerResponseListener);
//	verify(mockServerResponseListener, timeout(generalTimeout)).onServerResponded(anyObject(), (Exception)isNull());

}
